.. Portmanager documentation master file, created by
   sphinx-quickstart on Wed Jun 13 16:52:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Portmanager's documentation!
=======================================

.. toctree::
   :maxdepth: 2

   views



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
