#!/bin/bash

if [ $# -ne 4 ] ; then
	echo "usage: $0 tftpserver file destination write_community"
	exit 1
fi

TFTPSERVER=$1
TFTPFILE=$2
DESTINATION=$3
COMMUNITY=$4

RAND=$RANDOM

snmpset -v 2c -m CISCO-CONFIG-COPY-MIB \
	-c $COMMUNITY $DESTINATION \
	ccCopyProtocol.$RAND i 1 \
	ccCopySourceFileType.$RAND i 1 \
	ccCopyDestFileType.$RAND i 4 \
	ccCopyServerAddress.$RAND a $TFTPSERVER \
	ccCopyFileName.$RAND s "$TFTPFILE"
snmpset -v 2c -m CISCO-CONFIG-COPY-MIB \
	-c $COMMUNITY $DESTINATION \
 	ccCopyEntryRowStatus.$RAND i 1

sleep 5
RAND=$RANDOM

snmpset -v 2c -m CISCO-CONFIG-COPY-MIB \
	-c $COMMUNITY $DESTINATION \
	ccCopySourceFileType.$RAND i 4 \
	ccCopyDestFileType.$RAND i 3
snmpset -v 2c -m CISCO-CONFIG-COPY-MIB \
	-c $COMMUNITY $DESTINATION \
 	ccCopyEntryRowStatus.$RAND i 1
