��    �      L    |      �  J  �  �  �  *  �  �  �  �  �    @  \   B  �   �  �   N      �   %  6      E  o   L  �   �  s   �   X  �   W  N"    �#    �$  �   �%  ~   �&  d   3'  �   �'  �   P(  R   )  �   g)  i   *  �   �*    +  g   &,  ^   �,  X  �,  \  F.  �   �/  T  �0  ;  �1  g  3  �   �4  L   Y5  �   �5  �   R6  �   �6  �   �7  e   m8  �   �8  �  �9     8<     D<     R<     a<     i<     q<     w<     �<     �<     �<     �<     �<     �<     �<     �<     �<     �<     =     =     %=     6=     J=     Z=     l=     |=     �=     �=     �=     �=  
   �=     �=     �=     �=     
>  �   >     �>     �>     �>     �>  
   �>     �>     �>     ?  	   $?     .?     4?     G?     O?  r   U?     �?     �?     �?     �?     @  
   
@     @     @     ,@     @@     N@     S@     \@     q@     �@     �@     �@     �@  
   �@     �@     �@     �@  	   �@     �@  	   �@     �@     A     A     $A     ;A     AA  0   IA  9   zA  .   �A     �A     �A  	   �A     	B     B     B     !B     2B     @B     MB     [B     bB     nB     {B     �B     �B     �B     �B  8   �B  c   �B  ,   XC     �C     �C     �C     �C     �C     �C  	   �C  	   �C     �C     �C     D     D  	   D     #D     (D  $   ;D     `D     zD     �D     �D     �D     �D  '   �D  	   �D     E     E     E      E     6E  
   BE     ME     ]E     iE     wE  	   }E     �E     �E  !   �E     �E  	   �E     �E  "   �E     
F  6   F  n   GF     �F  \  �F    $H  �  <I  �   $K  ;  L  r  LM  �   �N  e   �O  �   �O  �   �P  �   rQ    iR    kS  �   oT  w   VU  �   �U  t   �V  m  W  s  �X  D  �Y     :[    ;\  o   Y]  W   �]  �   !^  �   _  K   �_  �   `  f   �`  �   2a    �a  b   �b  ^   2c  Q  �c  t  �d     Xf  �  Yg    �h  _  j  �   bk  I   (l  �   rl  �   m  �   �m  �   vn  p   o  �   �o  ;  Pp     �r     �r     �r     �r     �r     �r     �r     �r     �r     s  &   %s     Ls     Us     js     qs     �s     �s     �s     �s     �s     �s     �s     t     t     9t     Nt     _t     tt  
   {t     �t     �t     �t     �t     �t  �   �t     xu     �u     �u     �u     �u     �u     �u  &   �u     v     %v     +v     @v     Iv  x   Rv  
   �v     �v     �v     �v     w     w     #w     +w     Dw     bw  
   {w     �w     �w     �w     �w     �w     �w     �w     �w     �w     �w     x     	x     x  	   x     )x  #   8x     \x     dx     {x     �x  "   �x  "   �x  "   �x     �x     y     y     *y     <y     Ny     Ty     py     �y     �y  	   �y     �y  
   �y  
   �y     �y     �y     �y     z  /   z  {   Nz  5   �z      {     {     -{     C{     R{     W{     ]{     l{     {{     �{     �{     �{     �{     �{     �{  (   �{  #   �{       |     A|     \|     w|     ||  -   �|     �|     �|     �|     �|     �|     }  
   ,}     7}     K}     ^}     r}     ~}     �}     �}  "   �}     �}     �}     �}  "   	~     ,~  2   1~  |   d~     �~     :               +       ]   j   �   	   4             9      �               .   B           ;   o          '      �   u   w   %   �   ^   n   "       �   L   3       E   �   �   *   �   �   p   F       C   `       ,   �       �   #   b       7   f   �   r   ?           �   H   �   |         �   l   i       �   A   �   S   �         N           �   5       �   �         �       D                  �   �   2   �       �   �   �       �   k       {          m           /   �           @   x   V   _   <   G   
   �   [   �   �          R   =   �   v   �      ~   s          P       �   X   �   �   �   �   �       �           1   Y   �   a       �   �           �       d   �   -       I   �              �      �   �           �      K              }   �          O   )           �              J       �           W             e   q   �   t      �   8   �   z   �   Q   g           \   U   y   0       $       �   !      �   �       6       �   �   �      h       T              Z   �   �   �   c   &      �   �   �       M       (   >    
                    In this section, you can move all parameters of single port
                    (inc. port security parameter) into another.
                    Action is restricted by locality and source port is set to default configuration (shutdown, port
                    security 1, description none).
                 
            At the top of the Devices section, there is search field. Just type in what you want to find and wait for
            the results. Search is case sensitive. Results are cached and they are updated on every view of some device,
            so it should reflect the current config in case you manage your devices exclusively by Portmanager.
            If you want to update the dataset, click on update button on the right.
         
            Basically, you can transfer settings of single port, or all ports on device. Destination port is set same as
            source port (description, VLAN, PortSec, State). After that, source port is turned off, PortSec is set to 1
            and
            port is turned off.
         
            Be aware of that you need to specify destination device regex in settings first, this function is designed
            to
            1:1 transfer, so the regex must be set to match same number of ports as the source device.
            To transfer all ports, click on Devices > three dots on right for specific device > Transfer
            port settings. Select destination device and click on Transfer port settings.
         
            For exporting data from single device, you can use export action (Device > three dots on right > Export).
            It automatically generates CSV file with fields device, port, description, VLAN number, VLAN name PortSec
            and
            state.
            If you want to get some specific attributes, or get data from multiple devices, use Data export section in
            menu.
         
            If you want to change port description, just click on the actual label, change it and press
            enter to save new description.
            To change VLAN on a port, click on actual VLAN and you will see list of available VLANs.
         
            In <b>Log</b> tab, you can see list of actions you made in the system.
         
            In <b>User settings</b> tab, you can see your personal information, list of devices you manage
            and
            list of groups you belong to.
         
            Port Security tells you how many MAC addresses are allowed on a port. To check MAC address of
            devices
            connected to the port, click on Show mac adresses.
         
            Port can be in connected, not connected and disabled state. Not connected means, that port is
            enabled, but
            no active device is connected to that port. To enable/disable port, just click on current state of a
            port.
         
            Superuser is able to set default port config for every device type. You can apply it by clicking on Devices
            >
            Device modal > three dots on right for specific port > Apply default config.
         
            To transfer single port, click on Devices > Device modal > three dots on right for specific port > Transfer
            port settings. Select destination device and wait until ports on destination device are loaded. Select port
            and
            click on Transfer port settings.
         
            You can also see current power consumption of devices connected to PoE port. But the device
            needs to support it
            and device config needs to have PoE regex configured to tell the system which ports are
            PoE.
         
        <p>
            Add users that should see/modify ports on devices inside this group.
        </p>
     
        <p>
            Admins of the group are able to modify devices owned by the group and also add new ones.
            They are also responsible of adding new group users.
        </p>
     
        <p>
            Check if you don't want members of groups modify the ports defined here.
        </p>
     
        <p>
            Click on Vlans in admin menu (Only available for SuperUsers). Page shows all VLAN definitions in the system.
            Definitions are used to restrict what VLANs can be set on some port of a device.
            They can be used to restrict access for read and/or write for specific group of users.
        </p>

     
        <p>
            Click on ports in admin menu (Only available for SuperUsers). Page shows all port definitions in the system.
            Definitions are used to restrict what ports should be shown in the application.
            They can be used to restrict access for read and/or write for specific group of users.
        </p>

     
        <p>
            Click on switches in admin menu (Only available for SuperUsers). Page shows all devices in the system. You can change information about specific device there
            and also add new one. Click on create new switch to do that.
        </p>

     
        <p>
            Click on user groups in admin menu. Page shows all user groups in the system.
            Groups are used in device management section as logical unit to restrict user access to specific devices, VLANs, ports, etc.
        </p>
     
        <p>
            Click on users in admin menu (Only available for SuperUsers). Page shows all users in the system. You can change information about specific user there
            and, more importantly, modify user rights.
        </p>
     
        <p>
            Fill SNMP community string there. If you don't know it, check device configuration.
        </p>
     
        <p>
            Fill in hostname (preferred) or IP address of the device.
        </p>
     
        <p>
            Fill in range of VLAN numbers. e.g. for Cisco: [1-4096]
            You can add more tha one definition, also single number ([1,1001-1024]).
        </p>
     
        <p>
            Fill in range of ports numbers. e.g. for Cisco: [10001-10024]
            You can add more tha one definition, also single number ([10001,10001-12104]).
        </p>
     
        <p>
            Fill in version of SNMP used on device.
        </p>
     
        <p>
            Groups that should have access to the device.
            Only members of groups added here can see the device, do not forget to add it.
        </p>
     
        <p>
            Groups that should have access to the devices mentioned above.
        </p>
     
        <p>
            If checked, user is able only to list ports on devices, but he is not able to change anything there.
        </p>
     
        <p>
            If you are administrator of some user group, you can modify users and devices inside without SuperUser
            rights.
            Click on Management section, if you can't see it, you are not administrator of any user group.
        </p>
     
        <p>
            Name is usually chosen to match some place or specific team.
        </p>
     
        <p>
            Name of the definition. Should be simple and clear.
        </p>
     
        <p>
            New user that logs in through MUNI Single Sign-On is automatically added into Portmanager. But he can't see
            any devices and does not have any permission to do something, except changing information about himself.
            Only superusers and local admins are able to set correct rights.
        </p>
     
        <p>
            Page shows all VLAN definitions that you can modify.
            Definitions are used to restrict what VLANs can be set on some port of a device.
            They can be used to restrict access for read and/or write for specific group of users.
            Click on create new vlan to add new definition.
        </p>

     
        <p>
            Page shows all devices that you can modify. You can change information about specific device there
            and also add new one. Click on plus sign in the modal footer to do that.
        </p>

     
        <p>
            Page shows all port definitions that you can modify.
            Definitions are used to restrict what ports should be shown in the application.
            They can be used to restrict access for read and/or write for specific group of users.
            Click on create new port to add new one.
        </p>

     
        <p>
            Page shows all user groups that you can modify.
            Groups are used in device management section as logical unit to restrict user access to specific devices,
            VLANs, ports, etc.
            You can add new members and also add other admins of the group.
        </p>
     
        <p>
            Port security usually restricts how many devices can be connected to single port.
            For security reasons, it should be always one, but in special circumstances it can be set to higher number.
            By default, user is not able to change that. Check it if user should be responsible for changing that.
        </p>
     
        <p>
            Portmanager is able to manage devices manufactured by Cisco, HP and Juniper.
            Select available type of device, or create new one by clicking on button new device.
        </p>
     
        <p>
            Select devices that will be used.
        </p>
     
        <p>
            Select the location where the device is deployed, or create new one by clicking on button on the footer of
            the form.
        </p>
     
        <p>
            Select the location where the device is deployed, or create new one by clicking on button on the footer of the form.
        </p>
     
        <p>
            To be able to see/modify ports on real devices, we need to add the device to the system and also let the system know what ports and VLANs should be accessible.
        </p>
     
        <p>
            Used for mapping PoE ports into system.
            Fill in ranges with all PoE ports on switch to be able to see it in Portmanager.
        </p>
     
        <p>
            Used for updating settings on Juniper devices via NetConf.
        </p>
     
        <p>
            Whole Administration part of the menu is restricted for SuperUsers.
            Be aware of that regular users should not have right to see that.
        </p>
     
        <p> You have several items in the menu.
            The most important part is in <b>Devices</b> tab, where you can see actual settings of the devices that you
            have
            access to and you can change different parameters for each displayed port.
            After clicking on a device, you will see a list of ports with information about description, VLAN, port
            security
            and its state, etc. If you do not see a list of devices for any reason,
            feel free to contact administrator, or <a href="mailto:networks@ics.muni.cz">ICS MUNI</a> if you don't know
            who you
            should contact.
        </p>
      Add device  Add location ADMINISTRATION Actions Address Admin Admin reply Admins App Configuration App configuration values Apply default config Attribute set Basic usage Cancel Choose user City Close Community string Create new device Create new group Create new location Create new port Create new switch Create new user Create new vlan Create switch Create user Czech Data export Decription Default port config management Default port configurations Description Destination device Destination device has different number of ports. Set destination device port definition to match same number of ports as the source device Destination port Device address Device management Device name Device set Device type Devices Devices you have an access to Edit user Email Email is not valid English Event Experimental beta version (2.0) In case of any problems, questions, or ideas for improvement, feel free to contact Export Export by selected attributes Format Global settings Group Group name Groups Guide | Devices Guide | Local admin Guide | Users Help ICS MUNI Local administration Local management Locality Location Location management Location name Management Manufacturer Member of groups Model Move port Name PoE Regex Port management Port security PortSec Portmanager user guide Ports Portsec Press ENTER to apply immediate change of address Press ENTER to apply immediate change of community string Press ENTER to apply immediate change of regex Press ENTER to save Profile Read only ReadOnly Readonly Regex Registered users Regular Users Regular user Regular users Remark Remove user SNMP version SSH Key Save Save device Save location Search for specific port Select a location where the switch is physically located Select at least one device and attributes you'd like to export. Port names are exported by default. Select groups that have access to the device Set community string Set the SNMP version Source device Source port State Street SuperUser Superuser Surname Switch management Switches Tel. number Telephone Time Transfer all ports Transfer all ports to another switch Transfer all switch ports Transfer device settings Transfer port settings Transfer single port UCO USER SECTION Unread notifications from administrator Use cases Used devices User User Groups User group management User groups User guide User management User rights User settings Users VLAN name VLAN number VLANs View all operations on the system View and edit port View logs View user logs View your operations on the system Vlans Welcome to the app for administration of access ports. You do not have permissions to any device, please contact the administrator to assign rights to your location. name@example.com Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2
 
V této sekci máte možnost přemístit veškeré parametry portu (včetně maximálního počtu povolených zařízení)na port jiný. Akce je omezena lokalitou a zdrojový port je nastaven do defaultní konfigurace (shutdown, max. 1 povolené zařízení, prázdný popisek). 
V horní části sekce Správa portů je vyhledávací pole. Napište co chcete vyhledat a vyčkejte na výsledky vyhledávání. Vyhledávání respektuje velká písmena. Výsledky jsou cachovány a aktualizace probíhá při každém kliknutí na zařízení, pokud je zařízení spravováno pouze přes Portmanager, výsledky by měly být aktuální. V opačném případě můžete využít manuální aktualizaci tlačítkem aktualizovat vpravo od vyhledávácího pole.         
Lze přenést nastavení jednoho konkrétního portu, nebo všech portů zařízení. Cílový port je nastavený stejně jako zdrojový (popis, VLAN, PortSec, stav). Poté je zdrojový port vypnut a PortSec nastaven na jedna.         
Berte na vědomí, že je třeba nejdříve specifikovat regex v nastavení, tato funkce je určena pro přesun 1:1, takže regex musí obsahovat stejný počet portů jako na zdrojovém zařízení. K přesunu všech portů klikněte na Zařízení > tři tečky vpravo > Přenést nastavení zařízení.         
Pro export dat z konkrétního zařízení můžete použít akci export (Zařízení > tři tečky vpravo > Export). Akce automaticky vytvoří CSV soubor s atributy zařízení, port, popis, číslo a jméno VLAN, PortSec stav. Pokud vyžadujete specifičtější výběr atributů, nebo data z více zařízení současně, využijte sekci Export dat v menu.         
Pokud chcete změnit popis portu, klikněte na aktuální popis, změňte ho stiskněte Enter pro uložení. Ke změně VLAN klikněte na aktuálně nastavenou VLAN a zobrazí se Vám seznam dostupných VLAN.         
            V sekci <b>Log</b> můžete vidět seznam akcí které jste v systému provedli.         
            V sekci <b>Přehled uživatele</b> můžete vidět údaje o vaší osobě, seznam zařízení které máte k dispozici 
            a
            seznam skupin, do kterých patříte.
         
Port Security atribut říká, kolik MAC adres je povoleno na portu. Klikněte na Zobrazit MAC adresy pokud zkontrolovat adresy připojených zařízení.          
Port může být ve stavu connected, not connected a disabled. Stav not connected znamená, že port je zapnutý, ale není připojené žádné aktivní zařízení. Zapnutí/vypnutí portu provedete kliknutím na aktuální stav portu.         
Superuživatel je schopný nastavit defaultní konfiguraci portu pro různé typy zařízení v systému. Aplikovat je můžete ve Správě portů konkrétně Záložka konkrétního zařízení > tři tečky vpravo od portu > Vzorová konfigurace.         
K přenesení portu klikněte na Správa portů > Zařízení > tři tečky u portu > Přesunout nastavení. Vyberte cílové zařízení a vyčkejte na to, než se načtou porty. Poté vyberte cílový port a klikněte na Přenést nastavení portu.         
Můžete prohlížet také aktuální odběr PoE zařízení pořipojených na port. Swich to ale musí podporovat a v nastavení zařízení musí být nastaven PoE regex, aby systému řekl, které porty PoE podporují.         
<p>
 Přidejte uživatele, kteří by měli vidět/modifikovat porty na zařízení v této skupině.        </p>
     
<p>
 Administrátoři skupiny jsou schopni upravovat zařízení uvnitř skupiny a přidávat nová zařízení. Zároveň jsou zodpovědní za přidávání nových uživatelů do skupiny.        </p>
     
        <p>
Zaškrtněte, pokud nechcete aby členové skupiny modifikovali zde definované porty.        </p>
     
        <p>
Klikněte na VLAN v administrátorském menu (dostupné pouze pro superuživatele). Stránka zobrazuje všechny definice VLAN v systému. Definice jsou použity pro omezení toho, která VLAN může být na konkrétním portu nastavena. Definice mohou být také použity pro omezení práv čtení nebo zápisu pro specifickou skupinu uživatelů
     
        <p>
Klikněte na Porty v administrátorském menu (viditelné pouze pro superuživatele). Stránka zobrazuje všechny definice portů v systému. Definice jsou použity k omezení toho, které porty by měly být v aplikaci viditelné. Mohou být také použity k omezení přístupu pro čtení nebo zápis pro specifickou skupinu uživatelů.        </p>

     
        <p>
Klikněte na Switche v administrátorském menu (dostupné pouze pro superuživatele). Stránka zobrazuje všechna zařízení v systému. Můžete zde měnit informace o konkrétním zařízení a také vytvářet nová.Pro vytvoření nového zařízení klikněte na vytvořit nový switch.        </p>

     
<p>
 Klikněte na skupiny uživatelů v administrátorské sekci menu. Stránka ukazuje všechny skupiny v systému.  Skupiny jsou používány ve správě zařízení k omezení přístupu na konkrétní zařízení, VLAN, porty, apod. 
        </p>
     
        <p>
            Klikněte na uživatelé v administrátorské sekci menu (dostupné pouze pro superuživatele).
    Stránka zobrazuje všechny užibvatele v systému. Můžete zde měnit informace o konkrétním uživateli a měnit jeho uživatelská práva.        </p>
     
        <p>
Vyplňte SNMP heslo. Pokud ho neznáte, zkontrolujte své nastavení zařízení.        </p>
     
        <p>
Vyplňte FQDN (preferováno), nebo IP adresu zařízení.        </p>
     
        <p>
            Vyplňte rozsah čísel VLAN. například pro Cisco: [1-4096]. 
            Definic lze přidat více než jednu, zároveň to může být jenom konkrétní číslo  ([1,1001-1024]).
        </p>
     
        <p>
Vyplňte rozsah port indexů, například pro Cisco: Cisco: [10001-10024]. 
Můžete vyplnit více než jeden rozsah nebo jen jedno číslo ([10001,10001-12104]).
        </p>
     
        <p>
Vyplňte verzi SNMP použitou na zařízení.        </p>
     
        <p>
Skupiny, které by měly mít přístup k zařízení. Pouze členové skupiny přidaní zde budou schopni vidět zařízení, nezapomeňte je sem přidat.        </p>
     
        <p>
Skupiny, které by měly mít přístup k výše uvedeným zařízením.        </p>
     
        <p>
 Pokud je volba aktivní, uživatel je oprávněn porty pouze zobrazit, ale nemůže na nich nic měnit. 
        </p>
     
        <p>
            Pokud jste administrátor skupiny, můžete měnit uživatele a zařízení uvnitř skupiny bez práva superuživatele. 
            Klikněte na správa v menu, pokud tuto sekci nevidiíte, nejste administrátorem žádné skupiny.
        </p>
     
 <p>
 Jméno je obvykle vybíráno podle lokace, nebo specifického týmu. </p>
        </p>
     
        <p>
Pojmenování definice. Mělo by být jednoduché a výstižné.        </p>
     
        <p>
    Nový uživatel ketrý se přihlásí do systému skrze jednotné přihlášení MUNI je automaticky přidán do Portmanageru.     Nemůže si ale zobrazit žádná zařízení a nemá právo cokoliv dělat.    Pouze superuživatel nebo správce lokality je oprávnéný nastavit správná oprávnění.        </p>
     
        <p>
Stránka zobrazuje všechny definice VLAN které můžete modifikovat. Definice jsou používány k omezení toho, jaká VLAN na jakém portu zařízení může být nastavena. Definice VLAN mohou být použity k omezení práv ke čtení nebo zápisu pro specifickou skupinu uživatelů. Klikněte na vytvořit novou VLAN pro přidání nové definice. 
     
        <p>
            Stránka zobrazuje všechny zařízení, která můžete modifikovat. Můžete měnit existující zařízení a vytvářet nové. 
            Klikněte na znak plus v zápatí k vytvoření nového zařízení.
        </p>

     
        <p>
Stránka zobrazuje všechny definice portů které můžete modifikovat. Definice jsou používány k omezení toho, jaké porty zařízení by v aplikaci měly být pro běžné uživatele viditelné. Definice portů mohou být použity k omezení práv ke čtení nebo zápisu pro specifickou skupinu uživatelů. Klikněte na vytvořit nový port pro přidání nové definice. 
     
        <p>
Stránka zobrazuje všechny skupiny uživatelů které můžete modifikovat. Skupiny jsou použity ve správě zařízení k omezení přístupu ke konkrétnímu zařízení, VLAN, portu, apod. Je možné přidávat nové členy a také administrátory skupiny. 
     
        <p>
Port security parametr obvykle omezuje kolik zařízení je možné připojit na jeden port.Kvůli bezpečnosti by parametr měl být vždy roven jedné, ale ve speciálních případech je možné počet navýšit. Defaultně uživatel není oprávněn parametr měnit. V případě potřeby uživateli práva přidejte.        </p>
     
        <p>
Portmanager je schopen spravovat zařízení odf výrobců Cisco, HP a Juniper. Vyberte dostupný typ, nebo vytvořte nový kliknutím na tlačítko nové zařízení.        </p>
     
        <p>
Vyberte zařízení, která budou použita.        </p>
     
        <p>
            Vyberte lokalitu kde bude zařízení nasazeno, nebo vytvořte novou kliknutím na tlačítko na konci formuláře. 
        </p>
     
        <p>
Vyberte lokalitu kde je zařízení nasazeno, nebo vytvořte novou kliknutím na tlačítko v dolní části formuláře.        </p>
     
        <p>
K tomu abyste mohli vidět nebo modifikovat porty na reálném zařízení, musíte přidat zařízení do systému a nastavit, které porty a VLAN by měly být dostupné.         </p>
     
        <p>
Používáno pro mapování PoE portů v systému. Vyplňte rozsahy se všemi PoE porty na zařízení, pokud je chcete v systému vidět.        </p>
     
         <p>
    Používáno pro nastavování portů na zařízeních firmy Juniper přes NetConf   </p>
     
        <p>
 Celá administrátorská sekce menu je viditelná pouze superuživateli. Berte na vědomí, že běžní uživatelé by neměli mít právo tuto sekci vidět. 
        </p>
     
V menu máte na výběr z několika položek. Nejdůležitější je sekce <b>Správa portů</b>,ve které můžete vidět přehled zařízení ke kterým máte přístup a a jejich porty, pro které můžete měnit rozličné parametry. Po kliknutí na konkrétní zařízení se Vám rozbalí seznam portů s informacemi o popisu portu, nastavené VLAN, zabezpečení portu a stavu portu. Pokud je seznam zařízení z nějakého důvodu prázdný, kontaktujte administrátora, pokud nevíte kdo to je, piště na <a href="mailto:networks@ics.muni.cz">ICS MUNI</a></p> přidat zařízení Přidat lokalitu ADMINISTRACE Akce Adresa Administrátor Odpověď administrátora Správci Nastavení aplikace Konfigurace systému Aplikace defaultního nastavení portu Atributy Základní použití Storno Vybrat uživatele Město Zavřít Community heslo Vytvořit nové zařízení Vytvořit novou skupinu Vytvořit novou lokalitu Vytvořit nový port Vytvořit nový switch Vytvořit nového uživatele Vytvořit novou VLAN Vytvořit switch Vytvořit uživatele Česky Export dat Popis Správa vzorové konfigurace Vzorová konfigurace portu Popis Cílové zařízení Cílové zařízení má odlišný počet portů. Nastavte definici regex portů v nastavení cílového zařízení tak, aby byl počet portů stejný. Cílový port Adresa zařízení Správa zařízení Jméno zařízení Zařízení Typ zařízení Správa portů Zařízení ke kterým máte přístup Upravit uživatele Email Email není validní Anglicky Událost Experimentální beta verze (2.0). V případě jakýchkoliv problémů, otázek, nebo nápadů na vylepšení pište na Exportovat Export podle parametrů Formát Globální nastavení Skupina Jméno skupiny Skupiny Nápověda | Zařízení Nápověda | Správa lokality Nápověda | Uživatelé Nápověda ÚVT MU Správa lokality Správa lokality Lokalita Lokalita Správa lokality Název lokality Správa Výrobce Členem skupin Model Přepojování Jméno PoE Regex Správa portů Max. počet povolených zařízení Portsec Portmanager nápověda Porty Portsec Stiskněte ENTER k uložení změn Stiskněte ENTER k uložení změn Stiskněte ENTER k uložení změn Stiskněte Enter pro uložení Profil Pouze pro čtení Pouze pro čtení Pouze pro čtení Regex Registrovaných uživatelů Běžní uživatelé Běžný uživatel Běžní uživatelé Poznámka Smazat uživatele Verze SNMP SSH klíč Uložit Uložit zařízení Uložit lokalitu Vyhledání portu Vyberte lokaci, kde je switch fyzicky umístěn Vyberte alespoň jedno zařízení a atributy, které si přejete exportovat. Označení portů je exportováno defaultně. Vyberte skupiny, které mají k zařízení přístup Nastavte community heslo Nastavte verzi SNMP Zdrojové zařízení Zdrojový port Stav Ulice Superuživatel Superuživatel Příjmení Správa switchů Switche Tel. číslo Telefon Čas Přenést všechny porty Přenést všechny porty na jiný switch Přenést všechny porty na switchi Přenést nastavení zařízení Přenést nastavení portu Přenést konkrétní port UČO UŽIVATELSKÁ SEKCE Nepřečtená upozornění od administrátora Příklady užití Zařízení Uživatelé Skupiny uživatelů Správa skupin uživatelů Skupiny uživatelů Nápověda Správa uživatelů Práva uživatelů Přehled uživatele Uživatelé Jméno VLAN Číslo VLAN VLAN Seznam Vašich operací v systému Prohlížení a změna portu Zobrazení logů Zobrazení logů uživatele Seznam Vašich operací v systému VLAN Vítejte v aplikaci na správu accesových portů. Nemáte přístupová práva k žádnému switchi, požádejte prosím administrátora o přidělení práv k vaší lokaci. name@example.com 