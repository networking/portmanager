function add_reply(id_event, text) {
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            $('.overlay').fadeOut();
            $('.answer').fadeOut();
        }
    };
    xmlhttp.open("POST", "add_reply", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_event", id_event],
        ["text", text]
    ]);
    xmlhttp.send(send_text);
}

function load_more_logs(lFrom, lTo) {
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let switch_data = JSON.parse(xmlhttp.responseText);
            let table = $('#log_table').DataTable();

            for (let i = 0; i < (lTo - lFrom); i++) {
                table.row.add([
                    switch_data[i]["fields"].time,
                    switch_data[i]["fields"].who,
                    switch_data[i]["fields"].what,
                    switch_data[i]["fields"].reply
                ]).draw();
            }

            $(".loader").remove();

            let textAlert = gettext("Additional logs successfully loaded.");
            bootbox.dialog({
                message: textAlert,
                onEscape: true,
                backdrop: true
            });
        }
    };

    xmlhttp.open("POST", "load_more_logs.py", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["lFrom", lFrom],
        ["lTo", lTo + 1]]);
    xmlhttp.send(send_text);
}


$(document).ready(function () {

    $('#log_table').DataTable({
        order: [[0, 'desc']]
    });

    $("#load_logs").click(function () {
        let number_of_logs = $("#number_of_logs_input").val();

        if (number_of_logs.match(/[1-9]\d*/)) {
            let table_rows = $('#log_table').DataTable().count();
            $(this).parent().append('<div class="loader"></div>');

            load_more_logs(table_rows, table_rows + number_of_logs);
        } else {
            let textAlert = gettext("Incorrect number of ports.");
            bootbox.alert({
                message: textAlert,
                onEscape: true,
                backdrop: true
            });
        }
    });

});
