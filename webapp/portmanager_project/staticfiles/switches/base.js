function create_notification(text) {
    /*
    Bootbox used to create notification
     */
    bootbox.dialog({
        message: gettext(text),
        onEscape: true,
        backdrop: true
    });
}

function create_alert(text) {
    /*
    Bootbox used to create notification
     */
    bootbox.alert({
        message: gettext(text),
        callback: function () {
            location.reload();
        },
        onEscape: true,
        backdrop: true
    });
}

function getCookie(name) {
    /* Method used to get a cookie value according to the request
     * used only for authorization based on the csrf_token passed in the header
    */
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function makePOSTRawText(array) {
    /* Function for the creation of string that is handed over when sending AJAX request.
    *  It creates strandard string, AJAX adds it to the header and server part is then able to
    *  download the data from the header.
    */
    let raw_text = "";

    for (let i = 0; i < array.length; i++) {
        raw_text += array[i][0] + "=" + array[i][1] + "&";
    }
    return raw_text.substring(0, raw_text.length - 1);
}


$(document).ready(function () {
    $(function () {
        let url = window.location.pathname;
        $(".sidebar-navbar-collapse ul li a").each(function () {
            if ($(this).attr("href") === url || $(this).attr("href") === '') {
                $(this).parent().addClass("active");
            }
        });
    });
});
