function create_true_false_dialog(msg) {
    /*
     tato funkce je vysvetlena v souboru manage_switches.js
     */

    var answer = confirm(msg);
    return answer;

}

function change_statement(pv, pv_id, gs, gs_id, new_statement) {
    /*

	spolecna funkce pro nastavovani prislusnosti skupin k portum a vlanam
	pv nese informaci zda se jedna o porty nebo vlany,
	a
	gs nese informaci zda ase jedna o skupinu nebo switch

    */
    var xmlhttp = new XMLHttpRequest();


    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

        }
    };

    xmlhttp.open("POST", "change_statement_pv", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", pv],
        ["pv_id", pv_id],
        ["gs", gs],
        ["gs_id", gs_id],
        ["new_statement", new_statement]]);
    xmlhttp.send(send_text);

}

function add_item(name, switches, groups, readOnly, regex, mode) {
    /*

	prida bud porty nebo vlany podle nastaveneho modu,
	to uz si zpracuje Django.
	
	parametry jsou sebevysvetlujici 

    */

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {


        }
    };

    xmlhttp.open("POST", "add_portvlan", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["name", name],
        ["switches", switches],
        ["groups", groups],
        ["readOnly", readOnly],
        ["regex", regex],
        ["mode", mode]]);
    xmlhttp.send(send_text);

}

function change_regex(pv, id, regex) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            $('.change_port_regex').each(function () {
                $(this).blur();
            });
            $('.change_vlan_regex').each(function () {
                $(this).blur();
            });
        }
    };

    xmlhttp.open("POST", "change_regex", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", pv],
        ["id", id],
        ["regex", regex],
    ]);
    xmlhttp.send(send_text);

}


function remove(pv, id) {
    /*
	Smaze port/vlan podle IDcka.
	v pv parametru je uvedeno zda se jedna o port nebo vlan a podl etid se smaze

    */

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

        }
    };

    xmlhttp.open("POST", "remove_portvlan", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", pv],
        ["id", id]]);
    xmlhttp.send(send_text);
}

function change_visibility(pv, id) {
    /*
	Zmeni viditelnost [readOnly] na port/vlan podle ID a parametru urcujici o co se jedna.
	po potvrzeni [200] zmeni take opticky uzivateli ze nastala zmena.
    */

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            if (pv === 'P') {
                $('.change_port_vis').each(function () {
                    if ($(this).attr('name') === id) {
                        $(this).attr('disabled', false);
                    }

                });
            }
        }

        else if (xmlhttp.readyState == 3) {
            if (pv === 'P') {
                $('.change_port_vis').each(function () {
                    if ($(this).attr('name') === id) {
                        $(this).attr('disabled', true);
                    }

                });

            }
        }
    };

    xmlhttp.open("POST", "change_visibility", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", pv],
        ["id", id]]);
    xmlhttp.send(send_text);
}


$(document).ready(function () {
    /*
	
	metody hide(), hideAll(), hightlight()  

    */
    var $ports = $('#ports');
    var $loading = $(".loading");
    var $common = $('#common');
    var $add_ports = $('#add_ports');
    var $vlans = $('#vlans');
    var $add_vlans = $('#add_vlans');
    $('#view1').show();

    function highlight(id) {
        $common.css('background-color', 'white');
        $ports.css('background-color', 'white');
        $add_ports.css('background-color', 'white');
        $vlans.css('background-color', 'white');
        $add_vlans.css('background-color', 'white');

        $common.css('border', '1px solid black');
        $ports.css('border', '1px solid black');
        $add_ports.css('border', '1px solid black');
        $vlans.css('border', '1px solid black');
        $add_vlans.css('border', '1px solid black');
        $(id).css('background-color', '#EFF4F7');
        $(id).css('border', '2px solid black');
    }

    function hideAll() {
        $('#view1').hide();
        $('#view2').hide();
        $('#view3').hide();
        $('#view4').hide();
        $('#view5').hide();
    }

    highlight('#common');

    $common.click(function () {
        hideAll();
        $('#view1').show();
        highlight('#common');
    });

    $ports.click(function () {
        $loading.removeClass("hidden");
        hideAll();
        $('#view2').show();
        if ($("#edit_group").hasClass("disabled") == false) {
            $(".checkbox.groups.port_editable").hide();
            $(".change_port_vis").hide();
            $(".port_regex_edit").hide();
        }
        highlight('#ports');
        $loading.addClass("hidden");
    });

    $add_ports.click(function () {
        $loading.removeClass("hidden");
        hideAll();
        $('#view3').show();
        highlight('#add_ports');
        $loading.addClass("hidden");
    });

    $vlans.click(function () {
        $loading.removeClass("hidden");
        hideAll();
        $('#view4').show();
        if ($("#edit_vlan").hasClass("disabled") == false) {
            $(".checkbox.groups.vlan_editable").hide();
            $(".change_vlan_vis").hide();
            $(".vlan_regex_edit").hide();
        }
        highlight('#vlans');
        $loading.addClass("hidden");
    });

    $add_vlans.click(function () {
        $loading.removeClass("hidden");
        hideAll();
        $('#view5').show();
        highlight('#add_vlans');
        $loading.addClass("hidden");
    });

    $(".overlay").click(function () {
        $(".overlay").fadeOut();
        $("#add_location").fadeOut();
        $("#add_device").fadeOut();
    });

    $("#add_port_button").click(function () {
        /*
            event handler, ktery se stara o pridavani portu do databaze
            vytvori si pole do kterych pote pushne vsechny zaskrtnute
            skupiny a switche
        */
        $("#add-ports-form").valid();
        var name = $('#add_port_name').val();
        var switches = [];
        var groups = [];
        var readOnly = $('#add_port_visibility').is(':checked');
        var regex = $('#add_port_regex').val();

        $('.add_port_switches').each(function () {
            if ($(this).is(':checked')) {
                switches.push($(this).attr('name'));
            }
        });

        $('.add_port_groups').each(function () {
            if ($(this).is(':checked')) {
                groups.push($(this).attr('name'));
            }
        });

        switches = switches.join();
        groups = groups.join();
        if ($("#add-ports-form").valid()) add_item(name, switches, groups, readOnly, regex, 'port');

    });


    $("#add_vlan_button").click(function () {
        /*
            analogie k handleru o jedna vyse
        */
        $("#add-vlans-form").valid();
        var name = $('#add_vlan_name').val();
        var switches = [];
        var groups = [];
        var readOnly = $('#add_vlan_visibility').is(':checked');
        var regex = $('#add_vlan_regex').val();

        $('.add_vlan_switches').each(function () {
            if ($(this).is(':checked')) {
                switches.push($(this).attr('name'));
            }
        });

        $('.add_vlan_groups').each(function () {
            if ($(this).is(':checked')) {
                groups.push($(this).attr('name'));
            }
        });

        switches = switches.join();
        groups = groups.join();

        if ($("#add-vlans-form").valid()) add_item(name, switches, groups, readOnly, regex, 'vlan');
    });

    $('.add_switch_to_ports').change(function () {
        var id_switch = $(this).attr('name').split(',')[0];
        var id_ports = $(this).attr('name').split(',')[1];
        if ($(this).is(':checked')) {
            change_statement('P', id_ports, 'S', id_switch, '1');
        }
        else {
            change_statement('P', id_ports, 'S', id_switch, '0');

        }

    });

    $('.add_group_to_ports').change(function () {
        var id_group = $(this).attr('name').split(',')[0];
        var id_ports = $(this).attr('name').split(',')[1];

        if ($(this).is(':checked')) {
            change_statement('P', id_ports, 'G', id_group, '1');
        }
        else {
            change_statement('P', id_ports, 'G', id_group, '0');

        }

    });


    $('.add_switch_to_vlans').change(function () {
        var id_switch = $(this).attr('name').split(',')[0];
        var id_ports = $(this).attr('name').split(',')[1];

        if ($(this).is(':checked')) {
            change_statement('V', id_ports, 'S', id_switch, '1');
        }
        else {
            change_statement('V', id_ports, 'S', id_switch, '0');

        }
    });

    $('.add_group_to_vlans').change(function () {

        var id_group = $(this).attr('name').split(',')[0];
        var id_ports = $(this).attr('name').split(',')[1];

        if ($(this).is(':checked')) {
            change_statement('V', id_ports, 'G', id_group, '1');
        }
        else {
            change_statement('V', id_ports, 'G', id_group, '0');
        }
    });

    $('.change_vlan_vis').change(function () {
        var vlan_id = $(this).attr('name').replace(/\s/g, '');

        change_visibility('V', vlan_id);
    });


    $('.change_port_vis').change(function () {
        var port_id = $(this).attr('name').replace(/\s/g, '');
        change_visibility('P', port_id);
    });

    $('.change_port_regex').keypress(function (e) {
        //e.keyCode 13 je ENTER!

        if (e.keyCode === 13) {
            e.preventDefault();
            change_regex('P', $(this).attr('id'), $(this).val());
        }
    });

    $('.change_vlan_regex').keypress(function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            change_regex('V', $(this).attr('id'), $(this).val());
        }
    });

    $('.remove').click(function () {
        if ($(this).parent().attr('class') === 'ports') {
            if (create_true_false_dialog(gettext('Are you sure you want to delete settings of this port?'))) {
                remove('P', $(this).attr('id').replace(/\s/g, ''));
                $(this).parent().parent().remove();
            }
        }
        else if ($(this).parent().attr('class') === 'vlans') {
            if (create_true_false_dialog(gettext('Are you sure you want to delete settings of this VLAN?'))) {
                remove('V', $(this).attr('id').replace(/\s/g, ''));
                $(this).parent().remove();
            }
        }
    });
    $("#edit_group").click(function () {
        alert(gettext('Caution! All edits are executed immediately'));
        $(".ports_hide").hide();
        $(".checkbox.groups.port_editable").show();
        $(".change_port_vis").show();
        $(".port_regex_edit").show();
        $(this).addClass("disabled");
    });
    $("#edit_vlan").click(function () {
        alert(gettext('Caution! All edits are executed immediately'));
        $(".vlan_hide").hide();
        $(".checkbox.groups.vlan_editable").show();
        $(".change_vlan_vis").show();
        $(".vlan_regex_edit").show();
        $(this).addClass("disabled");
    });
});