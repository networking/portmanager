
function get_switch_permissions(switch_id) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {

            var data = JSON.parse(xmlhttp.responseText);
            var element = document.createElement('table');
            $(element).addClass('table table-condensed');

            if (data.length === 0) {
                var warning = document.createElement('tr');
                $(warning).html(gettext('No ports available on this device'));
                element.appendChild(warning);
            } else {
                var tbody = document.createElement('tbody');
                element.appendChild(tbody);

                for (var i = 0; i < data.length; i++) {
                    var id = data[i][0];
                    var descr = data[i][1];
                    var visibility = data[i][2];
                    var row = document.createElement('tr');
                    $(row).attr('class', 'visibility_' + visibility);
                    var id_column = document.createElement('td');
                    $(id_column).html(id);
                    var descr_column = document.createElement('td');
                    $(descr_column).html(descr);
                    row.appendChild(id_column);
                    row.appendChild(descr_column);
                    tbody.appendChild(row);
                }
            }

            $('#print_' + switch_id).html(element);
        }
    };

    xmlhttp.open("POST", "get_switch_permissions", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");

    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["switch_id", switch_id]]);
    xmlhttp.send(send_text);
}



$(document).ready(function () {
    $('.switch_label').click(function () {
        var element_id = $(this).attr('id');
        var switch_id = element_id.split('_')[1];
        get_switch_permissions(switch_id);
    });

        //boxWidget demands some removeTrigger, -> non-existing #xxx as fix
    $('.switch').each(function () {
        let id = $(this).attr('id').split('_')[1];
        $(this).boxWidget({
            animationSpeed: 500,
            collapseTrigger: '#sw_' + id,
            removeTrigger: '#xxx',
            collapseIcon: '',
            expandIcon: '',
            removeIcon: ''
        });
    });
});