let shouldICheck = true; // tato promena nam zajistuje jestli mame kontrolovat 'statement' lock souboru, meni se napriklad kdyz vychazime z prohlizece, zavirame okno atd.
let lock_files = []; //
let hidden, visibilityChange;
let key_hidder = '';
let refreshing_array = [];

let textFile = null;

makeTextFile = function (text) {
    let data = new Blob([text], {type: 'text/plain'});

    // If we are replacing a previously generated file we need to
    // manually revoke the object URL to avoid memory leaks.
    if (textFile !== null) {
        window.URL.revokeObjectURL(textFile);
    }

    textFile = window.URL.createObjectURL(data);

    return textFile;
};

function export_csv(devices){

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function(){
        if(xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            bootbox.hideAll()
            let export_data = JSON.parse(xmlhttp.responseText);

            if (export_data['error']) {
                bootbox.dialog({
                    message: export_data['error'],
                    onEscape: true,
                    backdrop: true
                });
            } else {
                let timeInMs = Date.now();
                let link = document.getElementById('export_download_link');


                link.href = makeTextFile(export_data['data']);

                link.setAttribute("download", "export" + timeInMs + ".csv");

                let textAlert = gettext("Successfull export.");
                bootbox.alert({
                    message: textAlert,
                    callback: function () {
                        link.click();
                    },
                    onEscape: true,
                    backdrop: true
                });
            }
        }
    };

    xmlhttp.open('POST', 'export_csv', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["devices", devices],
        ["features", ["device", "port", "description", "vlan_num", "vlan_name", "portsec", "state"]],
    ]);

    xmlhttp.send(send_text);
}

function isASCII(str) {
    /*
     funkce zkontroluje, jestli je text v ASCII
    */
    return /^[\x20-\x7F]*$/.test(str);
}


function get_connected_macs(switch_id, port_id) {
    let xmlhttp = new XMLHttpRequest();
    let $selected_interface = $(".get_connected_macs_button[name=" + switch_id + "_" + port_id + "]");
    let element = $selected_interface.parent();

    $selected_interface.hide();
    element.append('<div class="loader1"></div>');

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let text = xmlhttp.responseText;
            let data = JSON.parse(text);
            if (data.length > 0) {
                let mac_list = document.createElement("div");
                for (let i = 0; i < data.length; i++) {
                    let li = document.createElement("p");
                    $(li).html(data[i]).addClass("list-group-item");
                    mac_list.append(li);
                }
                bootbox.dialog({
                    message: mac_list,
                    onEscape: true,
                    backdrop: true
                })
            }
            else {
                bootbox.dialog({
                    message: gettext('There is no MAC address registered on this port.'),
                    onEscape: true,
                    backdrop: true
                })
            }

            $(".loader1").remove();
            $selected_interface.delay(1500).show();

        } else if (xmlhttp.readyState === 4 && xmlhttp.status === 503) {
            bootbox.dialog({
                message: gettext('Action wasn\'t successful. Service unavailable.'),
                onEscape: true,
                backdrop: true
            })
        }
    };


    xmlhttp.open("POST", "get_connected_macs", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");

    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", switch_id],
        ["id_port", port_id]]);

    xmlhttp.send(send_text);
}

function change_alias(id_switch, port, new_alias) {
    /*
        funkce ktera odesle zmenu aliasu na server
    */

    if (!isASCII(new_alias)) {
        bootbox.dialog({
            message: gettext('Alias can contain only ASCII characters'),
            onEscape: true,
            backdrop: true
        });
        return;
    }
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let response = xmlhttp.responseText;
            if (response === "2") {
                bootbox.dialog({
                    message: gettext('You entered an empty string, there is no change'),
                    onEscape: true,
                    backdrop: true
                })
            }
            if (response === '1') {
                bootbox.dialog({
                    message: gettext('The change was not successful'),
                    onEscape: true,
                    backdrop: true
                })
            }
            if (response === '3') {
                bootbox.dialog({
                    message: gettext('Alias cannot contain non-ASCII characters'),
                    onEscape: true,
                    backdrop: true
                })
            }
            if (response === '4') {
                bootbox.dialog({
                    message: gettext('missing write permission, can\'t change port alias'),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "change_port_alias", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");

    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch],
        ["newalias", new_alias],
        ["port", port]
    ]);

    xmlhttp.send(send_text);
}

function turnonoff(id_switch, port) {
    /*
    funkce ktera vypina porty
    */

    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let text = xmlhttp.responseText;
            if (text === "1") {
                bootbox.dialog({
                    message: gettext('missing write permission, turning the port on/off is not possible'),
                    onEscape: true,
                    backdrop: true
                })
            }
            if (text === '2') {
                bootbox.dialog({
                    message: gettext('unable to turn on/off port'),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "turn_on_off_port", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch],
        ["port", port]
    ]);
    xmlhttp.send(send_text);
}

function change_vlan(id_switch, port, vlan) {
    /*
    funkce pro zmenu vlany na cisco switchich
     */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 1) {
            //console.log("readyState 1");
            //console.log(id_switch, port);
        }
        else if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                bootbox.dialog({
                    message: gettext('vlan changed'),
                    onEscape: true,
                    backdrop: true
                })
            }
            else if (responseText === "1") {
                bootbox.dialog({
                    message: gettext('the change was not successful'),
                    onEscape: true,
                    backdrop: true
                })
            }
            else if (responseText === "3") {
                console.log("change to same VLAN");
            }
            else if (responseText === "2") {
                bootbox.dialog({
                    message: gettext('missing write permission, change of VLAN is not possible'),
                    onEscape: true,
                    backdrop: true
                })
            }
        }

        else if (xmlhttp.readyState === 4 && xmlhttp.status === 500) {
            bootbox.dialog({
                message: gettext('internal server error'),
                onEscape: true,
                backdrop: true
            })
        }
    };

    xmlhttp.open("POST", "change_vlan", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch],
        ["port", port],
        ["vlan", vlan]
    ]);
    xmlhttp.send(send_text);
}

function getChangeableInformation(id_switch, port) {
    let xmlhttp = new XMLHttpRequest();
    // getChangeableInformation
    // tato funkce se vola pokud se odemce port na nejakem zarizeni
    // funkce getne pouze jeden port a prepise stavajici skryte informace
    // aby byly aktualni
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = JSON.parse(xmlhttp.responseText);

            let alias = responseText[0].replace('<br>', '');
            let status = responseText[1];
            let vlan_number = responseText[2];
            let vlan_name = responseText[3];

            let row = $('.row_' + id_switch + '_' + port);

            let status_map = {
                0: '<img src="/portmanager/static/switches/connected.png" alt="Connected" title="Connected" height="20px">',
                1: '<img src="/portmanager/static/switches/notconnected.png" alt="Not Connected" title="Not Connected" height="20px">',
                2: '<img src="/portmanager/static/switches/disabled.png"  alt="Disabled" title="Disabled" height="20px">',
                3: 'error'
            };
            // tato funkce je podobna funkci get switch information

            row.find('.turn_on_off').attr('id', id_switch + ',' + port + ',' + status);
            row.find('.turn_on_off_vo').attr('id', id_switch + ',' + port + ',' + status);

            row.find('.turn_on_off').html(status_map[status]);
            row.find('.turn_on_off_vo').html(status_map[status]);


            row.find('.change_alias').html(alias);
            row.find('.select').html('<div class="visible-xs onlynumber">' + vlan_number + '</div><div class="hidden-xs full">' + vlan_number + ' - ' + vlan_name + '</div>');

            $('#temp_row_' + id_switch + '_' + port).remove();
            row.show();
        }
    };

    xmlhttp.open("POST", "get_changeable_information", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["port", port],
        ['id_switch', id_switch]
    ]);
    xmlhttp.send(send_text);

}

function getStatement() {
    /*
        posle serveru request o vsechny lockfile, ktere se ho netykaji [cizi zamky]
    */
    let id_switch_array = JSON.stringify(refreshing_array);
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            let json;
            json = JSON.parse(responseText);
            /*

            server vrati json se vsemi zamky a nyni pozamykame radky tabulky, ktere jsou nekym jinym opravovany
            json prevedeme zpatky do rozumne datove struktury
            */


            for (let i = 0; i < json.length; i++) {
                /*
                    pro kazdy zamek vytvorime 'nahradni' radek do tabulky,
                    a vlozime ho za ten, ktery budeme skryvat, protoze je
                    editovan v jinem prohlizeci

                */

                if ($.inArray(json[i], lock_files) === -1) {
                    lock_files.push(json[i]);
                }

                let switch_id = json[i].split('_')[0];
                let port = json[i].split('_')[1];
                let user = json[i].split('_')[2];

                if ($('#temp_row_' + switch_id + '_' + port).length === 0) {
                    let tr = document.createElement('tr');
                    let element = document.createElement('td');
                    element.setAttribute('id', 'temp_row_' + switch_id + '_' + port);
                    element.setAttribute('class', 'temp_row');
                    element.setAttribute('colspan', '6');
                    element.innerHTML = gettext('User') + ' ' + user + ' ' + gettext('is currently working on this port.');
                    $(element).css('background-color', '#BB084D');
                    tr.appendChild(element);
                    $(tr).insertAfter('.row_' + switch_id + '_' + port);
                    $('.row_' + switch_id + '_' + port).hide();
                }
            }

            let someChanges = false;
            for (let i = 0; i < lock_files.length; i++) {
                /* v lock_files mame soubory ktere byly v predchozi iteraci zamcene
                a nyni uz nejsou, tedy je odemkneme tak, ze zobrazime zpet skryty radek
                a temp smazeme
                 */
                if ($.inArray(lock_files[i], json) === -1) {
                    let index = lock_files.indexOf(lock_files[i]);
                    let switch_id = lock_files[i].split('_')[0];
                    let port = lock_files[i].split('_')[1];
                    lock_files.splice(index, 1);
                    someChanges = true;
                }
            }
            if (someChanges) {
                getChangeableInformation(switch_id, port);
            }
            if (shouldICheck) {
                setTimeout(function () {
                    setTimeout(getStatement(), 4000);
                }, 4000);
            }
        }
    };
    xmlhttp.open("POST", "getStatement", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch_array]
    ]);
    xmlhttp.send(send_text);
}

function getSwitchInformation(id) {
    let xmlhttp = new XMLHttpRequest();
    $('#swc_' + id).html('<div class="loader"></div>');
    xmlhttp.onreadystatechange = function () {
        /*
            po kliknuti na switch, musime zobrazit ziskane informace o
            konkretnim switchi
        */
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            let switch_data = JSON.parse(responseText);
            // kontrola, jestli neni backendem predana nejaka chyba
            let error = switch_data['error'];
            if (error) {
                let msg = gettext('An error occurred while obtaining information from the switch.') +
                    "<br/>" +
                    error.toString();
                $('#swc_' + id).html(msg);
            } else {
                // vse je v poradku
                // nyni z odpovedi extrahujeme informace o portech a vlanech
                let switch_id = switch_data['switch_id'].toString();
                let ports = switch_data['ports'];
                let vlans = switch_data['vlans'];
                let table;
                // status_mapa definuje stringy, ktere se maji nahradit
                // za cisla, tyto cisla jsou predavana z backendu
                let status_map = {
                    0: '<img src="/portmanager/static/switches/connected.png" alt="Connected" title="Connected" height="20px">',
                    1: '<img src="/portmanager/static/switches/notconnected.png" alt="Not Connected" title="Not Connected" height="20px">',
                    2: '<img src="/portmanager/static/switches/disabled.png"  alt="Disabled" title="Disabled" height="20px">',
                    3: 'error'
                };

                table = document.createElement('table');
                table.setAttribute('class', 'display compact');
                table.setAttribute('id', switch_id);
                // nyni vytvorime tabulku, ktera bude ovladat konkretni switch
                let header = document.createElement('thead');
                let tr = document.createElement('tr');
                let label = "<td>Port</td> <td>" + gettext('Port description') + "</td><td>VLAN</td><td title=" + gettext('If the security port is enabled, it indicates the number of allowed MAC addresses') + "><span class='glyphicon glyphicon-lock visible-xs visible-sm'></span><div class='hidden-xs hidden-sm'>" + gettext('Port security') + "</div></td><td><div class='visible-xs'>MAC</div><div class='hidden-xs'>" + gettext('MAC adresses of connected devices') + "</div></td><td>" + gettext('State') + "</td>";
                // hlavicka tabulky
                tr.innerHTML = label;
                header.appendChild(tr);

                table.appendChild(header);


                let tbody = document.createElement('tbody');
                // nyni vytvorime a naplnime telo tabulky
                for (let i = 0; i < ports.length; i++) {
                    // za kazdy port na switchy vytvorime jeden radek tabulky
                    // v patem prvku pole je ulozena informace
                    // zda je port urcen pouze pro cteni, nebo
                    // i pro zapis
                    let read_only;

                    read_only = ports[i][1]['read_only'] !== '0';

                    // nastavime si dle toho promennou read_only
                    let row = document.createElement('tr');

                    row.setAttribute('class', 'row_' + switch_id + '_' + ports[i][0]);
                    let name = document.createElement('td');
                    name.setAttribute('class', 'name_column');
                    let short_name = ports[i][1]['descr'];
                    if (ports[i][1]['descr'].indexOf('FastEthernet') !== -1) {
                        short_name = ports[i][1]['descr'].replace('FastEthernet', 'Fa');
                    }
                    if (ports[i][1]['descr'].indexOf('GigabitEthernet') !== -1) {
                        short_name = ports[i][1]['descr'].replace('GigabitEthernet', 'Gi');
                    }
                    if (ports[i][1]['descr'].indexOf('Ten GigabitEthernet') !== -1) {
                        short_name = ports[i][1]['descr'].replace('Ten GigabitEthernet', 'Ten');
                    }
                    name.innerHTML = "<div class='visible-xs'>" + short_name + "</div><div class='hidden-xs'>" + ports[i][1]['descr'] + "</div>";

                    let alias = document.createElement('td');
                    alias.setAttribute('class', 'alias_column');

                    let change_alias = document.createElement('div');
                    if (read_only) {
                        change_alias.setAttribute('class', 'change_alias_vo');
                    } else {
                        change_alias.setAttribute('class', 'change_alias');
                        change_alias.setAttribute('contentEditable', 'true');
                    }
                    //
                    if (!(ports[i][1]['alias'] === "")) {
                        console.log(ports[i][1]['alias']);
                        change_alias.innerHTML = ports[i][1]['alias'];
                    } else {
                        change_alias.innerHTML = '(none)';
                    }

                    change_alias.setAttribute('id', switch_id + ',' + ports[i][0]);
                    // nastaveni atributu id, na switch_id, port_id, abychom mohli snadno
                    // pote urcit, na kterem portu a switchy chceme danou vec nastavit
                    alias.appendChild(change_alias);


                    let connected_macs = document.createElement('td');
                    $(connected_macs).attr("class", "connected_macs");

                    // Pokud z backendu jako pripojene adresy
                    // prijde 'None', tak tak jej zmenime na N/A
                    // na druhou stranu pokud je tam neco nastavene
                    // tak jsou to jednotlive mac-addresy, ktere jsou oddeleny pomoci
                    // carky, podle carky splitneme string
                    // a zacneme vkladat mac_adresy oddelene enterem

                    let element = document.createElement("div");
                    $(element).attr("class", "get_connected_macs_button");
                    $(element).attr("name", switch_id + "_" + ports[i][0]);

                    $(element).html('<button type="button" class="btn btn-block btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span>' + gettext('Show MAC adresses') + '</button>');


                    $(connected_macs).append(element);

                    let security = document.createElement('td');

                    security.innerHTML = ports[i][1]['portsec'];

                    security.title = gettext('If port security is enabled, number indicates how many MAC addresses are allowed');

                    // v mac_address si nastavime mac_adresu
                    // konkretniho portu
                    let turn_on_off = document.createElement('td');
                    // turn_on_off je tlacitko slouzici k vypinani a zapinani portu
                    //
                    let c_turn_on_off = document.createElement('div');
                    // v pripade ze se jedna o read_only port,
                    // tak opet nastavime tridu navic o _vo a tim
                    // znemoznime "klikani" na ni
                    if (read_only) {
                        c_turn_on_off.setAttribute('class', 'turn_on_off_vo');
                    } else {
                        c_turn_on_off.setAttribute('class', 'turn_on_off');
                    }
                    c_turn_on_off.setAttribute('id', switch_id + ',' + ports[i][1]['iid'] + ',' + ports[i][1]['status']);
                    c_turn_on_off.innerHTML = status_map[ports[i][1]['status']];

                    turn_on_off.appendChild(c_turn_on_off);

                    //
                    // posledni sloupec,
                    // tento sloupec slouzi k zobrazeni do jake vlany
                    // je port nastaven a take aby umoznil zmenit konkretni vlanu
                    //
                    let select = document.createElement('td');
                    select.setAttribute('id', switch_id + '_' + ports[i][0]);

                    // do id si opet ukladame pomocne informace, ktere se nam
                    // pozdeji budou hodit v nastavovani jednotlivych inforamci
                    //

                    let select_vlan = document.createElement('select');
                    select_vlan.setAttribute('id', switch_id + '_' + ports[i][0] + '_' + 'vlan');

                    if (read_only) {
                        select_vlan.setAttribute('class', 'disabled');
                    }

                    let option = document.createElement('option');
                    option.setAttribute('selected', 'selected');
                    option.setAttribute('value', switch_id + '_' + ports[i][0] + '_' + ports[i][1]['vlan']);
                    option.innerHTML = ports[i][1]['vlan'];
                    select_vlan.appendChild(option);


                    for (let j = 0; j < vlans.length; j++) {
                        let vlan_read_only;

                        if (ports[i][1]['vlan'] === vlans[j][0] + " - " + vlans[j][1]) {
                            continue;
                        }

                        if (vlans[j][2] === 0) {
                            vlan_read_only = false;
                        } else {
                            vlan_read_only = true;
                        }

                        let option = document.createElement('option');

                        if (vlan_read_only) {
                            option.setAttribute('class', 'disabled');
                        }
                        option.setAttribute('value', switch_id + '_' + ports[i][0] + '_' + vlans[j][0]);
                        option.innerHTML = vlans[j][0] + ' - ' + vlans[j][1];
                        select_vlan.appendChild(option);
                    }

                    select.appendChild(select_vlan);
                    row.appendChild(name);
                    row.appendChild(alias);
                    row.appendChild(select);
                    row.appendChild(security);
                    row.appendChild(connected_macs);
                    row.appendChild(turn_on_off);

                    // nyni radek naplnime nachystanymi daty a pak radek pridame do tabulky
                    tbody.appendChild(row);

                }
                table.appendChild(tbody);
                $('#swc_' + id).html('');
                $('#swc_' + id).append(table);
                $('#' + switch_id).find('select').each(function () {
                    $(this).select2().on('change', function (e) {
                        let vlan_target = e.delegateTarget.value.split('_');
                        let switch_id = vlan_target[0];
                        let interface_id = vlan_target[1];
                        let vlan_id = vlan_target[2];
                        change_vlan(switch_id, interface_id, vlan_id);
                    });
                });
                $('#' + switch_id).DataTable({
                    paging: false,
                    ordering: false,
                    search: true
                });
            }
        }
    };

    xmlhttp.open("POST", "get_switch_information", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id", id]]);
    xmlhttp.send(send_text);
}

$(document).ready(function () {

    getStatement(refreshing_array);

    /*
     tyto eventhandlery nam zajistuji vyskakovani a vskakovani do oken, aby jsme nevyvolavali requesty zbytecne,
     pripadne nezamknuli soubory, kdyz se nic nedeje
    */


    if (typeof document.hidden !== "undefined") {
        hidden = "hidden";
        visibilityChange = "visibilitychange";
    } else if (typeof document.mozHidden !== "undefined") {
        hidden = "mozHidden";
        visibilityChange = "mozvisibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
        hidden = "msHidden";
        visibilityChange = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
        hidden = "webkitHidden";
        visibilityChange = "webkitvisibilitychange";
    }


    $(document).delegate('.turn_on_off', 'click', function () {
        /*
            vypinac portu, predame funkci id_switche a port, ktery am vypnout a vypne ho
            a v popredi zmeni barvu 'tlacitka'
         */

        let id_switch = $(this).attr('id').split(',')[0];
        let port = $(this).attr('id').split(',')[1];

        $(this).html('<div class="loaderPort"></div>');

        try {
            turnonoff(id_switch, port);
            setTimeout(function () {
                getChangeableInformation(id_switch, port);
            }, 2000)
        }
        catch (err) {
            bootbox.dialog({
                message: err,
                onEscape: true,
                backdrop: true
            });
        }
    });

    //boxWidget demands some removeTrigger, -> non-existing #xxx as fix
    $('.switch').each(function () {
        let id = $(this).attr('id').split('_')[1];
        $(this).boxWidget({
            animationSpeed: 500,
            collapseTrigger: '#header_' + id,
            removeTrigger: '#xxx',
            collapseIcon: '',
            expandIcon: '',
            removeIcon: ''
        });
    });

    $('.export_button').click(function (e) {
        let device = $(this).attr('id').split('_')[1];

        bootbox.dialog({
            message: '<h4 class="text-center mb-0"><i class="fa fa-spin fa-cog"></i>&nbsp; Prebieha export..</h4>',
            closeButton: false
        });

        export_csv(device)
    });

    $('.switch-header').click(function (e) {
        /*
            event stahne informace o switchy,
            a prida switch_id do pole aktualne
            proverovanych switchu

        */

        let id = $(this).attr('id').split('_')[1];
        if ($(this).hasClass('opened')) {
            $(this).removeClass('opened');
            console.log('slidedup' + id);
            refreshing_array = refreshing_array.filter(item >= item !== id
            )
        } else {
            $(this).addClass('opened');
            if (($.inArray(id, refreshing_array)) === -1) {
                refreshing_array.push(id);
            }
            getSwitchInformation(id);
        }
    });

    $(document).delegate('.change_alias[contentEditable]', 'keydown', function (e) {
        if (e.keyCode === 13) {
            // enter
            // v pripade, ze nekdo odenteruje popisek,
            // tak se popisek musi na zarizeni nastavit,
            // tento handler je ekvivalentni k blur (tedy
            // pokud uzivatel rozostri, tak se musi popisek
            // nastavit
            e.preventDefault();

            let id_switch = $(this).attr('id').split(',')[0].replace(/\s/g, '');
            let port = $(this).attr('id').split(',')[1].replace(/\s/g, '');
            // vsechny nutne informace pro nastavovani jsou nastavene
            // v idcku
            let new_alias = $(this).text();

            if (!$(this).text().replace(/\s/g, '').length) {
                // podle dohody pokud uzivatel odesle prazdny
                // alias, pak se na jeho misto nastavi None
                //
                let none = "None";
                $(this).html(none);
                change_alias(id_switch, port, none);
            } else {
                change_alias(id_switch, port, new_alias);
                $(this).attr('id', $(this).attr('id').split(',')[0] + ',' + $(this).attr('id').split(',')[1] + ',' + new_alias);
                $(this).blur();
            }
        }
    });

    $(document).delegate('.change_alias[contentEditable]', 'blur', function (e) {
        // tento handler ulozi konfiguraci na change_aliasu
        // na blur

        let id_switch = $(this).attr('id').split(',')[0].replace(/\s/g, '');
        let port = $(this).attr('id').split(',')[1].replace(/\s/g, '');
        let new_alias = $(this).text();
        if (!$(this).text().replace(/\s/g, '').length) {
            // pokud je text prazdny,
            // pak jej nahradime stringem "None"
            // a odesleme None i do backendu
            let none = "None";
            $(this).html(none);
            change_alias(id_switch, port, none);
        } else {
            change_alias(id_switch, port, new_alias);
            $(this).attr('id', $(this).attr('id').split(',')[0] + ',' + $(this).attr('id').split(',')[1] + ',' + new_alias);
        }
        $('tbody>tr').css('font-weight', 'normal');
    });

    $(document).delegate(".get_connected_macs_button", "click", function () {
        let id = $(this).attr("name");
        let switch_id = id.split("_")[0];
        let port_id = id.split("_")[1];

        get_connected_macs(switch_id, port_id);
    });

    $(function () {
        let pgurl = window.location.pathname;
        $(".sidebar-navbar-collapse ul li a").each(function () {
            if ($(this).attr("href") === pgurl || $(this).attr("href") === '')
                $(this).parent().addClass("active");
        })
    });
});
