from django.conf.urls import include, url
from django.contrib import admin
from django.views.i18n import JavaScriptCatalog

admin.autodiscover()


js_info_dict = {
    'packages': ('switches',),
}

urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^jsi18n/$', JavaScriptCatalog.as_view(), name='javascript-catalog'),
    url(r'^admin/', admin.site.urls),
    url(r'^', include('switches.urls')),
]
