"""
WSGI config for portmanager project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments.

"""
import os
import sys

from django.core.wsgi import get_wsgi_application

path = "/srv/portmanager/"
if path not in sys.path:
    sys.path.append(path)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "portmanager.settings")
application = get_wsgi_application()
