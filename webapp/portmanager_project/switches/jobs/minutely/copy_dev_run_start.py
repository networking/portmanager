from django_extensions.management.jobs import MinutelyJob
import switches.services


class Job(MinutelyJob):
    help = "copy running config periodically to avoid data loss"

    def execute(self):
        switches.services.all_locks_copy_run_start()
