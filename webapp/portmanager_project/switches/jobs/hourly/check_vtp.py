from django_extensions.management.jobs import HourlyJob
import switches.services


class Job(HourlyJob):
    help = "check if Cisco VLANS in portmanager are up-to-date (for caching)"

    def execute(self):
        switches.services.update_all_dev_vlans()
