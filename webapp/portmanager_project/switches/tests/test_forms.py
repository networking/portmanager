from django.test import TestCase
from ..forms import CreateAccountForm
from ..models import Person


class AccountFormTest(TestCase):
    def test_valid_form(self):
        person = Person.objects.create(id_person=123456, name="john", surname="doe", email="test@test.com", superuser=True, contact="123456")
        data = {
            'Uco': person.id_person,
            'Name': person.name,
            'Surname': person.surname,
            'Email': person.email,
            'SuperUser': person.superuser,
            'Telephone': person.contact
        }
        form = CreateAccountForm(data=data)
        self.assertTrue(form.is_valid())

