from django.test import TestCase
from django.test.client import RequestFactory
from .. import views


class SwitchPermissionTest(TestCase):
    """Contact list view tests."""

    def test_contacts_in_the_context_request_factory(self):

        factory = RequestFactory()
        request = factory.get('/get_switch_permissions')
        response = views.get_switch_permission(request)
        self.assertEquals(response.status_code, 200)


class TurnOnOffTest(TestCase):
    def test_turn_on_off(self):
        pass