function getGroupUsers(group_id) {
    /*
    *Gets all members of user group and also users that could be added to the group
    *@param group_id
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = JSON.parse(xmlhttp.responseText);
            let people_all = responseText['all'];
            let members = responseText['members'];
            let data_array = [];
            people_all.forEach(function (element) {
                data_array.push({
                    "id": element['id_person'],
                    "text": element['id_person'] + " - " + element['name'] + " " + element['surname'],
                    "selected": element['selected']
                });
            });
            let select_elem = $('#select_' + group_id);
            select_elem.select2({
                multiple: true,
                data: data_array
            });
            let members_ids = [];
            members.forEach(function (element) {
                members_ids.push(element['id_person']);
            });
            select_elem.val(members_ids).trigger("change");
        }
    };
    xmlhttp.open("POST", "get_group_users", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["group_id", group_id]]);
    xmlhttp.send(send_text);
}


function getGroupAdmins(group_id) {
    /*
    *Gets all admins of user group and also users that could be added to the group as admins
    *@param group_id
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = JSON.parse(xmlhttp.responseText);
            let people_all = responseText['all'];
            let members = responseText['members'];
            let data_array = [];
            people_all.forEach(function (element) {
                data_array.push({
                    "id": element['id_person'],
                    "text": element['id_person'] + " - " + element['name'] + " " + element['surname'],
                    "selected": element['selected']
                });
            });
            let select_elem = $('#adm-select_' + group_id);
            select_elem.select2({
                multiple: true,
                data: data_array
            });
            let members_ids = [];
            members.forEach(function (element) {
                members_ids.push(element['id_person']);
            });
            select_elem.val(members_ids).trigger("change");
        }
    };
    xmlhttp.open("POST", "get_group_admins", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["group_id", group_id]]);
    xmlhttp.send(send_text);
}


function removeFromGroup(user_id, group_id) {
    /*
    *Removes user from group
    *@param user_id
    *@param group_id
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText !== "0") {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };

    xmlhttp.open("POST", "remove_user_from_group", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["user_id", user_id],
        ["group_id", group_id]]);
    xmlhttp.send(send_text);
}


function removeAdminFromGroup(user_id, group_id) {
    /*
    *Removes user from group
    *@param user_id
    *@param group_id
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText !== "0") {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };

    xmlhttp.open("POST", "remove_admin_from_group", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["user_id", user_id],
        ["group_id", group_id]]);
    xmlhttp.send(send_text);
}


function removeGroup(group_id) {
    /*
    *Removes single group from the Django database
    *@param group_id
    */

    let xmlhttp = new XMLHttpRequest();
    let responseText;

    xmlhttp.onreadystatechange = function () {
        responseText = xmlhttp.responseText;
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            if (responseText === "0") {
                let textAlert = gettext("Group was successfully removed.");
                bootbox.alert({
                    message: textAlert,
                    callback: function () {
                        location.reload();
                    },
                    onEscape: true,
                    backdrop: true
                });
            } else {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "remove_group", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["group_id", group_id],]);
    xmlhttp.send(send_text);
}


function createGroup(name) {
    /*
    *Creates new group with unique ID
    *@param name name of the group
    */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                let textAlert = gettext("Group was successfully created.");
                bootbox.alert({
                    message: textAlert,
                    callback: function () {
                        location.reload();
                    },
                    onEscape: true,
                    backdrop: true
                });
            }
            else {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "create_group", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["name", name],]);
    xmlhttp.send(send_text);
}


function addToGroup(user_id, group_id) {
    /*
    *Adds uer to existing group
    *@param user_id
    *@param group_id
    */
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText !== "0") {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "add_user_to_group", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["user_id", user_id],
        ["group_id", group_id]]);
    xmlhttp.send(send_text);
}


function addAdminToGroup(user_id, group_id) {
    /*
    *Adds user to existing group as admin
    *@param user_id
    *@param group_id
    */
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText !== "0") {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "add_admin_to_group", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["user_id", user_id],
        ["group_id", group_id]]);
    xmlhttp.send(send_text);
}


function updateGroupName(gid, name) {
    /*
    *Changes name of the existing group
    *@param gid ID of the group
    *@param name new name of the group
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                $("#group-name_" + gid).text(name);
            } else {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };

    xmlhttp.open("POST", "update_group_name", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["gid", gid],
        ["name", name],]);
    xmlhttp.send(send_text);

}


$(document).ready(function () {

    let select_elem = $('.select_group');

    select_elem.on('select2:select', function (e) {
        let gid = $(this).attr('id').split('_')[1];
        let uid = e.params.data.id;
        addToGroup(uid, gid);
    });

    select_elem.on('select2:unselect', function (e) {
        let gid = $(this).attr('id').split('_')[1];
        let uid = e.params.data.id;
        removeFromGroup(uid, gid);
    });

    select_elem.each(function () {
        getGroupUsers($(this).attr('id').split('_')[1]);
    });

    let select_elem_adm = $('.select_group_adm');

    select_elem_adm.on('select2:select', function (e) {
        let gid = $(this).attr('id').split('_')[1];
        let uid = e.params.data.id;
        addAdminToGroup(uid, gid);
    });

    select_elem_adm.on('select2:unselect', function (e) {
        let gid = $(this).attr('id').split('_')[1];
        let uid = e.params.data.id;
        removeAdminFromGroup(uid, gid);
    });

    select_elem_adm.each(function () {
        getGroupAdmins($(this).attr('id').split('_')[1]);
    });

    $(document.body).on('click', '#new_group', function () {
        bootbox.prompt({
            size: "medium",
            title: gettext('Enter the name the group you\'d like to create'),
            callback: function (result) {
                if (result) {
                    createGroup(result);
                }
            }
        })
    });

    $(document.body).on('click', '.sorting_1', function () {
        let gid = $(this).parent().attr('id');
        console.log(gid);
        let actual_name = $('#group-name_' + gid).text();
        bootbox.prompt({
            size: "medium",
            title: gettext('Enter new name of the group'),
            value: actual_name,
            callback: function (result) {
                if (result) {
                    updateGroupName(gid, result);
                }
            }
        })
    });

    $(document.body).on('click', '.remove_group', function () {
        let gid = $(this).attr('id').split("_")[2];

        bootbox.confirm({
            title: "Remove group?",
            message: gettext('Are you sure you want to delete this user group?'),
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    removeGroup(gid);
                }
            }
        });
    });
});
