/*
 *Creates new user account
 *@param uco ID of a person
 *@param name
 *@param surname
 *@param email
 *@param contact tel. number of a person
 *@param {boolean} superuser
 */
function saveNewAccount(uco, name, surname, email, contact, superuser, read_only, portsec) {

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                let textAlert = gettext("User created. Don\'t forget to add user to groups and check if access to the app is granted.");
                bootbox.alert({
                    message: textAlert,
                    callback: function () {
                        location.reload();
                    },
                    onEscape: true,
                    backdrop: true
                });

            } else {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };

    xmlhttp.open("POST", "create_acc", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["Uco", uco],
        ["Name", name],
        ["Surname", surname],
        ["Email", email],
        ["Telephone", contact],
        ["SuperUser", superuser],
        ["ReadOnly", read_only],
        ["PortSec", portsec],
    ]);
    xmlhttp.send(send_text);

}

/*
 *Updates existing user account
 *@param uco ID of a person
 *@param name
 *@param surname
 *@param email
 *@param contact tel. number of a person
 */
function updateAccount(uco, name, surname, email, contact, superuser, read_only, portsec) {

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                let textAlert = gettext("User successfully changed.");
                bootbox.alert({
                    message: textAlert,
                    callback: function () {
                        location.reload();
                    },
                    onEscape: true,
                    backdrop: true
                });
            } else {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };

    xmlhttp.open("POST", "update_acc", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["Uco", uco],
        ["Name", name],
        ["Surname", surname],
        ["Email", email],
        ["Telephone", contact],
        ["Superuser", superuser],
        ["ReadOnly", read_only],
        ["PortSec", portsec],
    ]);
    xmlhttp.send(send_text);

}

/*
 *Removes existing user account after confirmation
 *@param user_id ID of a person
 */
function removeUser(user_id) {

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                let textAlert = gettext("User successfully removed.");
                bootbox.alert({
                    message: textAlert,
                    callback: function () {
                        location.reload();
                    },
                    onEscape: true,
                    backdrop: true
                });
            } else {
                bootbox.dialog({
                    message: responseText.split(" ").slice(1),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "remove_user", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["user_id", user_id],]);
    xmlhttp.send(send_text);
}


/*
 *Event handlers
 */
$(document).ready(function () {

    var language = $('#current_lang').val();
    if (language !== 'en/') {
        $('#example2').DataTable({
            paging: false,
            ordering: false,
            search: true,
            scrollX: true,
            scrollCollapse: true,
            autoWidth: false,
            fixedColumns: true,
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Czech.json"
            }
        });
    } else {
        $('#example2').DataTable({
            paging: false,
            ordering: false,
            search: true,
            scrollX: true,
            scrollCollapse: true,
            autoWidth: false,
            fixedColumns: true,
        });
    }

    $('#new_user').on('click', function () {
        $('#newuser_box').modal('show');
    });

    $('#create_acc').on('click', function () {
        let $form = $("#new-user-form");
        $form.valid(); //kontrola spravnosti udajov
        let uco = document.getElementById('uco_new');
        let name = document.getElementById('name_new');
        let surname = document.getElementById('surname_new');
        let email = document.getElementById('email_new');
        let contact = document.getElementById('contact_new');
        let superuser = $('#superuser_new').is(':checked');
        let read_only = $('#readonly_new').is(':checked');
        let portsec = $('#portsec_new').is(':checked');

        if ($form.valid()) {
            saveNewAccount(uco.value, name.value, surname.value, email.value, contact.value, superuser, read_only, portsec);
            $('#newuser_box').modal('hide');
        } else return false;
    });

    $('.update-acc').on('click', function () {
        let id = $(this).attr('id').split("_")[1];
        let elem = "#edit-user-form_" + id;
        let $form = $(elem);

        $form.valid(); //kontrola spravnosti udajov
        let name = document.getElementById('name_edit_' + id);
        let surname = document.getElementById('surname_edit_' + id);
        let email = document.getElementById('email_edit_' + id);
        let contact = document.getElementById('contact_edit_' + id);
        let superuser = $('#superuser_edit_' + id).is(':checked');
        let read_only = $('#readonly_edit_' + id).is(':checked');
        let portsec = $('#portsec_edit_' + id).is(':checked');

        if ($form.valid()) {
            updateAccount(id, name.value, surname.value, email.value, contact.value, superuser, read_only, portsec);
            $('#modal_' + id).modal('hide');
        } else return false;
    });


    $.validator.addMethod('phoneValid', function (value, element) {
        //Validator for tel. number
        let contact = document.getElementById('contact_new');
        if (contact && contact.value.match(/^(\+|00)?([0-9 ]*)$/)) {
            return true;
        }
    }, gettext("Tel. number has wrong format"));

    $("#new-user-form").validate({
        rules: {
            uco_new: {
                required: true,
                min: 1
            },
            name_new: {
                required: true
            },
            surname_new: {
                required: true
            },
            email_new: {
                required: true,
                email: true
            },
            contact_new: {
                phoneValid: true
            }
        },
        messages: {
            uco_new: {
                required: gettext("Set UCO"),
                min: gettext("UCO must be >0")
            },
            name_new: gettext("Set Name"),
            surname_new: gettext("Set Surname"),
            email_new: {
                required: gettext("Set Email"),
                email_new: gettext("Email is not valid!")
            },
        },
        highlight: function (element) {
            $(element).closest('.form-control-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            element
                .text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
            $(element).closest('.form-control-group').removeClass('has-error').addClass('has-success');
        }
    });


    $.validator.addMethod('phoneEditValid', function (value, element) {
        //Validator for tel. number
        if (value && value.match(/^(\+|00)?([0-9 ]*)$/)) {
            return true;
        }
    }, gettext("Tel. number has wrong format"));

    $(".edit-user-form").validate({
        rules: {
            name_edit: {
                required: true
            },
            surname_edit: {
                required: true
            },
            email_edit: {
                required: true,
                email: true
            },
            contact_edit: {
                phoneEditValid: true
            }
        },
        messages: {
            name_edit: gettext("Set Name"),
            surname_edit: gettext("Set Surname"),
            email_edit: {
                required: gettext("Set Email"),
                email_edit: gettext("Email is not valid!")
            }
        },
        highlight: function (element) {
            $(element).closest('.form-control-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            element
                .text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
            $(element).closest('.form-control-group').removeClass('has-error').addClass('has-success');
        }
    });

    $('.modal-usr').on('hidden.bs.modal', function () {
        let form = $(this).find('form');
        form.validate().resetForm();
        form[0].reset();
    });

    $('#newuser_box').on('hidden.bs.collapse', function () {
        let form = $(this).find('form');
        form.validate().resetForm();
        form[0].reset();
    });

    $(document.body).on('click', '.remove_usr', function () {
        let uid = $(this).attr('id').split("_")[2];

        bootbox.confirm({
            title: "Remove switch?",
            message: gettext('Are you sure you want to delete this user?'),
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    removeUser(uid);
                }
            }
        });
    });

});
