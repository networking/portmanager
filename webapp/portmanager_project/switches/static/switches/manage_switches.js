//ajax calls optimizing loading times by getting only necessary data from database
function load_switch_groups(switch_id, callback) {
    /*
    gets groups assigned to selected switch and continue loading of groups by callback
    */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let switch_data = JSON.parse(xmlhttp.responseText);

            if (switch_data['error']) {
                create_notification(switch_data['error']);
            } else {
                callback(switch_data, switch_id);
            }
        }
    };

    xmlhttp.open("POST", "load_switch_groups.py", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["switch_id", switch_id]]);
    xmlhttp.send(send_text);
}

function generate_switch_groups(data, switch_id) {
    /*
    collects data of switch groups and id by being callback,
    then collects data about all groups,
    then triggers generating of html
    */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let switch_group_data = JSON.parse(xmlhttp.responseText);

            if (switch_group_data['error']) {
                create_notification(switch_group_data['error']);
            } else {
                let switch_owners = data[0]["fields"].owners;

                for (let i = 0; i < switch_group_data.length; i++) {
                    let tmp_group = switch_group_data[i];
                    let is_active = $.inArray(tmp_group.pk, switch_owners) !== -1;

                    let newGroup = new Option(tmp_group["fields"].name, tmp_group.pk, is_active, is_active);
                    $("#switch-groups-select_" + switch_id).append(newGroup).trigger('change');
                }
            }
        }
    };

    xmlhttp.open("POST", "generate_all_groups.py", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["switch_id", switch_id]]);
    xmlhttp.send(send_text);
}


//functions for insertion/deletion of selected elements to database
function add_switch(name, address, loc, device, groups, community, snmp, poe_regex, ssh_key) {
    /*
        Add new switch to database.
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                create_alert(gettext("Switch was successfully added."));
            } else {
                create_alert(responseText.split(" ").slice(1));
            }
        }
    };
    console.log(groups.join());

    xmlhttp.open('POST', 'create_switch', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["name", name],
        ['address', address],
        ['loc', loc],
        ['device', device],
        ['groups', groups.join()],
        ['community', community],
        ['snmp', snmp],
        ['poe_regex', poe_regex],
        ['ssh_key', ssh_key],
    ]);

    xmlhttp.send(send_text);
}

function add_location(name, street, city) {
    /*
        Add new location to database
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_alert(gettext("Location was successfully added."))
            } else {
                create_alert(responseText.split(" ").slice(1))
            }
        }
    };

    xmlhttp.open('POST', 'create_location', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["name", name],
        ['street', street],
        ['city', city]
    ]);

    xmlhttp.send(send_text);
}

function add_device(manufacturer, model, note) {
    /*
        Add new device to database
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_alert(gettext("Device was successfully added."));
            } else {
                create_alert(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open('POST', 'create_device', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["manufacturer", manufacturer],
        ['model', model],
        ['note', note]
    ]);

    xmlhttp.send(send_text);
}

function remove_switch(id_switch) {
    console.log(id_switch);
    /*
        Removes switch
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_alert(gettext("Switch was successfully removed."));
            } else {
                create_alert(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open('POST', 'remove_switch', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ['id_switch', id_switch]
    ]);

    xmlhttp.send(send_text);
}


//functions responsible for operations with switch
function change_switch_name(id_switch, new_name) {
    /*
    Function to change switch name in real time.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                $("#a-title_" + id_switch).text("ID" + id_switch + " " + new_name);
                create_notification(gettext("Switch name successfully changed."));
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "update_switch_name", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["name", new_name],
        ["switch_id", id_switch]]);
    xmlhttp.send(send_text);
}

function change_switch_location(id_switch, new_location) {
    /*
    Function to change switch location in real time.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_alert(gettext("Switch location successfully changed."));
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "update_switch_location", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["location", new_location],
        ["switch_id", id_switch]]);
    xmlhttp.send(send_text);
}

function change_switch_device(id_switch, device_type) {
    /*
    Function to change switch device type in real time.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification(gettext("Switch device type successfully changed."));
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "update_switch_device", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["device", device_type],
        ["switch_id", id_switch]]);
    xmlhttp.send(send_text);
}

function change_statement_on_switch(id_switch, id_group, mode) {
    /*
        Remove or add group to the switch according to mode.
    */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                if (mode === 0) {
                    create_notification(gettext("Group was successfully added to switch definition"));
                } else {
                    create_notification(gettext("Group was successfully removed from switch definition"));
                }

            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open('POST', 'change_statement', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch],
        ['id_group', id_group],
        ['mode', mode]]);

    xmlhttp.send(send_text);
}

function change_switch_address(id_switch, new_address) {
    /*
    Function to change switch address at manage_switches in real time.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification(gettext("Switch address successfully changed."));
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "update_switch_address", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["address", new_address],
        ["switch_id", id_switch]]);
    xmlhttp.send(send_text);
}

function change_switch_community_string(id_switch, new_community_string) {
    /*
    Function to change switch community_string at manage_switches in real time.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification(gettext("Switch community string successfully changed."));
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "update_switch_community_string", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["community_string", new_community_string],
        ["switch_id", id_switch]]);
    xmlhttp.send(send_text);
}

function change_switch_poe_regex(id_switch, poe_regex) {
    /*
    Function to change switch community_string at manage_switches in real time.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification(gettext("PoE regex successfully changed."));
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "change_poe_regex", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["poe_regex", poe_regex],
        ["switch_id", id_switch]]);
    xmlhttp.send(send_text);
}

function change_switch_ssh_key(id_switch, ssh_key) {
    /*
    Function to change switch ssh key at manage_switches in real time.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification(gettext("SSH key successfully changed."));
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "change_ssh_key", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["ssh_key", ssh_key],
        ["switch_id", id_switch]]);
    xmlhttp.send(send_text);
}

function change_switch_snmp(id_switch, snmp_version) {
    /*
    Function to change switch device type in real time.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification(gettext("Switch snmp version successfully changed."));
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "update_switch_snmp", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["snmp", snmp_version],
        ["switch_id", id_switch]]);
    xmlhttp.send(send_text);
}

function show_switch_definitions(id_switch) {
    /*
    Function to change switch device type in real time.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification(gettext("Switch snmp version successfully changed."));
            } else {
                create_notification(responseText);
            }
        }
    };

    xmlhttp.open("POST", "show_switch_definitions", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["switch_id", id_switch]]);
    xmlhttp.send(send_text);
}


$(document).ready(function () {
    $('.select2box').select2();
    let switch_groups = $('.switch-groups');
    let enter_to_input = $('.enter-to-input');


    //toggle collapsed box and load data if not loaded
    $('.a-title').click(function () {
        let switch_id = $(this).attr("id").split("_")[1];
        let element = $("#switch-box_" + switch_id);

        if (element.hasClass("load-data")) {
            element.removeClass("load-data");

            //loads groups options for switch and its owners
            load_switch_groups(switch_id, generate_switch_groups);
        }

        element.boxWidget('toggle');
    });


    $('.switch-location').on('select2:select', function (e) {
        let switch_id = $(this).attr('id').split('_')[1];
        let location = e.params.data.id;

        change_switch_location(switch_id, location)
    });

    $('.switch-devices').on('select2:select', function (e) {
        let switch_id = $(this).attr('id').split('_')[1];
        let device = e.params.data.id;

        change_switch_device(switch_id, device)
    });

    $('.switch-snmp').on('select2:select', function (e) {
        let switch_id = $(this).attr('id').split('_')[1];
        let snmp_version = e.params.data.id;

        change_switch_snmp(switch_id, snmp_version)
    });

    switch_groups.on('select2:select', function (e) {
        let switch_id = $(this).attr('id').split('_')[1];
        let group = e.params.data.id;

        change_statement_on_switch(switch_id, group, 0)
    });

    switch_groups.on('select2:unselect', function (e) {
        let switch_id = $(this).attr('id').split('_')[1];
        let group = e.params.data.id;

        change_statement_on_switch(switch_id, group, 1)
    });

    //functionality for editing address/community-string when enter is smashed while focus is at input
    enter_to_input.on('keypress', function (e) {

        if (e.which === 13) {
            e.preventDefault();

            let id_switch = $(this).attr("id").split("_")[1];
            $(this).attr("disabled", "disabled");


            if ($(this).hasClass("community-string")) {
                //editing community-string
                let new_community_string = $('#community-string-input_' + id_switch).val();

                if (new_community_string !== "") {
                    change_switch_community_string(id_switch, new_community_string);
                    $("#community-string-label_" + id_switch).hide();
                } else {
                    create_notification(gettext("Community string cannot be blank!"));
                }
            } else if ($(this).hasClass("poe-regex")) {
                let new_regex = $('#edit-poe-regex-input_' + id_switch).val();
                if (new_regex !== "") {
                    if (!/^(\[)(([0-9]*),?|([0-9]*)-([0-9]*),?)*(\])$/.test(new_regex)) {
                        create_notification("Regex is not valid.");
                    } else {
                        change_switch_poe_regex(id_switch, new_regex);
                    }
                }
            } else if ($(this).hasClass("ssh-key")) {
                let new_key = $('#edit-ssh-key-input_' + id_switch).val();
                if (new_key !== "") {
                    change_switch_ssh_key(id_switch, new_key);
                }
            } else {
                //editing address
                let new_address = $('#sw-address-input_' + id_switch).val();

                if (new_address !== "") {
                    change_switch_address(id_switch, new_address);

                    $("#address-label_" + id_switch).hide();
                } else {
                    create_notification(gettext("Address cannot be blank!"));
                }
            }
        }

        $(this).removeAttr("disabled");
    });

    //shows information label
    enter_to_input.on('click', function () {
        let id_switch = $(this).attr("id").split("_")[1];

        if ($(this).hasClass("community-string")) {
            $("#community-string-label_" + id_switch).show();
        } else {
            $("#address-label_" + id_switch).show();
        }
    });

    //edit name of switch
    $('.edit-name').on('click', function () {
        let switch_id = $(this).attr('id').split('_')[1];
        let actual_name = $('#a-title_' + switch_id).text().trim().split(" ")[1];

        bootbox.prompt({
            size: "medium",
            title: gettext('Enter new name of the switch'),
            value: actual_name,
            callback: function (result) {
                if (result) {
                    change_switch_name(switch_id, result);
                }
            }
        })
    });


    //removes switch
    $('.remove-switch').click(function () {
        let switch_id = $(this).attr("id").split("_")[1];

        bootbox.confirm({
            title: gettext("Remove switch"),
            message: gettext('Are you sure you want to delete this switch?'),
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i>' + gettext('Cancel')
                },
                confirm: {
                    label: '<i class="fa fa-check"></i>' + gettext('Confirm')
                }
            },
            callback: function (result) {
                if (result) {
                    remove_switch(switch_id);
                }
            }
        });
    });

    //create new switch
    $('.create_switch').on('click', function () {
        let form = $("#newSwitchForm");
        form.valid();

        let loc = [];
        let devices = [];
        let groups = [];

        loc.push($("#switch-location-select").select2("val"));
        devices.push($("#switch-devices-select").select2("val"));
        groups.push($("#switch-groups-select").select2("val"));

        if (form.valid()) {
            add_switch($('#switch_name').val(),
                $('#switch_address').val(),
                loc,
                devices,
                groups,
                $('#switch_community').val(),
                $('#switch_snmp').val(),
                $('#add_port_poe_regex').val(),
                $('#add_ssh_key').val());
        }
    });

    //validation form for new switch creation
    $("#newSwitchForm").validate({
        rules: {
            switch_name: {
                required: true,
            },

            switch_address: {
                required: true
            },

            switch_community: {
                required: true
            },

            switch_snmp: {
                required: true,
                digits: true,
                range: [2, 3]
            }

        },
        messages: {
            switch_name: {
                required: gettext("Enter switch name")
            },
            switch_address: gettext("Set switch address"),
            switch_community: gettext("Set community string"),
            switch_snmp: gettext("Set correct SNMP version")
        },
        highlight: function (element) {
            $(element).closest(".form-control-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            element
                .text("OK!").addClass("valid")
                .closest(".control-group").removeClass("error").addClass("success");
            $(element).closest(".form-control-group").removeClass("has-error").addClass("has-success");
        }
    });

    //creates new location
    $('#add_location_save').click(function () {
        add_location($('#location_name').val(),
            $('#location_street').val(),
            $('#location_city').val());
    });

    //creates new device
    $('#add_device_save').click(function () {
        add_device($('#device_manufacturer').val(),
            $('#device_model').val(),
            $('#device_note').val());
    });

    $('.show-defs').click(function () {
        let switch_id = $(this).attr("id").split("_")[1];
        show_switch_definitions(switch_id);
    });
});
