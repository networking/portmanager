function create_notification(text) {
    /*
    Bootbox used to create notification
     */
    bootbox.dialog({
        message: gettext(text),
        onEscape: true,
        backdrop: true
    });
}

function create_alert(text) {
    /*
    Bootbox used to create notification
     */
    bootbox.alert({
        message: gettext(text),
        callback: function () {
            location.reload();
        },
        onEscape: true,
        backdrop: true
    });
}

function getCookie(name) {
    /* Method used to get a cookie value according to the request
     * used only for authorization based on the csrf_token passed in the header
    */
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function makePOSTRawText(array) {
    /* Function for the creation of string that is handed over when sending AJAX request.
    *  It creates strandard string, AJAX adds it to the header and server part is then able to
    *  download the data from the header.
    */
    let raw_text = "";

    for (let i = 0; i < array.length; i++) {
        raw_text += array[i][0] + "=" + array[i][1] + "&";
    }
    return raw_text.substring(0, raw_text.length - 1);
}

function change_lang(language) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            location.reload();
        }
    };
    console.log(language);
    xmlhttp.open("POST", "i18n/setlang/", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["language", language],
    ]);
    xmlhttp.send(send_text);
}


$(document).ready(function () {
    $(function () {
        let url = window.location.pathname;
        $(".sidebar-navbar-collapse ul li a").each(function () {
            if ($(this).attr("href") === url || $(this).attr("href") === '') {
                $(this).parent().addClass("active");
            }
        });
    });

    $(document).delegate("#change_lang", "click", function () {
        let current_lang = $('#current_lang').val();
        if (current_lang === "en/") {
            change_lang('cs');
        } else {
            change_lang('en');
        }
    });

    var url = window.location;
    var element = $('ul.sidebar-menu a').filter(function () {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).parent().addClass('active');
    if (element.is('li')) {
        element.addClass('active').parent().parent('li').addClass('active')
    }
    //$('.treeview-menu').tree()

    //$('ul').on('expanded.tree', {
    //})
})
;
