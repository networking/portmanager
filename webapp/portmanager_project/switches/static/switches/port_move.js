function transfer_port_settings(src_switch_id, src_port_id, dst_switch_id, dst_port_id) {

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var responseText = xmlhttp.responseText;
            if (responseText == "0") {
                alert("Port settings was transfered successfully");
            }
            else {
                alert(responseText);
            }
        }
    };

    xmlhttp.open("POST", "transfer_port_settings", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");

    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["src_switch_id", src_switch_id],
        ["src_port_id", src_port_id],
        ["dst_switch_id", dst_switch_id],
        ["dst_port_id", dst_port_id]
    ]);
    xmlhttp.send(send_text);
}

function show_switches(locality_id) {

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {

            var data = JSON.parse(xmlhttp.responseText);
            var src_dev = document.getElementById('sel_src_dev');
            var dst_dev = document.getElementById('sel_dst_dev');


            var opt_src = document.createElement('option');
            $(opt_src).attr('class', 'src_dev');
            $(opt_src).attr('id', 'src_dev_none');
            src_dev.appendChild(opt_src);
            var opt_dst = document.createElement('option');
            $(opt_dst).attr('class', 'dst_dev');
            $(opt_dst).attr('id', 'dst_dev_none');
            dst_dev.appendChild(opt_dst);

            for (var i = 0; i < data.length; i++) {
                var id = data[i]['id_switch'];
                var name = data[i]['switch_name'];
                var opt_src = document.createElement('option');
                $(opt_src).attr('class', 'src_dev');
                $(opt_src).attr('id', 'src_dev_' + id);
                $(opt_src).html(name);
                src_dev.appendChild(opt_src);
                var opt_dst = document.createElement('option');
                $(opt_dst).attr('class', 'dst_dev');
                $(opt_dst).attr('id', 'dst_dev_' + id);
                $(opt_dst).html(name);
                dst_dev.appendChild(opt_dst);
            }
        }
    };

    xmlhttp.open("POST", "get_switches_by_location", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");

    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["locality_id", locality_id]
    ]);

    xmlhttp.send(send_text);
}

function show_src_port(switch_id) {
    var xmlhttp = new XMLHttpRequest();
    var element = $('#sel_src_port');
    element.hide();
    element.parent().append('<div class="loaderPort loaderPortSrc"></div>');
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            $(".loaderPortSrc").remove();
            element.show();
            var data = JSON.parse(xmlhttp.responseText);
            var error = data['error'];
            if (error) {
                var msg = gettext('An error occurred while obtaining information from the switch.') +
                    error.toString();
                alert(msg);
            } else {
                var ports = data['ports'];
                var src_port = document.getElementById('sel_src_port');
                for (var i = 0; i < ports.length; i++) {
                    var name = ports[i][0];
                    var alias;
                    var id = ports[i][6];
                    var vlan_id = ports[i][7];
                    var vlan_name = ports[i][8];
                    if (!(ports[i][1] === null)) {
                        alias = ports[i][1];
                    } else {
                        alias = 'None';
                    }
                    var opt_src = document.createElement('option');
                    $(opt_src).attr('class', 'src_port');
                    $(opt_src).attr('id', 'src_port_' + id);
                    $(opt_src).html(name + ' Description: ' + alias + ' VLAN: ' + vlan_id + ' ' + vlan_name);
                    src_port.appendChild(opt_src);
                }
            }
        }
    };

    xmlhttp.open("POST", "get_switch_information", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");

    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id", switch_id]
    ]);

    xmlhttp.send(send_text);
}

function show_dst_port(switch_id) {
    var xmlhttp = new XMLHttpRequest();
    var element = $('#sel_dst_port');
    element.hide();
    element.parent().append('<div class="loaderPort loaderPortDst"></div>');

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            $(".loaderPortDst").remove();
            element.show();
            var data = JSON.parse(xmlhttp.responseText);
            var error = data['error'];
            if (error) {
                var msg = gettext('An error occurred while obtaining information from the switch.') +
                    error.toString();
                alert(msg);
            } else {
                var ports = data['ports'];
                var dst_port = document.getElementById('sel_dst_port');
                for (var i = 0; i < ports.length; i++) {
                    var name = ports[i][0];
                    var alias;
                    var id = ports[i][6];
                    var vlan_id = ports[i][7];
                    var vlan_name = ports[i][8];
                    if (!(ports[i][1] === null)) {
                        alias = ports[i][1];
                    } else {
                        alias = 'None';
                    }
                    var opt_dst = document.createElement('option');
                    $(opt_dst).attr('class', 'dst_port');
                    $(opt_dst).attr('id', 'src_port_' + id);
                    $(opt_dst).html(name + ' Description: ' + alias + ' VLAN: ' + vlan_id + ' ' + vlan_name);
                    dst_port.appendChild(opt_dst);
                }
            }
        }
    };

    xmlhttp.open("POST", "get_switch_information", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");

    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id", switch_id]
    ]);

    xmlhttp.send(send_text);
}

function unlockFile(file) {
    /*
        funkce ktera odesila, ze ma odemknout [smazat] konkretni soubor na serveru
        byva ve tvaru switchid_port_user.lock
     */
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var responseText = xmlhttp.responseText;
            if (responseText === "1") {
                console.log('Unable to unlock resource');
            }
        }
    };
    xmlhttp.open("POST", "unlockFile", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["file", file]
    ]);
    xmlhttp.send(send_text);
}

function lockFile(file) {
    /*
        funkce ktera odesila, ze ma zamknout [smazat] konkretni soubor na serveru
        byva ve tvaru switchid_port_user.lock
    */
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var responseText = xmlhttp.responseText;
            if (responseText === "1") {
                console.log('Unable to lock resource');
            }
        }
    };

    xmlhttp.open("POST", "lockFile", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["file", file]
    ]);
    xmlhttp.send(send_text);
}

$(document).ready(function () {

    var locality_id = $('#sel1').find(':selected').attr('id').split('_')[1];
    $('#sel_src_dev').empty();
    $('#sel_src_port').empty();
    $('#sel_dst_dev').empty();
    $('#sel_dst_port').empty();

    show_switches(locality_id);

    $(document).on('click', '.locality', function () {
        var locality_id = $('#sel1').find(':selected').attr('id').split('_')[1];
        $('#sel_src_dev').empty();
        $('#sel_src_port').empty();
        $('#sel_dst_dev').empty();
        $('#sel_dst_port').empty();
        show_switches(locality_id);
    });

    $(document).on('click', '.src_dev', function () {
        var switch_id = $('#sel_src_dev').find(':selected').attr('id').split('_')[2];
        $('#sel_src_port').empty();
        if (switch_id != 'none') {
            show_src_port(switch_id);
        }
    });

    $(document).on('click', '.dst_dev', function () {
        var switch_id = $('#sel_dst_dev').find(':selected').attr('id').split('_')[2];
        $('#sel_dst_port').empty();
        if (switch_id != 'none') {
            show_dst_port(switch_id);
        }
    });

    $(document).on('click', '#submit_transfer', function () {
        var src_switch_id = $('#sel_src_dev').find(':selected').attr('id').split('_')[2];
        var src_port_id = $('#sel_src_port').find(':selected').attr('id').split('_')[2];
        var dst_switch_id = $('#sel_dst_dev').find(':selected').attr('id').split('_')[2];
        var dst_port_id = $('#sel_dst_port').find(':selected').attr('id').split('_')[2];
        transfer_port_settings(src_switch_id, src_port_id, dst_switch_id, dst_port_id);
    });

});