//ajax calls optimizing loading times by getting only necessary data from database
function load_vlan_groups(def_id, callback) {
    /*
    Load groups of selected vlan by Ajax and execute adding them as selected by callback
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let vlan_data = JSON.parse(xmlhttp.responseText);

            if (vlan_data['error']) {
                create_notification(vlan_data['error']);
            } else {
                callback(vlan_data, def_id);
            }
        }
    };

    xmlhttp.open("POST", "load_portvlan_data.py", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", "V"],
        ["def_id", def_id]]);
    xmlhttp.send(send_text);
}

function load_vlan_switches(def_id) {
    /*
    Load switches of selected vlan by Ajax and add them as selected options
     */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let available_switches = JSON.parse(xmlhttp.responseText);

            if (available_switches['error']) {
                create_notification(available_switches['error']);
            } else {
                for (let i = 0; i < available_switches.length; i++) {

                    let tmp_switch = available_switches[i];

                    let newSwitch = new Option(tmp_switch["fields"].name, tmp_switch.pk, false, false);
                    $("#switches-select_" + def_id).append(newSwitch).trigger('change');
                }
            }
        }
    };

    xmlhttp.open("POST", "load_switches_available_for_portvlan.py", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", "V"],
        ["def_id", def_id]]);
    xmlhttp.send(send_text);
}

function create_vlans_group_options(data, def_id) {
    /*
    Add all groups as options while getting selected one from load_vlan_groups by being callback
    */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let vlan_group_data = JSON.parse(xmlhttp.responseText);

            if (vlan_group_data['error']) {
                create_notification(vlan_group_data['error']);
            } else {
                data = data[0]["fields"].groups;

                for (let i = 0; i < vlan_group_data.length; i++) {
                    let tmp_group = vlan_group_data[i];
                    let is_active = $.inArray(tmp_group.pk, data) !== -1;

                    let newGroup = new Option(tmp_group["fields"].name, tmp_group.pk, is_active, is_active);
                    $("#groups-select_" + def_id).append(newGroup).trigger('change');
                }
            }
        }
    };

    xmlhttp.open("POST", "generate_all_groups.py", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf]]);
    xmlhttp.send(send_text);
}


//functions responsible for operations with vlans
function add_device_to_vlan_def(def_id, switch_id) {
    /*
    Add new device to selected vlan
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                create_notification("Switch successfully added to vlan definition.");
            } else {
                create_alert(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "add_device_to_portvlan_def", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", "V"],
        ["def_id", def_id],
        ["switch_id", switch_id]]);
    xmlhttp.send(send_text);
}

function remove_device_from_vlan_def(def_id, switch_id) {
    /*
    Removes device from selected vlan
     */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                create_notification("Switch successfully removed from vlan definition.");
            } else {
                create_alert(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "remove_device_from_portvlan_def", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["def_id", def_id],
        ["pv", "V"],
        ["switch_id", switch_id]]);
    xmlhttp.send(send_text);
}

function add_group_to_vlan_def(def_id, gid) {
    /*
    Add group to selected vlan
     */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                create_notification("Group successfully added to vlan definition.");
            } else {
                create_alert(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "add_group_to_portvlan_def", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["def_id", def_id],
        ["pv", "V"],
        ["gid", gid]]);
    xmlhttp.send(send_text);
}

function remove_group_from_vlan_def(def_id, gid) {
    /*
    Removes group from selected vlan.
     */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                create_notification("Group successfully removed from vlan definition.");
            } else {
                create_alert(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "delete_group_from_portvlan_def", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["def_id", def_id],
        ["pv", "V"],
        ["gid", gid],]);
    xmlhttp.send(send_text);
}

function edit_vlan_name(def_id, name_new) {
    /*
    Change name of the selected vlan
     */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification("Vlan name successfully changed.");
                $("#a-title_" + def_id).text(name_new);
            }
            else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "save_portvlan_def_name", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["def_id", def_id],
        ["pv", "V"],
        ["name_new", name_new],]);
    xmlhttp.send(send_text);
}

function change_visibility(def_id) {
    /*
    Changes readOnly attribute of selected vlan
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                create_notification("Visibility successfully changed.");
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "change_visibility", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", "V"],
        ["id", def_id]]);
    xmlhttp.send(send_text);
}

function edit_regex(def_id, regex) {
    /*
    Change regex of selected vlan
     */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                create_notification("Regex successfully changed.");
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "change_regex", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", "V"],
        ["id", def_id],
        ["regex", regex],
    ]);
    xmlhttp.send(send_text);

}

function add_vlan_def(name, switches, groups, readOnly, regex) {
    /*
    Create new vlan definition
    */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                create_alert("Vlan successfully created.");
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "add_portvlan", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["name", name],
        ["switches", switches],
        ["groups", groups],
        ["readOnly", readOnly],
        ["regex", regex],
        ["pv", "V"]]);
    xmlhttp.send(send_text);
}

function remove_vlan_def(def_id) {
    /*
	Removes vlan definition
	*/

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;

            if (responseText === "0") {
                create_alert("Vlan successfully removed.");
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "remove_portvlan", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["pv", "V"],
        ["id", def_id]]);
    xmlhttp.send(send_text);
}



$(document).ready(function () {
    $('.select2box').select2();
    let select_switch = $('.switches-select');
    let select_group = $('.groups-select');


    //adding device to vlan when selected
    select_switch.on('select2:select', function (e) {
        let def_id = $(this).attr('id').split('_')[1];
        let switch_id = e.params.data.id;

        add_device_to_vlan_def(def_id, switch_id)
    });

    //removing device from vlan when unselected
    select_switch.on('select2:unselect', function (e) {
        let def_id = $(this).attr('id').split('_')[1];
        let switch_id = e.params.data.id;

        remove_device_from_vlan_def(def_id, switch_id)
    });

    //adding group to vlan when selected
    select_group.on('select2:select', function (e) {
        let def_id = $(this).attr('id').split('_')[1];
        let group_id = e.params.data.id;

        add_group_to_vlan_def(def_id, group_id);
    });

    //removing group from vlan when unselected
    select_group.on('select2:unselect', function (e) {
        let def_id = $(this).attr('id').split('_')[1];
        let group_id = e.params.data.id;

        remove_group_from_vlan_def(def_id, group_id);
    });


    //edit label of vlan
    $(document.body).on('click', '.edit_label', function () {
        let vlan_id = $(this).attr('id').split('_')[1];
        let actual_name = $('#a-title_' + vlan_id).text();
        actual_name = actual_name.trim()

        bootbox.prompt({
            size: "medium",
            title: gettext('Enter new label of the vlan'),
            value: actual_name,
            callback: function (result) {
                if (result) {
                    edit_vlan_name(vlan_id, result);
                }
            }
        })
    });

    //confirm box for removing vlan
    $('.remove_def').click(function () {
        /*
        removes vlan if true/false dialog is answered positively
        */
        let vlan_id = $(this).attr("id").split("_")[1];

        bootbox.confirm({
            title: "Remove vlan?",
            message: gettext('Are you sure you want to delete this vlan?'),
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    remove_vlan_def(vlan_id)
                }
            }
        });
    });


    //create vlan form and validation
    $(".create_vlan").click(function () {

        let form = $("#newVlanForm");
        let switches = $('.switches-select-newVlan').select2("val");
        let groups = $('.groups-select-newVlan').select2("val");

        form.valid();

        if (switches === null) {
            switches = []
        }

        if (groups === null) {
            groups = []
        }

        if (form.valid()) {
            add_vlan_def($('#add_vlan_name').val(), switches, groups,
                $('#add_vlan_visibility').is(':checked'),
                $('#add_vlan_regex').val());
        } else {
            create_notification("You have not filled out all the data correctly.");
        }
    });


    //functionality for editing regex when enter is smashed while focus is at input
    $('.enter-to-input').on('keypress', function (e) {

        if(e.which === 13){
            e.preventDefault();

            let def_id = $(this).attr("id").split("_")[1];
            $(this).attr("disabled", "disabled");
            let new_regex = $('#edit-regex-input_' + def_id).val();

            if (!/^(\[)(([0-9]*),?|([0-9]*)-([0-9]*),?)*(\])$/.test(new_regex)) {
                create_notification("Regex is not valid.");
            } else {
                edit_regex(def_id, new_regex);
            }
        }

        $(this).removeAttr("disabled");
    });


    $('.edit-ro').on('click', function () {
        change_visibility($(this).attr("id").split("_")[1]);
    });


    $.validator.addMethod('regexValid', function (value,element) {
        let regex = document.getElementById(element.id);
        if(regex && regex.value.match(/^(\[)(([0-9]*),?|([0-9]*)-([0-9]*),?)*(\])$/))
        {
            return true;
        }
    }, '{% trans "Wrong regular expression"%}');

    $("#newVlanForm").validate({
        rules: {
            add_vlan_name: {
                required: true
            },

            add_vlan_regex: {
                regexValid: true
            }
        },
        messages: {
            add_vlan_name: gettext("Enter name")
        },
        highlight: function(element) {
            $(element).closest('.form-control-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            element
                .text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
            $(element).closest('.form-control-group').removeClass('has-error').addClass('has-success');
        }
    });

    //toggle collapsed box and load data if not loaded already
    $('.a-title').click(function () {
        let vlan_id = $(this).attr("id").split("_")[1];
        let element = $("#widget-vlan_" + vlan_id);

        if (element.hasClass("load_data")) {
            element.removeClass("load_data");

            load_vlan_switches(vlan_id);
            load_vlan_groups(vlan_id, create_vlans_group_options);
        }

        element.boxWidget('toggle');
    });
});