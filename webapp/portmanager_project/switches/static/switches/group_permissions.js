function create_true_false_dialog(msg) {
    /*
	tato funkce je vysvetlena v souboru manage_switches.js
    */ 

    var answer = confirm(msg);
    if (answer) {
        return true;
    } else {
        return false;
    }

}



function get_switch_permissions(switch_id, group_id, override){
        /*
         * funkce ktera odesle zmenu aliasu na server
        */

        /* mode definuje chovani funkce */
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                console.log(xmlhttp.responseText);
                var data = JSON.parse(xmlhttp.responseText);
                var element = document.createElement('table');

                for (var i = 0; i < data.length; i++){
                    var id = data[i][0];
                    var descr = data[i][1];
                    var visibility = data[i][2];
                    var trunk = data[i][3];

                    var row = document.createElement('tr');
                    var radio = document.createElement('select');

                    $(radio).attr('name', visibility + '_' + trunk);

                    var nothing = document.createElement('option');
                    var read = document.createElement('option');
                    var write = document.createElement('option');

                    $(nothing).attr('class', 'nothing');    
                    $(read).attr('class', 'read');
                    $(write).attr('class', 'write');

                    $(nothing).html(gettex('no permission'));
                    $(read).html(gettex('read only'));
                    $(write).html(gettex('full access'));
                

                    var legend = document.createElement('div');
                    var select_all = document.createElement('div');
                    var deselect_all = document.createElement('div');
                    var default_all = document.createElement('div');
                    var deselect_trunks = document.createElement('div');
                                       
                    $(legend).attr('class', 'legend');
                    $(select_all).attr('class', 'legend_item select_all');
                    $(deselect_all).attr('class', 'legend_item deselect_all');
                    $(default_all).attr('class', 'legend_item default_all');
                    $(deselect_trunks).attr('class', 'legend_item deselect_trunks');
                    
                    $(select_all).html(gettext('mark all'));
                    $(deselect_all).html(gettext('unmark all'));
                    $(default_all).html(gettext('set default'));
                    $(deselect_trunks).html(gettext('unmark trunks'));

                    legend.appendChild(default_all);
                    legend.appendChild(deselect_trunks);
                    legend.appendChild(deselect_all);
                    legend.appendChild(select_all);


                    if (!override){
                        if (visibility === 0){
                            nothing.selected = true;
                        }
                        else if (visibility == 1){
                            read.selected = true;
                        }
                        else{
                            write.selected =true;
                        }
                    }else{
                        write.selected = true;
                    }
                    radio.appendChild(nothing);
                    radio.appendChild(read); 
                    radio.appendChild(write);

                    $(row).attr('class', 'visibility_' + visibility);


                    var id_column = document.createElement('td');
                    $(id_column).html(id);
                    $(id_column).attr('id', switch_id);
                    $(id_column).attr('class', 'switch_id');
                    var descr_column = document.createElement('td');
                    $(descr_column).html(descr);
                    row.appendChild(id_column);
                    row.appendChild(descr_column);
                    row.appendChild(radio);
                    element.appendChild(row);
                }

                $('#print_' + switch_id + '_' + group_id).html(element);
                $('#print_' + switch_id + '_' + group_id).append(legend);
                $('#print_' + switch_id + '_' + group_id).parent().show();
            }
        };
        xmlhttp.open("POST", "get_switch_group_permission", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var csrf = getCookie("csrftoken");
 
        send_text = makePOSTRawText([
            ["csrfmiddlewaretoken", csrf],
            ["group_id", group_id],
            ["switch_id", switch_id]
        ]);

        xmlhttp.send(send_text);
}

function actualize_switch_control(switch_id, group_id, mode){
    /*
 *  mode rozhoduje jestli uzivatel klikl na checkbox a nebo jen na listu
 *  */
    var element = $('#group_' + group_id + '_' + switch_id);
    var control_element = $(element).parent().find('.switch_control');

    var is_it_open = $(control_element).is(':visible');

    if (mode === true){
        // zde je pripad ze uzivatel kliknul na checkbox,
        // v tomto pripade ocekavame, ze u konkretniho zarizeni
        // "prevratime" hodnoty z niceho na zapisovaci
        // 
        var checked = $(element).find('input').is(':checked');

        //console.log(checked);
        if (is_it_open){
            //console.log('jdu to prehodit hombre');
            // pokud jsou hodnoty jiz nacteny, neni nutne volat AJAX pro zisakni
            // portu znova a staci hodnoty pouze "prevrati
            if (checked){
                $(control_element).find('select').prop('selectedIndex', 2);
                //$(control_element).find('input:checkbox'). = true;
            }
            else{
                $(control_element).find('select').prop('selectedIndex', 0);
                //$(control_element).find('input:checkbox'). = false;
            }
        }else
            {
            if (checked){
                get_switch_permissions(switch_id, group_id, true);
            }
            else{
                get_switch_permissions(switch_id, group_id, false);
            }
        }
    } else{
        if (is_it_open){
            
            return 0;
        }
        else{
            get_switch_permissions(switch_id, group_id, false);
        }
    }
}


$(document).ready(function () {
    /*
	
	metody hide(), hideAll(), hightlight()  

    */
    $('.switches').hide();
    $('.switches').first().show();

    $('.group').click(function(){
        var id = $(this).attr('id').trim();
        $('.switches').hide();
        $('#switches_' + id).show();
    });

    $('.switch_label').click(function(event){
        var element = ($(event.target).prop('tagName')).trim();
        var group_id = $(this).attr('id').split('_')[1].trim();
        var switch_id = $(this).attr('id').split('_')[2].trim();
        var is_it_open = $(this).parent().find('.switch_control').is('visible');
        if (element === 'DIV'){
            actualize_switch_control(switch_id, group_id, false);
        }
        else{
            actualize_switch_control(switch_id, group_id, true);
        }
    });

    $(document).on("click", ".select_all", function(){
        $(this).parent().parent().find('select').prop('selectedIndex', 2);
    });

    $(document).on("click", ".deselect_all", function(){
        $(this).parent().parent().find('select').prop('selectedIndex', 0);
    });

    $(document).on("click", ".default_all", function(){
        // v attributu name mame ulozene, jaky byl vychozi stav
        //.attr('name');
        $(this).parent().parent().find('select').each(function(){
            var info = $(this).attr('name');
            var visibility = parseInt(info.split('_')[0]);

            $(this).prop('selectedIndex', visibility);
        });
    });

    $(document).on("click", ".deselect_trunks", function(){
        $(this).parent().parent().find('select').each(function(){
            var info = $(this).attr('name');
            var trunk = info.split('_')[1];
            console.log(trunk);
            if (trunk === 'true'){
                $(this).prop('selectedIndex', 0);
            }
        });
    });

    $('#submit').click(function(){
        var export_data = {};

        var group_id = $('.group_id:visible').attr('id');
        console.log(group_id);
        export_data['group_id'] = group_id;

        var list = [];
        $('.display_data:visible').each(function(){
            var temp_list = {};
            var data = {};
            var switch_id = $(this).attr('id').split('_')[1];
            var selects = $(this).find('select');
            var temp_hash = {};
            $(selects).each(function(){
                var port_id = $(this).parent().find('.switch_id').html();
                temp_hash[port_id] = $(this).prop('selectedIndex');
            });

            data['switch_id'] = switch_id;
            data['ports'] = temp_hash;
            list.push(data);
        });
        export_data['switches'] = list;
        var form = document.createElement('form');
        $(form).attr('method', 'post');
        $(form).attr('action', 'save_group_permission_configuration.html');
        var input = document.createElement('input');
        $(input).attr('name', 'data');
        $(input).attr('value', JSON.stringify(export_data));
        $(form).append(input);
        var csrf = getCookie("csrftoken");
        var csrf_input = document.createElement('input');
        $(csrf_input).attr('name', 'csrfmiddlewaretoken');
        $(csrf_input).attr('value', csrf);
        $(form).append(csrf_input);
        form.submit();
        });
        /*
        if (element === 'DIV'){
            if (is_it_open){
                get_switch_control(switch_id, group_id, 0);
            } else{
                get_switch_control(switch_id,group_id, 1);
            }
        }
        else{
            if ($(this).is('checked')){
                actualize_switch_control(switch_id, group_id, 1);
            }
            else{
                actualize_switch_control(switch_id, group_id, 2);
            }
        }
        });
    
    $('.switch_label').click(function(){
        var element_id = $(this).attr('id');
        var element_id = element_id.split('_'); 
        var switch_id = element_id[0];
        var user_id = element_id[1];
        get_switch_permissions(switch_id, user_id);
    });
    */
});




