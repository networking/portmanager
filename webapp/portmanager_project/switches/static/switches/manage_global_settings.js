function update_max_port_sec(new_num) {
    /*
    Function to change device default config.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification("Value successfully changed.");
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "update_max_port_sec", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["port_sec", new_num],
    ]);
    xmlhttp.send(send_text);
}

$(document).ready(function () {
    //saves new config value
    $('.btn-update-config').click(function () {
        update_max_port_sec(
            $('#default-config').val(),
        );
    });
});
