let textFile = null;
makeTextFile = function (text) {
    let data = new Blob([text], {type: 'text/plain'});

    // If we are replacing a previously generated file we need to
    // manually revoke the object URL to avoid memory leaks.
    if (textFile !== null) {
        window.URL.revokeObjectURL(textFile);
    }

    textFile = window.URL.createObjectURL(data);

    return textFile;
};


function export_csv(){

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function(){
        if(xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let export_data = JSON.parse(xmlhttp.responseText);

            if (export_data['error']) {
                bootbox.dialog({
                    message: export_data['error'],
                    onEscape: true,
                    backdrop: true
                });

                $(".loader").remove();
                $('.devices-select').val(null).trigger('change');
                $('.attributes-select').val(null).trigger('change');
            }
            else {
                let timeInMs = Date.now();
                let link = document.getElementById('downloadlink');

                link.href = makeTextFile(export_data['data']);
                link.setAttribute("download", "export" + timeInMs + ".csv");
                $(".loader").remove();

                let textAlert = gettext("Successfull export.");
                bootbox.alert({
                    message: textAlert,
                    callback: function () {
                        link.click();
                    },
                    onEscape: true,
                    backdrop: true
                });
            }
        }
    };

    xmlhttp.open('POST', 'export_csv', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    let devices = [];
    let features = ["device", "port"];

    devices.push($('.devices-select').val());
    features.push($('.attributes-select').val());

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["devices", devices],
        ["features", features],
    ]);

    xmlhttp.send(send_text);
}

$(document).ready(function () {
    $('.attributes-select').select2();
    $('.devices-select').select2();
    $('.form-control').select2();

    $( "#export-btn").click(function() {
        export_csv();
        $(this).parent().parent().append('<div class="loader"></div>');
    });
});