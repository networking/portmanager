let shouldICheck = true; // tato promena nam zajistuje jestli mame kontrolovat 'statement' lock souboru, meni se napriklad kdyz vychazime z prohlizece, zavirame okno atd.
let lock_files = []; //
let hidden, visibilityChange;
let key_hidder = '';
let refreshing_array = [];

let textFile = null;

makeTextFile = function (text) {
    let data = new Blob([text], {type: 'text/plain'});

    // If we are replacing a previously generated file we need to
    // manually revoke the object URL to avoid memory leaks.
    if (textFile !== null) {
        window.URL.revokeObjectURL(textFile);
    }

    textFile = window.URL.createObjectURL(data);

    return textFile;
};

function export_csv(devices) {

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            bootbox.hideAll()
            let export_data = JSON.parse(xmlhttp.responseText);

            if (export_data['error']) {
                bootbox.dialog({
                    message: export_data['error'],
                    onEscape: true,
                    backdrop: true
                });
            } else {
                let timeInMs = Date.now();
                let link = document.getElementById('export_download_link');


                link.href = makeTextFile(export_data['data']);

                link.setAttribute("download", "export_" + export_data['devices'] + '_' + timeInMs + ".csv");

                let textAlert = gettext("Successfull export.");
                bootbox.alert({
                    message: textAlert,
                    callback: function () {
                        link.click();
                    },
                    onEscape: true,
                    backdrop: true
                });
            }
        }
    };

    xmlhttp.open('POST', 'export_csv', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["devices", devices],
        ["features", ["device", "port", "description", "vlan_num", "vlan_name", "portsec", "state"]],
    ]);

    xmlhttp.send(send_text);
}

function get_poe_data(device) {

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            bootbox.hideAll()
            let poe_data = JSON.parse(xmlhttp.responseText);
            if (poe_data['error']) {
                bootbox.dialog({
                    message: poe_data['error'],
                    onEscape: true,
                    backdrop: true
                });
            } else {
                let textAlert = poe_data['data'].toString().split(',').join("<br />");
                bootbox.alert({
                    message: textAlert,
                    onEscape: true,
                    backdrop: true
                });
            }
        }
    };

    xmlhttp.open('POST', 'get_poe_data', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["device", device],
    ]);

    xmlhttp.send(send_text);
}


function isASCII(str) {
    /*
     funkce zkontroluje, jestli je text v ASCII
    */
    return /^[\x20-\x7F]*$/.test(str);
}

function get_connected_macs(switch_id, port_id, vlan_name) {
    let xmlhttp = new XMLHttpRequest();
    let $selected_interface = $(".get_connected_macs_button[name=" + switch_id + "_" + port_id + "_" + vlan_name + "]");
    let element = $selected_interface.parent();

    $selected_interface.hide();
    element.append('<div class="loader1"></div>');

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let text = xmlhttp.responseText;
            let data = JSON.parse(text);
            if (data.length > 0) {
                let mac_list = document.createElement("div");
                for (let i = 0; i < data.length; i++) {
                    let li = document.createElement("p");
                    $(li).html(data[i]).addClass("list-group-item");
                    mac_list.append(li);
                }
                bootbox.dialog({
                    message: mac_list,
                    onEscape: true,
                    backdrop: true
                })
            } else {
                bootbox.dialog({
                    message: gettext('There is no MAC address registered on this port.'),
                    onEscape: true,
                    backdrop: true
                })
            }

            $(".loader1").remove();
            $selected_interface.delay(1500).show();

        } else if (xmlhttp.readyState === 4 && xmlhttp.status === 503) {
            bootbox.dialog({
                message: gettext('Action wasn\'t successful. Service unavailable.'),
                onEscape: true,
                backdrop: true
            })
        }
    };


    xmlhttp.open("POST", "get_connected_macs", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");

    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", switch_id],
        ["id_port", port_id],
        ["vlan_name", vlan_name],
    ]);

    xmlhttp.send(send_text);
}

function change_alias(id_switch, port, port_name, new_alias) {
    /*
        funkce ktera odesle zmenu aliasu na server
    */

    if (!isASCII(new_alias)) {
        bootbox.hideAll();
        bootbox.dialog({
            message: gettext('Alias can contain only ASCII characters'),
            onEscape: true,
            backdrop: true
        });
        return;
    }
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            bootbox.hideAll();
            let response = xmlhttp.responseText;
            if (response === "0") {
                bootbox.dialog({
                    message: gettext('Successfully changed'),
                    onEscape: true,
                    backdrop: true
                })
            }
            if (response === "2") {
                bootbox.dialog({
                    message: gettext('You entered an empty string, there is no change'),
                    onEscape: true,
                    backdrop: true
                })
            }
            if (response === '1') {
                bootbox.dialog({
                    message: gettext('The change was not successful'),
                    onEscape: true,
                    backdrop: true
                })
            }
            if (response === '3') {
                bootbox.dialog({
                    message: gettext('Alias cannot contain non-ASCII characters'),
                    onEscape: true,
                    backdrop: true
                })
            }
            if (response === '4') {
                bootbox.dialog({
                    message: gettext('missing write permission, can\'t change port alias'),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "change_port_alias", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");

    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch],
        ["newalias", new_alias],
        ["port", port],
        ["port_name", port_name],
    ]);

    xmlhttp.send(send_text);
}

function turnonoff(id_switch, port) {
    /*
    funkce ktera vypina porty
    */

    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let text = xmlhttp.responseText;
            if (text === "0") {
                getChangeableInformation(id_switch, port);
            }
            if (text === "1") {
                bootbox.dialog({
                    message: gettext('missing write permission, turning the port on/off is not possible'),
                    onEscape: true,
                    backdrop: true
                })
            }
            if (text === '2') {
                bootbox.dialog({
                    message: gettext('unable to turn on/off port'),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "turn_on_off_port", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch],
        ["port", port]
    ]);
    xmlhttp.send(send_text);
}


function apply_default_config(id_switch, port_id, port_name) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            bootbox.hideAll()
            let text = xmlhttp.responseText;
            if (text === 'False') {
                bootbox.dialog({
                    message: gettext('Default config not applied'),
                    onEscape: true,
                    backdrop: true
                })
            } else {
                bootbox.dialog({
                    message: gettext('Default config applied'),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "apply_port_default_config", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch],
        ["port_id", port_id],
        ["port_name", port_name],
    ]);
    xmlhttp.send(send_text);
}

function transfer_port_config(id_switch, src_port, src_port_name, dst_switch, dst_port, dst_port_name) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            bootbox.hideAll();
            let text = xmlhttp.responseText;
            console.log(text);
            if (text !== "0") {
                bootbox.dialog({
                    message: text,
                    onEscape: true,
                    backdrop: true
                })
            } else {
                $('#modal_transfer').modal('hide');
                bootbox.dialog({
                    message: gettext('Port transfer completed'),
                    onEscape: true,
                    backdrop: true
                })
                getChangeableInformation(id_switch, src_port)
                getChangeableInformation(dst_switch, dst_port)
            }
        }
    };
    xmlhttp.open("POST", "transfer_port_settings", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["src_switch_id", id_switch],
        ["src_port_id", src_port],
        ["src_port_name", src_port_name],
        ["dst_switch_id", dst_switch],
        ["dst_port_id", dst_port],
        ["dst_port_name", dst_port_name],
    ]);
    xmlhttp.send(send_text);
}

function transfer_switch(id_switch, dst_switch) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let text = xmlhttp.responseText;
            if (text !== "0") {
                bootbox.dialog({
                    message: text,
                    onEscape: true,
                    backdrop: true
                })
            } else {
                $('#modal_transfer').modal('hide');
                bootbox.dialog({
                    message: gettext('Switch transfer completed'),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "transfer_switch", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["src_switch_id", id_switch],
        ["dst_switch_id", dst_switch],
    ]);
    xmlhttp.send(send_text);
}

function reload_cache() {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let text = xmlhttp.responseText;
            if (text !== "0") {
                bootbox.dialog({
                    message: text,
                    onEscape: true,
                    backdrop: true
                })
            } else {
                $("#reload_favicon").removeClass("fa-spin");
            }
        }
    };
    xmlhttp.open("POST", "reload_cache", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
    ]);
    xmlhttp.send(send_text);
}


function update_port_sec(id_switch, port, value) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let text = xmlhttp.responseText;
            if (text !== "0") {
                bootbox.dialog({
                    message: gettext('PortSec change failed'),
                    onEscape: true,
                    backdrop: true
                })
            }
        }
    };
    xmlhttp.open("POST", "update_port_sec", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch],
        ["port", port],
        ["port_sec", value],
    ]);
    xmlhttp.send(send_text);
}

function change_vlan(id_switch, port, port_name, vlan) {
    /*
    funkce pro zmenu vlany na cisco switchich
     */
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            bootbox.hideAll();
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                bootbox.dialog({
                    message: gettext('vlan changed'),
                    onEscape: true,
                    backdrop: true
                })
            } else if (responseText === "1") {
                bootbox.dialog({
                    message: gettext('the change was not successful'),
                    onEscape: true,
                    backdrop: true
                })
            } else if (responseText === "3") {
                console.log("change to same VLAN");
            } else if (responseText === "2") {
                bootbox.dialog({
                    message: gettext('missing write permission, change of VLAN is not possible'),
                    onEscape: true,
                    backdrop: true
                })
            }
        } else if (xmlhttp.readyState === 4 && xmlhttp.status === 500) {
            bootbox.hideAll();
            bootbox.dialog({
                message: gettext('internal server error'),
                onEscape: true,
                backdrop: true
            })
        }
    };

    xmlhttp.open("POST", "change_vlan", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch],
        ["port", port],
        ["vlan", vlan],
        ["port_name", port_name],
    ]);
    xmlhttp.send(send_text);
}

function getChangeableInformation(id_switch, port) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = JSON.parse(xmlhttp.responseText);
            console.log(responseText);

            let alias = responseText[0].replace('<br>', '');
            let status = responseText[1];
            let vlan_number = responseText[2];
            let vlan_name = responseText[3];
            let port_sec = '';
            if (responseText.length > 4) {
                port_sec = responseText[4];
            }


            let row = $('.row_' + id_switch + '_' + port);

            let status_map = {
                0: '<img src="/portmanager/static/switches/connected.png" alt="Connected" title="Connected" height="20px">',
                1: '<img src="/portmanager/static/switches/notconnected.png" alt="Not Connected" title="Not Connected" height="20px">',
                2: '<img src="/portmanager/static/switches/disabled.png"  alt="Disabled" title="Disabled" height="20px">',
                3: 'error'
            };


            row.find('.turn_on_off').attr('id', id_switch + ',' + port + ',' + status);
            row.find('.turn_on_off_vo').attr('id', id_switch + ',' + port + ',' + status);

            row.find('.turn_on_off').html(status_map[status]);
            row.find('.turn_on_off_vo').html(status_map[status]);


            row.find('.change_alias').html(alias);
            let short_name = row.find('.change_alias').id;
            let vlan_elem = row.find('select');
            vlan_elem.val(id_switch + '_' + short_name + '_' + port + '_' + vlan_number);
            try {
                row.find('.portsec_input').val(port_sec);
            } catch (err) {
                console.log('no portsec on changed port');
            }

            $('#temp_row_' + id_switch + '_' + port).remove();
            row.show();
        }
    };

    xmlhttp.open("POST", "get_changeable_information", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["port", port],
        ['id_switch', id_switch]
    ]);
    xmlhttp.send(send_text);

}

function getStatement() {
    let id_switch_array = JSON.stringify(refreshing_array);
    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            let json;
            json = JSON.parse(responseText);

            for (let i = 0; i < json.length; i++) {

                if ($.inArray(json[i], lock_files) === -1) {
                    lock_files.push(json[i]);
                }

                let switch_id = json[i].split('_')[0];
                let port = json[i].split('_')[1];
                let user = json[i].split('_')[2];

                if ($('#temp_row_' + switch_id + '_' + port).length === 0) {
                    let tr = document.createElement('tr');
                    let element = document.createElement('td');
                    element.setAttribute('id', 'temp_row_' + switch_id + '_' + port);
                    element.setAttribute('class', 'temp_row');
                    element.setAttribute('colspan', '6');
                    element.innerHTML = gettext('User') + ' ' + user + ' ' + gettext('is currently working on this port.');
                    $(element).css('background-color', '#bb084d');
                    tr.appendChild(element);
                    $(tr).insertAfter('.row_' + switch_id + '_' + port);
                    $('.row_' + switch_id + '_' + port).hide();
                }
            }

            let someChanges = false;
            for (let i = 0; i < lock_files.length; i++) {
                /* v lock_files mame soubory ktere byly v predchozi iteraci zamcene
                a nyni uz nejsou, tedy je odemkneme tak, ze zobrazime zpet skryty radek
                a temp smazeme
                 */
                if ($.inArray(lock_files[i], json) === -1) {
                    let index = lock_files.indexOf(lock_files[i]);
                    let switch_id = lock_files[i].split('_')[0];
                    let port = lock_files[i].split('_')[1];
                    lock_files.splice(index, 1);
                    someChanges = true;
                }

                if (someChanges) {
                    getChangeableInformation(switch_id, port);
                }
                if (shouldICheck) {
                    setTimeout(function () {
                        setTimeout(getStatement(), 4000);
                    }, 4000);
                }
            }
        }
    };
    xmlhttp.open("POST", "getStatement", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id_switch", id_switch_array]
    ]);
    xmlhttp.send(send_text);
}

function getSwitchInformation(id, port_name) {
    let xmlhttp = new XMLHttpRequest();
    $('#swc_' + id).html('<div class="loader"></div>');
    xmlhttp.onreadystatechange = function () {
        /*
            po kliknuti na switch, musime zobrazit ziskane informace o
            konkretnim switchi
        */
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            let switch_data = JSON.parse(responseText);
            // kontrola, jestli neni backendem predana nejaka chyba
            let error = switch_data['error'];
            if (error) {
                let msg = gettext('An error occurred while obtaining information from the switch.') +
                    "<br/>" +
                    error.toString();
                $('#swc_' + id).html(msg);
            } else {
                // vse je v poradku
                // nyni z odpovedi extrahujeme informace o portech a vlanech
                let switch_id = switch_data['switch_id'].toString();
                let ports = switch_data['ports'];
                let read_only = switch_data['read_only']
                //console.log(ports);
                let vlans = switch_data['vlans'];
                let poe_ports = switch_data['poe_ports'];
                let table;
                let portsec_bool = false;
                let poe_bool = false;
                // status_mapa definuje stringy, ktere se maji nahradit
                // za cisla, tyto cisla jsou predavana z backendu
                let status_map = {
                    0: '<img src="/portmanager/static/switches/connected.png" alt="Connected" title="Connected" height="20px">',
                    1: '<img src="/portmanager/static/switches/notconnected.png" alt="Not Connected" title="Not Connected" height="20px">',
                    2: '<img src="/portmanager/static/switches/disabled.png"  alt="Disabled" title="Disabled" height="20px">',
                    3: 'error'
                };

                table = document.createElement('table');
                table.setAttribute('class', 'display compact');
                table.setAttribute('id', switch_id);
                table.setAttribute('style', 'width=100%')
                // nyni vytvorime tabulku, ktera bude ovladat konkretni switch
                let header = document.createElement('thead');
                let tr = document.createElement('tr');
                let label;
                if (typeof poe_ports != "undefined" && Object.keys(poe_ports).length > 0) {
                    poe_bool = true;
                    if (ports[0][1]['portsec']) {
                        label = "<td><b>Port</b></td> <td><b>" + gettext('Port description') + "</b></td><td><b>VLAN</b></td><td title=" + gettext('If the security port is enabled, it indicates the number of allowed MAC addresses') + "><span class='glyphicon glyphicon-lock visible-xs visible-sm'></span><div class='hidden-xs hidden-sm'><b>" + gettext('Port security') + "</b></div></td><td><b><div class='visible-xs'>MAC</div><div class='hidden-xs'>" + gettext('MAC adresses of connected devices') + "</div></b></td><td><b>" + gettext('State') + "</b></td><td><b>PoE (W)</b></td><td><b></b></td>";
                        portsec_bool = true;
                    } else {
                        label = "<td><b>Port</b></td> <td><b>" + gettext('Port description') + "</b></td><td><b>VLAN</b></td><td><b><div class='visible-xs'>MAC</div><div class='hidden-xs'>" + gettext('MAC adresses of connected devices') + "</div></b></td><td><b>" + gettext('State') + "</b></td><td><b>PoE (W)</b></td><td><b></b></td>";
                    }
                } else {
                    if (ports[0][1]['portsec']) {
                        label = "<td><b>Port</b></td> <td><b>" + gettext('Port description') + "</b></td><td><b>VLAN</b></td><td title=" + gettext('If the security port is enabled, it indicates the number of allowed MAC addresses') + "><span class='glyphicon glyphicon-lock visible-xs visible-sm'></span><div class='hidden-xs hidden-sm'> <b>" + gettext('Port security') + "</b></div></td><td><b><div class='visible-xs'>MAC</div><div class='hidden-xs'>" + gettext('MAC adresses of connected devices') + "</div></b></td><td><b>" + gettext('State') + "</b></td><td><b></b></td>";
                        portsec_bool = true;
                    } else {
                        label = "<td><b>Port</b></td> <td><b>" + gettext('Port description') + "</b></td><td><b>VLAN</b></td><td><b><div class='visible-xs'>MAC</div><div class='hidden-xs'>" + gettext('MAC adresses of connected devices') + "</div></b></td><td><b>" + gettext('State') + "</b></td><td><b></b></td>";
                    }
                }
                tr.innerHTML = label;
                header.appendChild(tr);
                header.style.visibility = "hidden";
                table.appendChild(header);

                let tbody = document.createElement('tbody');
                let row = document.createElement('tr');
                row.innerHTML = label
                tbody.appendChild(row);

                let searched_row = "";
                // nyni vytvorime a naplnime telo tabulky
                for (let i = 0; i < ports.length; i++) {
                    // za kazdy port na switchy vytvorime jeden radek tabulky
                    // v patem prvku pole je ulozena informace
                    // zda je port urcen pouze pro cteni, nebo
                    // i pro zapis

                    if (ports[i][1]['descr'] === port_name) {
                        searched_row = 'row_' + switch_id + '_' + ports[i][0];
                    }

                    if (read_only !== true) {
                        read_only = ports[i][1]['read_only'] !== '0';
                    }
                    // nastavime si dle toho promennou read_only
                    let row = document.createElement('tr');

                    row.setAttribute('class', 'row_' + switch_id + '_' + ports[i][0]);
                    let name = document.createElement('td');
                    name.setAttribute('class', 'name_column');
                    let short_name = ports[i][1]['descr'];
                    if (ports[i][1]['descr'].indexOf('FastEthernet') !== -1) {
                        short_name = ports[i][1]['descr'].replace('FastEthernet', 'Fa');
                    }
                    if (ports[i][1]['descr'].indexOf('GigabitEthernet') !== -1) {
                        short_name = ports[i][1]['descr'].replace('GigabitEthernet', 'Gi');
                    }
                    if (ports[i][1]['descr'].indexOf('Ten GigabitEthernet') !== -1) {
                        short_name = ports[i][1]['descr'].replace('Ten GigabitEthernet', 'Ten');
                    }
                    name.innerHTML = "<div class='visible-xs'>" + short_name + "</div><div class='hidden-xs'>" + ports[i][1]['descr'] + "</div>";
                    // name bude obsahovat informaci
                    // typu FastEthernet atd.
                    let alias = document.createElement('td');
                    alias.setAttribute('class', 'alias_column');
                    // alias obsahuje popisek portu,
                    // u tohoto sloupce musime rozlisovat, zda
                    // je u nej mozny zapis (tedy mu zpristupnime contentEditable
                    // jinak nic, tridu nastavime vo (view_only), aby na ni nepusobili
                    // handlery a event listenery.
                    let change_alias = document.createElement('div');
                    if (read_only) {
                        change_alias.setAttribute('class', 'change_alias_vo');
                    } else {
                        change_alias.setAttribute('class', 'change_alias');
                        change_alias.setAttribute('contentEditable', 'true');
                    }
                    //
                    if (!(ports[i][1]['alias'] === "")) {
                        change_alias.innerHTML = ports[i][1]['alias'];
                    } else {
                        change_alias.innerHTML = '(None)';
                    }


                    change_alias.setAttribute('id', switch_id + ',' + ports[i][0] + ',' + short_name);
                    // nastaveni atributu id, na switch_id, port_id, abychom mohli snadno
                    // pote urcit, na kterem portu a switchy chceme danou vec nastavit
                    alias.appendChild(change_alias);


                    let connected_macs = document.createElement('td');
                    $(connected_macs).attr("class", "connected_macs");

                    // Pokud z backendu jako pripojene adresy
                    // prijde 'None', tak tak jej zmenime na N/A
                    // na druhou stranu pokud je tam neco nastavene
                    // tak jsou to jednotlive mac-addresy, ktere jsou oddeleny pomoci
                    // carky, podle carky splitneme string
                    // a zacneme vkladat mac_adresy oddelene enterem

                    let element = document.createElement("div");
                    $(element).attr("class", "get_connected_macs_button");
                    $(element).attr("name", switch_id + "_" + ports[i][0] + "_" + ports[i][1]['vlan'].split(' ')[0]);

                    $(element).html('<button type="button" class="btn btn-block btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span>' + gettext('Show MAC adresses') + '</button>');


                    $(connected_macs).append(element);
                    let security;
                    if (portsec_bool) {
                        security = document.createElement('td');

                        if ((switch_data['portsec_modify'] !== true) && (!read_only)) {
                            security.innerHTML = ports[i][1]['portsec'];
                        } else {
                            let sec_element = document.createElement("input");
                            $(sec_element).attr("id", switch_id + "_" + ports[i][0] + "_portsec");
                            $(sec_element).attr("value", ports[i][1]['portsec']);
                            $(sec_element).attr("class", "portsec_input");
                            $(sec_element).attr("type", "number");
                            $(sec_element).attr("min", "1");
                            $(sec_element).attr("max", switch_data['portsec_max']);
                            $(security).append(sec_element);
                        }

                        security.title = gettext('If port security is enabled, number indicates how many MAC addresses are allowed');
                    }

                    let poe;
                    if (poe_bool) {
                        poe = document.createElement('td');
                        poe.innerHTML = poe_ports[ports[i][1]['descr']];
                        if (poe_ports[ports[i][1]['descr']] === undefined) {
                            poe.innerHTML = 'N/A';
                        }
                    }
                    // v mac_address si nastavime mac_adresu
                    // konkretniho portu
                    let turn_on_off = document.createElement('td');
                    // turn_on_off je tlacitko slouzici k vypinani a zapinani portu
                    //
                    let c_turn_on_off = document.createElement('div');
                    // v pripade ze se jedna o read_only port,
                    // tak opet nastavime tridu navic o _vo a tim
                    // znemoznime "klikani" na ni
                    if (read_only) {
                        c_turn_on_off.setAttribute('class', 'turn_on_off_vo');
                    } else {
                        c_turn_on_off.setAttribute('class', 'turn_on_off');
                    }
                    c_turn_on_off.setAttribute('id', switch_id + ',' + ports[i][1]['iid'] + ',' + ports[i][1]['status']);
                    c_turn_on_off.innerHTML = status_map[ports[i][1]['status']];

                    turn_on_off.appendChild(c_turn_on_off);

                    //
                    // posledni sloupec,
                    // tento sloupec slouzi k zobrazeni do jake vlany
                    // je port nastaven a take aby umoznil zmenit konkretni vlanu
                    //
                    let select = document.createElement('td');
                    select.setAttribute('id', switch_id + '_' + ports[i][0]);

                    // do id si opet ukladame pomocne informace, ktere se nam
                    // pozdeji budou hodit v nastavovani jednotlivych inforamci
                    //

                    let select_vlan = document.createElement('select');
                    select_vlan.setAttribute('id', switch_id + '_' + ports[i][0] + '_' + 'vlan');

                    if (read_only) {
                        select_vlan.setAttribute('class', 'disabled');
                    }
                    let option = document.createElement('option');
                    option.setAttribute('selected', 'selected');
                    option.setAttribute('value', switch_id + '_' + +short_name + '_' + ports[i][0] + '_' + ports[i][1]['vlan']);
                    option.innerHTML = ports[i][1]['vlan'];
                    select_vlan.appendChild(option);


                    for (let j = 0; j < vlans.length; j++) {
                        let vlan_read_only;

                        if (ports[i][1]['vlan'] === vlans[j][0] + " - " + vlans[j][1]) {
                            continue;
                        }

                        if (vlans[j][2] === 0) {
                            vlan_read_only = false;
                        } else {
                            vlan_read_only = true;
                        }

                        let option = document.createElement('option');

                        if (vlan_read_only) {
                            option.setAttribute('class', 'disabled');
                        }
                        option.setAttribute('value', switch_id + '_' + short_name + '_' + ports[i][0] + '_' + vlans[j][0]);
                        option.innerHTML = vlans[j][0] + ' - ' + vlans[j][1];
                        select_vlan.appendChild(option);
                    }
                    let menu = document.createElement('td');
                    menu.innerHTML = "<div class='dropdown'>" +
                        "<a type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' class='fa fa-ellipsis-v btn btn-default btn-sm'></a>" +
                        "<div class='dropdown-menu dropdown-menu-right'>" +
                        "<a class='dropdown-item' id='default_" + switch_id + "_" + ports[i][1]['descr'] + "_" + ports[i][0] + "' style='cursor: pointer;'><i class='icon fa fa-eraser'></i>&nbsp;&nbsp;" + gettext('Apply default config') + "</a> <br>" +
                        "<a class='dropdown-item' id='transfer_" + switch_id + "_" + ports[i][1]['descr'] + "_" + ports[i][0] + "' style='cursor: pointer;'><i class='icon fa fa-random'></i>&nbsp;&nbsp;" + gettext('Transfer port settings') + "</a>" +
                        "</div></div>"


                    select.appendChild(select_vlan);
                    row.appendChild(name);
                    row.appendChild(alias);
                    row.appendChild(select);
                    if (portsec_bool) {
                        row.appendChild(security);
                    }
                    row.appendChild(connected_macs);
                    row.appendChild(turn_on_off);
                    if (poe_bool) {
                        row.appendChild(poe);
                    }
                    row.appendChild(menu);

                    // nyni radek naplnime nachystanymi daty a pak radek pridame do tabulky
                    tbody.appendChild(row);

                }
                table.appendChild(tbody);
                $('#swc_' + id).html('');
                $('#swc_' + id).append(table);
                $('#' + switch_id).find('select').each(function () {
                    $(this).select2().on('change', function (e) {
                        bootbox.dialog({
                            message: '<h4 class="text-center mb-0"><i class="fa fa-spin fa-cog"></i>&nbsp;' + gettext("Applying new VLAN") + '</h4>',
                            closeButton: false
                        });
                        let vlan_target = e.delegateTarget.value.split('_');
                        let switch_id = vlan_target[0];
                        let interface_name = vlan_target[1];
                        let interface_id = vlan_target[2];
                        let vlan_id = vlan_target[3].split(' - ')[0];
                        change_vlan(switch_id, interface_id, interface_name, vlan_id);
                    });
                });
                var language = $('#current_lang').val();
                if (language !== 'en/') {
                    $('#' + switch_id).DataTable({
                        paging: false,
                        ordering: false,
                        search: true,
                        scrollX: true,
                        scrollCollapse: true,
                        autoWidth: false,
                        fixedColumns: true,
                        language: {
                            "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Czech.json"
                        }
                    });
                } else {
                    $('#' + switch_id).DataTable({
                        paging: false,
                        ordering: false,
                        search: true,
                        scrollX: true,
                        scrollCollapse: true,
                        autoWidth: false,
                        fixedColumns: true,
                    });
                }
                if (searched_row !== "") {
                    $('html, body').animate({
                        scrollTop: $('.' + searched_row).offset().top
                    }, 200);
                }
            }
        }
    };

    xmlhttp.open("POST", "get_switch_information", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let csrf = getCookie("csrftoken");
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id", id]]);
    xmlhttp.send(send_text);
}

function show_switches(locality_id) {

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {

            var data = JSON.parse(xmlhttp.responseText);
            var dst_dev = document.getElementById('sel_dst_dev');

            var opt_dst = document.createElement('option');
            $(opt_dst).attr('class', 'dst_dev');
            $(opt_dst).attr('id', 'dst_dev_none');
            dst_dev.appendChild(opt_dst);

            for (var i = 0; i < data.length; i++) {
                var id = data[i]['id_switch'];
                var name = data[i]['switch_name'];
                var opt_dst = document.createElement('option');
                $(opt_dst).attr('class', 'dst_dev');
                $(opt_dst).attr('id', 'dst_dev_' + id);
                $(opt_dst).html(name);
                dst_dev.appendChild(opt_dst);
            }
        }
    };

    xmlhttp.open("POST", "get_switches_by_location", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");

    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["locality_id", locality_id]
    ]);

    xmlhttp.send(send_text);
}

function show_dst_port(switch_id) {
    var xmlhttp = new XMLHttpRequest();
    var element = $('#sel_dst_port');
    element.html('');
    element.hide();
    element.parent().append('<div class="loaderPort loaderPortDst"></div>');

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            $(".loaderPortDst").remove();
            element.show();
            var data = JSON.parse(xmlhttp.responseText);
            var error = data['error'];
            if (error) {
                var msg = gettext('An error occurred while obtaining information from the switch.') +
                    error.toString();
                alert(msg);
            } else {
                var ports = data['ports'];
                var dst_port = document.getElementById('sel_dst_port');
                for (var i = 0; i < ports.length; i++) {
                    var name = ports[i][1]['descr'];
                    var alias;
                    var id = ports[i][0];
                    var vlan = ports[i][1]['vlan'];
                    if (!(ports[i][1]['alias'] === null)) {
                        alias = ports[i][1]['alias'];
                    } else {
                        alias = '(None)';
                    }
                    var opt_dst = document.createElement('option');
                    $(opt_dst).attr('class', 'dst_port');
                    $(opt_dst).attr('id', 'dst_port_' + id + '_' + name);
                    $(opt_dst).html(name + ' | Description: ' + alias + ' | VLAN: ' + vlan);
                    dst_port.appendChild(opt_dst);
                }
            }
        }
    };

    xmlhttp.open("POST", "get_switch_information", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var csrf = getCookie("csrftoken");

    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["id", switch_id]
    ]);

    xmlhttp.send(send_text);
}

$(document).ready(function () {

    getStatement(refreshing_array);

    /*
     tyto eventhandlery nam zajistuji vyskakovani a vskakovani do oken, aby jsme nevyvolavali requesty zbytecne,
     pripadne nezamknuli soubory, kdyz se nic nedeje
    */


    if (typeof document.hidden !== "undefined") {
        hidden = "hidden";
        visibilityChange = "visibilitychange";
    } else if (typeof document.mozHidden !== "undefined") {
        hidden = "mozHidden";
        visibilityChange = "mozvisibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
        hidden = "msHidden";
        visibilityChange = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
        hidden = "webkitHidden";
        visibilityChange = "webkitvisibilitychange";
    }


    $(document).delegate('.turn_on_off', 'click', function () {
        /*
            vypinac portu, predame funkci id_switche a port, ktery am vypnout a vypne ho
            a v popredi zmeni barvu 'tlacitka'
         */

        let id_switch = $(this).attr('id').split(',')[0];
        let port = $(this).attr('id').split(',')[1];

        $(this).html('<div class="loaderPort"></div>');
        turnonoff(id_switch, port);
    });

    //boxWidget demands some removeTrigger, -> non-existing #xxx as fix
    $('.switch').each(function () {
        let id = $(this).attr('id').split('_')[1];
        $(this).boxWidget({
            animationSpeed: 500,
            collapseTrigger: '#header_' + id,
            removeTrigger: '#xxx',
            collapseIcon: '',
            expandIcon: '',
            removeIcon: ''
        });
    });

    $('.action_button').click(function (e) {
        let device = $(this).attr('id').split('_')[1];
        event.stopPropagation();
        $('.dropdown-toggle').dropdown();
    });

    $('.switch-header, .box-title').click(function (e) {
        /*
            event stahne informace o switchy,
            a prida switch_id do pole aktualne
            proverovanych switchu

        */
        if (e.target !== e.currentTarget) return;
        let id = $(this).attr('id').split('_')[1];
        if ($(this).hasClass('opened')) {
            $(this).removeClass('opened');
            refreshing_array = refreshing_array.filter(item >= item !== id
            )
        } else {
            $(this).addClass('opened');
            if (($.inArray(id, refreshing_array)) === -1) {
                refreshing_array.push(id);
            }
            getSwitchInformation(id, "");
        }
    });

    $(document).delegate('.change_alias[contentEditable]', 'keydown', function (e) {
        if (e.keyCode === 13) {
            // enter
            // v pripade, ze nekdo odenteruje popisek,
            // tak se popisek musi na zarizeni nastavit,
            // tento handler je ekvivalentni k blur (tedy
            // pokud uzivatel rozostri, tak se musi popisek
            // nastavit
            e.preventDefault();
            bootbox.dialog({
                message: '<h4 class="text-center mb-0"><i class="fa fa-spin fa-cog"></i>&nbsp;' + gettext("Applying port description") + '</h4>',
                closeButton: false
            });
            let id_switch = $(this).attr('id').split(',')[0].replace(/\s/g, '');
            let port = $(this).attr('id').split(',')[1].replace(/\s/g, '');
            let port_name = $(this).attr('id').split(',')[2].replace(/\s/g, '');
            // vsechny nutne informace pro nastavovani jsou nastavene
            // v idcku
            let new_alias = $(this).text();

            if (!$(this).text().replace(/\s/g, '').length) {
                // podle dohody pokud uzivatel odesle prazdny
                // alias, pak se na jeho misto nastavi None
                //
                let none = "None";
                $(this).html(none);
                change_alias(id_switch, port, port_name, none);
            } else {
                change_alias(id_switch, port, port_name, new_alias);
                $(this).attr('id', $(this).attr('id').split(',')[0] + ',' + $(this).attr('id').split(',')[1] + ',' + new_alias);
                $(this).blur();
            }
        }
    });

    $(document).delegate('.change_alias[contentEditable]', 'blur', function (e) {
        // tento handler ulozi konfiguraci na change_aliasu
        // na blur

        let id_switch = $(this).attr('id').split(',')[0].replace(/\s/g, '');

        let port = $(this).attr('id').split(',')[1].replace(/\s/g, '');
        let port_name = $(this).attr('id').split(',')[2].replace(/\s/g, '');
        let new_alias = $(this).text();
        if (!$(this).text().replace(/\s/g, '').length) {
            // pokud je text prazdny,
            // pak jej nahradime stringem "None"
            // a odesleme None i do backendu
            let none = "None";
            $(this).html(none);
            //change_alias(id_switch, port, port_name, none);
        } else {
            //change_alias(id_switch, port, port_name, new_alias);
            $(this).attr('id', $(this).attr('id').split(',')[0] + ',' + $(this).attr('id').split(',')[1] + ',' + new_alias);
        }
        $('tbody>tr').css('font-weight', 'normal');
    });

    $(document).delegate(".get_connected_macs_button", "click", function () {
        let id = $(this).attr("name");
        let switch_id = id.split("_")[0];
        let port_id = id.split("_")[1];
        let vlan_name = id.split("_")[2];

        get_connected_macs(switch_id, port_id, vlan_name);
    });

    $(document).delegate(".portsec_input", "change", function () {
        let id = $(this).attr("id");
        let switch_id = id.split("_")[0];
        let port_id = id.split("_")[1];
        let value = $(this).val();
        update_port_sec(switch_id, port_id, value);
    });

    $(document).on('click', '.locality', function () {
        var locality_id = $('#sel1').find(':selected').attr('id').split('_')[1];
        $('#sel_dst_dev').empty();
        $('#sel_dst_port').empty();
        show_switches(locality_id);
    });

    $(document).on('click', '.dst_dev', function () {
        var switch_id = $('#sel_dst_dev').find(':selected').attr('id').split('_')[2];
        $('#sel_dst_port').empty();
        if (switch_id != 'none') {
            show_dst_port(switch_id);
        }
    });

    $(document).on('click', '#transfer_port', function () {
        var src_switch_id = $('#src_dev').text().split(' - ')[0];
        var src_port_id = $('#src_dev').text().split(' - ')[1];
        var src_port_name = $('#src_dev').text().split(' - ')[2];
        var dst_switch_id = $('#device-search').find(':selected').val();
        var dst_port_id = $('#sel_dst_port').find(':selected').attr('id').split('_')[2];
        var dst_port_name = $('#sel_dst_port').find(':selected').attr('id').split('_')[3];
        bootbox.dialog({
            message: '<h4 class="text-center mb-0"><i class="fa fa-spin fa-cog"></i>&nbsp;' + gettext("Trasfer in progress") + '</h4>',
            closeButton: false
        });
        transfer_port_config(src_switch_id, src_port_id, src_port_name, dst_switch_id, dst_port_id, dst_port_name);
    });

    $(document).on('click', '#transfer_switch', function () {
        var src_switch_id = $('#src_dev-1').text();
        var dst_switch_id = $('#device-search-1').find(':selected').val();
        transfer_switch(src_switch_id, dst_switch_id);
    });

    $(document).delegate(".dropdown-item", "click", function () {
        let id = $(this).attr("id");
        let action = id.split("_")[0];
        let switch_id = id.split("_")[1];
        let port_name = id.split("_")[2];
        let port_id = id.split("_")[3];
        let switch_name = $('#title_' + switch_id).text();
        if (action === "default") {
            bootbox.dialog({
                message: '<h4 class="text-center mb-0"><i class="fa fa-spin fa-cog"></i>&nbsp;' + gettext("Applying default config") + '</h4>',
                closeButton: false
            });
            apply_default_config(switch_id, port_id, port_name);
        }

        if (action === "transfer-switch") {
            $('#transfer-switch-title').html(gettext("Transfer all ports from device ") + switch_name)
            $('#src_dev-1').html(switch_id);
            $('#src_dev-1').hide();
            $('#modal_transfer_switch').modal('show');
        }
        if (action === "transfer") {
            $('#transfer-port-title').html(gettext("Transfer port settings from") + " " + port_name + " " + gettext("on device") + " " + switch_name)
            $('#src_dev').html(switch_id + ' - ' + port_id + ' - ' + port_name);
            $('#src_dev').hide();
            $('#modal_transfer').modal('show');
        }
        if (action === "export") {
            let device = $(this).attr('id').split('_')[1];

            bootbox.dialog({
                message: '<h4 class="text-center mb-0"><i class="fa fa-spin fa-cog"></i>&nbsp;' + gettext("Export in progress") + '</h4>',
                closeButton: false
            });
            export_csv(device)
        }

    });

    $(document).delegate("#reload_cache", "click", function () {
        $("#reload_favicon").addClass("fa-spin");
        reload_cache();
    });

    $(function () {
        let pgurl = window.location.pathname;
        $(".sidebar-navbar-collapse ul li a").each(function () {
            if ($(this).attr("href") === pgurl || $(this).attr("href") === '')
                $(this).parent().addClass("active");
        })
    });


    $('#global-search').select2({
        ajax: {
            url: "get_cached_ports",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: gettext("Search for a port alias (case sensitive)"),
        minimumInputLength: 1,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });

    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }

        var $container = $(
            "<div class='select2-result clearfix'>" +
            "<span class='select2-result__location'></span>" +
            "<span class='select2-result__device'></span>" +
            "<span class='select2-result__port'></span>" +
            "<span class='select2-result__alias'></span>" +
            "</div>"
        );

        $container.find(".select2-result__location").text('Location: ' + repo.loc_name);
        $container.find(".select2-result__device").text('| Device: ' + repo.switch_name);
        $container.find(".select2-result__port").text('| Port: ' + repo.port_name);
        $container.find(".select2-result__alias").text('| Alias: ' + repo.port_alias);
        return $container;
    }

    function formatRepoSelection(repo) {
        if (repo.id) {
            return repo.id + ' | ' + repo.switch_host + ' | ' + repo.port_name + ' | ' + repo.port_alias;
        }
        return repo.text;
    }

    $("#device-search, #device-search-1").select2({
        ajax: {
            url: "get_devices",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: gettext('Search for a destination device'),
        minimumInputLength: 1,
        templateResult: formatSwitch,
        templateSelection: formatSwitchSelection
    });

    function formatSwitch(repo) {
        if (repo.loading) {
            return repo.text;
        }

        var $container = $(
            "<div class='select2-result clearfix'>" +
            "<span class='select2-result__location'></span>" +
            "<span class='select2-result__device'></span>" +
            "<span class='select2-result__destination'></span>" +
            "</div>"
        );

        $container.find(".select2-result__location").text(repo.loc_name);
        $container.find(".select2-result__device").text('|' + repo.switch_name);
        $container.find(".select2-result__destination").text(' (' + repo.switch_host + ')');

        return $container;
    }

    function formatSwitchSelection(repo) {
        return repo.switch_host || repo.text;
    }

    $('#device-search').on('change', function () {
        let switch_name = this.value;
        show_dst_port(switch_name);
    });

    $('#global-search').on('change', function () {
        let switch_id = $("#global-search").select2('data')[0].id;
        let port_name = $("#global-search").select2('data')[0].port_name;
        if ($('#header_' + switch_id).hasClass('opened')) {
            $('#header_' + switch_id).removeClass('opened');
            refreshing_array = refreshing_array.filter(item >= item !== switch_id
            )
        } else {
            $('#header_' + switch_id).addClass('opened');
            $('#box_' + switch_id).boxWidget('toggle');
            if (($.inArray(switch_id, refreshing_array)) === -1) {
                refreshing_array.push(switch_id);
            }
            $('html, body').animate({
                scrollTop: $("#box_" + switch_id).offset().top
            }, 2000);
            getSwitchInformation(switch_id, port_name);
        }
        $("#global-search").val(null);
    });


});
