$(document).ready(function() {
    var language = $('#current_lang').val();
    if (language !== 'en/') {
        $('#log_table').DataTable({
            order: [[0, 'desc']],
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Czech.json"
            }
        });
    } else {
        $('#log_table').DataTable({
            order: [[0, 'desc']]
        });
    }
} );
