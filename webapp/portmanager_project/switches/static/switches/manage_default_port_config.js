function add_device(manufacturer, model, note) {
    /*
        Add new device to database
    */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_alert(gettext("Device was successfully added."));
            } else {
                create_alert(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open('POST', 'create_device', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    let csrf = getCookie('csrftoken');
    let send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["manufacturer", manufacturer],
        ['model', model],
        ['note', note]
    ]);

    xmlhttp.send(send_text);
}

function change_device_port_config(id_device, new_port_config) {
    /*
    Function to change device default config.
     */

    let xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            let responseText = xmlhttp.responseText;
            if (responseText === "0") {
                create_notification(gettext("Device default port config successfully changed."));
            } else {
                create_notification(responseText.split(" ").slice(1));
            }
        }
    };

    xmlhttp.open("POST", "update_device_default_port_config", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    let csrf = getCookie("csrftoken");
    send_text = makePOSTRawText([
        ["csrfmiddlewaretoken", csrf],
        ["port_config", new_port_config],
        ["device_id", id_device]]);
    xmlhttp.send(send_text);
}

$(document).ready(function () {

    //creates new device
    $('#add_device_save').click(function () {
        add_device($('#device_manufacturer').val(),
            $('#device_model').val(),
            $('#device_note').val());
    });

    //saves new port config
    $('.btn-update-config').click(function () {
        let dev_id = $(this).attr('id').split('_')[1];
        change_device_port_config(
            dev_id,
            $('#default-config_' + dev_id).val(),
        );
    });
});
