# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-10-12 23:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('switches', '0006_device_default_port_config'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='port_sec',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
