# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-07-16 21:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('switches', '0003_auto_20200716_2333'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='read_only',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
