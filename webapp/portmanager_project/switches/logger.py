import datetime

from switches.models import *


class Logger:
    """Class for log file handling"""

    def __init__(self):
        self.events = Event.objects.all()

    def get_list_of_logs(self, count):
        """Returns the exact number of incidents from the last time"""

        self.events = self.events.order_by("-id_event")
        return self.events[:count]

    def get_list_of_logs_from_to(self, lFrom, lTo):
        """Returns the exact number of incidents from the last time"""

        self.events = self.events.order_by("-id_event")
        return self.events[lFrom:lTo]

    def write_incident(self, who, what):
        """Creates a record about the incident
        Saves it to the file with timestamp
        """
        date = datetime.datetime.now().date()
        time = datetime.datetime.now().time()

        when = str(date) + " : " + str(time)[:8]
        if Event.objects.count() == 0:
            id_event = 0
        else:
            id_event = self.events.order_by("-id_event")[0].id_event + 1

        event = Event.objects.create(id_event=id_event,
                                     who=who,
                                     what=what,
                                     reply="",
                                     seen=True,
                                     time=when,)

        event.save()

        return 1

    def newest_incident(self):
        """Returns the newest incident"""
        return self.events.order_by("-id_event")[0]

    def get_list_of_all_logs(self):
        """Returns list of all incidents"""
        return self.events.order_by("-id_event")
