from django.contrib import admin
from switches.models import *


class LocationAdmin(admin.ModelAdmin):
    pass


class DeviceAdmin(admin.ModelAdmin):
    pass


class SwitchAdmin(admin.ModelAdmin):
    pass


class PersonAdmin(admin.ModelAdmin):
    pass


class GroupAdmin(admin.ModelAdmin):
    pass


class PortsAdmin(admin.ModelAdmin):
    pass


class VlansAdmin(admin.ModelAdmin):
    pass


class EventAdmin(admin.ModelAdmin):
    pass
admin.site.register(Location, LocationAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(Switch, SwitchAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Ports, PortsAdmin)
admin.site.register(Vlans, VlansAdmin)
admin.site.register(Event, EventAdmin)
