from django.db import models


class Device(models.Model):
    id_device = models.IntegerField(primary_key=True)
    manufacturer = models.CharField(max_length=32)
    name = models.CharField(max_length=32)
    note = models.CharField(max_length=255)
    default_port_config = models.CharField(max_length=255, null=True, blank=True)

    def __unicode__(self):
        return self.manufacturer + " " + self.name


class Person(models.Model):
    id_person = models.CharField(primary_key=True, max_length=32)
    name = models.CharField(max_length=32)
    surname = models.CharField(max_length=32)
    email = models.EmailField()
    contact = models.CharField(max_length=32)
    superuser = models.BooleanField()
    read_only = models.BooleanField()
    port_sec = models.BooleanField()

    def __unicode__(self):
        return "%s - %s %s" % (self.id_person, self.name, self.surname,)
        # return ", " + self.name + " " + self.surname

    class Meta:
        ordering = ('surname', 'name', 'id_person',)


class Group(models.Model):
    id_group = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32)
    members = models.ManyToManyField(Person)
    admins = models.ManyToManyField(Person, related_name='admins')

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('id_group', 'name')


class Location(models.Model):
    id_location = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32)
    street = models.CharField(max_length=32)
    city = models.CharField(max_length=32)
    groups = models.ManyToManyField(Group)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Switch(models.Model):
    id_switch = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32, blank=False)
    dest_host = models.CharField(max_length=32, blank=False)
    location = models.ForeignKey(Location)
    device = models.ForeignKey(Device)
    owners = models.ManyToManyField(Group)
    community = models.CharField(max_length=32)
    snmp_version = models.IntegerField()
    poe_regex = models.CharField(max_length=64, null=True, blank=True)
    ssh_key = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.dest_host

    class Meta:
        ordering = ('name', 'dest_host')


class Ports(models.Model):
    id_ports = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32)
    switch = models.ManyToManyField(Switch)
    groups = models.ManyToManyField(Group)
    view_only = models.BooleanField()
    regex = models.CharField(max_length=64)

    def __unicode__(self):
        return self.name + " " + self.regex

    class Meta:
        ordering = ('name',)


class Vlans(models.Model):
    id_vlans = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32)
    switch = models.ManyToManyField(Switch)
    groups = models.ManyToManyField(Group)
    view_only = models.BooleanField()
    regex = models.CharField(max_length=64)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('id_vlans',)


class Event(models.Model):
    id_event = models.IntegerField(primary_key=True)
    who = models.ForeignKey(Person)
    what = models.CharField(max_length=64)
    reply = models.CharField(max_length=128, blank=True)
    seen = models.BooleanField()
    time = models.CharField(max_length=32)

    def __unicode__(self):
        return str(self.id_event) + " " + self.time + " "
        + str(self.who).decode('utf-8') + " " + self.what

    class Meta:
        ordering = ('id_event',)
