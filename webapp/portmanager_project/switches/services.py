import logging

from django.core.cache import cache

from models import Switch
from switch_modules import cisco
from switch_modules import general
from switch_modules import hp
from switch_modules import hp_old
from switch_modules import juniper

logger = logging.getLogger(__name__)


def get_instance(switch):
    """
    Creates switch instance for specific manufacturers.
    :param switch: model.switch
    :return:
    """

    if switch.device.manufacturer == "Cisco":
        instance = cisco.CiscoSwitch(switch.id_switch)

    elif switch.device.manufacturer == "HP":
        instance = hp.HpSwitch(switch.id_switch)

    elif switch.device.manufacturer == 'HPOld':
        instance = hp_old.OldHpSwitch(switch.id_switch)

    elif switch.device.manufacturer == "Juniper":
        instance = juniper.JuniperSwitch(switch.id_switch)

    else:
        instance = general.GeneralSwitch(switch.id_switch)

    return instance


def update_all_dev_vlans():
    switches = Switch.objects.all()
    for switch in switches:
        try:
            switch_instance = get_instance(switch)
            if switch.device.manufacturer == "Cisco":
                switch_instance.update_vlan_table()
        except Exception as e:
            logger.warning(switch.dest_host + ': ' + str(e))


def create_lock(switch):
    """
    Creates lock for periodically saving running config.
    @param switch:
    @return:
    """
    try:
        cache.set('dev_lock_' + str(switch.id_switch), 'a', timeout=420)
    except Exception as e:
        logger.error(str(e))
        return False
    return True


def create_port_lock(switch_id, port_id, user_id):
    """
    Creates lock for port that is currently being changed to avoid duplicate changes.
    @rtype: bool
    @param user_id:
    @param switch_id:
    @param port_id:
    @return:
    """
    try:
        cache.set('lock_' + switch_id + '_' + port_id + '_' + user_id + '.lock', 'a', timeout=30)
    except Exception as e:
        logger.error(str(e))
        return False
    return True


def remove_port_lock(switch_id, port_id, user_id):
    """
    Removes lock for port that is not currently being changed.
    @rtype: bool
    @param user_id:
    @param switch_id:
    @param port_id:
    @return:
    """
    try:
        cache.delete('lock_' + switch_id + '_' + port_id + '_' + user_id + '.lock')
    except Exception as e:
        logger.error(str(e))
        return False
    return True


def all_locks_copy_run_start():
    """
    Checks if changes on switches are older than 5 minutes and saves running config.
    @rtype: bool
    @return:
    """
    locks = cache.keys("dev_lock_*")
    for lock_file in locks:
        if cache.ttl(lock_file) < 120:
            switch_id = lock_file.split('_')[2]
            switch = Switch.objects.get(id_switch=switch_id)
            switch_instance = get_instance(switch)
            try:
                switch_instance.copy_run_start()
                cache.delete(lock_file)
            except Exception as e:
                logger.error(str(e))
                return False
            return True
