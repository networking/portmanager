from django import forms

from models import Person


class CreateAccountForm(forms.Form):
    Uco = forms.IntegerField()
    Name = forms.CharField(max_length=32)
    Surname = forms.CharField(max_length=32)
    Email = forms.EmailField()
    SuperUser = forms.BooleanField()
    Telephone = forms.CharField(max_length=32)


class AddGroup(forms.Form):
    Jmeno = forms.CharField(max_length=32)
    members = Person.objects.all()
    options = ()
    for member in members:
        options = options + \
            ((member.id_person, member.name + " " + member.surname),)

    Uzivatele = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(), choices=options)
