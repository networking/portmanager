from os import environ

from logger import Logger
from models import Group, Switch, Ports
from models import Person


def from_uco_load_groups(uco):
    """
    :param uco: personal ID
    :return:
    """
    try:
        person = Person.objects.get(id_person=uco)
        groups = Group.objects.all().filter(members=person)
        return groups

    except ValueError:
        return None


def is_admin(uco):
    """
    :param uco: personal ID
    :return:
    """
    try:
        person = Person.objects.get(id_person=uco)
        groups = Group.objects.all().filter(admins=person)
        return groups

    except ValueError:
        return None


def get_uco(uco):
    """
    :param uco: personal ID
    :return:
    """
    return uco.split("@")[0]


def from_uco_load_person(uco):
    """
    :param uco: personal ID
    :return:
    """
    try:
        person = Person.objects.get(id_person=uco)
        return person
    except ValueError:
        return None


def login(request):
    """

    :param request:
    :return:
    """
    # For debug I can return any specific person
    if environ.get('DEBUG')=='true':
        uid = environ.get('DEBUG_USR')
        return str(uid), Person.objects.get(id_person=uid)

    # correct returning of a person from request
    if 'HTTP_X_REMOTE_USER' in request.META:
        user = request.META.get('HTTP_X_REMOTE_USER', '')
        user = str(user).partition('@')[0]
        try:
            Person.objects.get(id_person=user)
        except Person.DoesNotExist:
            name = 'N/A'
            surname = 'N/A'
            email = 'example@example.com'
            superuser = False
            readonly = True
            portsec = False
            contact = 'N/A'

            log = Logger()
            try:
                new_user = Person.objects.create(id_person=user,
                                                 name=name,
                                                 surname=surname,
                                                 email=email,
                                                 contact=contact,
                                                 superuser=superuser,
                                                 read_only=readonly,
                                                 port_sec=portsec)
                new_user.save()

            except Exception as e:
                log.write_incident(
                    user, ' tried to create user ' + user + ' - ' + name + ' ' +
                          surname + '. ' + e.message + " " + str(e))
                return "None"
            log.write_incident(
                user, ' created user ' + user + ' - ' + name + ' ' + surname)
        return user, Person.objects.get(id_person=user)
    else:
        return "None"


def check_port_permission(port_requested, switch, user):
    """
    Checks if user has write permission for port
    @rtype: bool
    @param port_requested: port ifIndex
    @param switch: switch ID
    @param user: user ID
    @return: True if user has write permission
    """

    groups = from_uco_load_groups(user)
    switch = Switch.objects.get(id_switch=switch)

    user_ports = Ports.objects.all()
    user_ports = user_ports.filter(switch=switch, groups__in=groups).distinct()

    port_permission = False
    for group_of_ports in user_ports:
        regex_instance = RegExp(group_of_ports.regex)
        array = regex_instance.getStringArray()

        # have to differentiate between ports containing single or multiple ports
        for ports in array:
            if ports == '':
                continue
            if '-' in ports:
                start = int(ports.split('-')[0])
                end = int(ports.split('-')[1])

                for i in range(start, end + 1):
                    if str(i) != str(port_requested):
                        continue
                    if group_of_ports.view_only:
                        continue
                    else:
                        port_permission = True
            else:
                if str(ports) != str(port_requested):
                    continue
                if group_of_ports.view_only:
                    continue
                else:
                    port_permission = True

    return port_permission


def check_vlan_permission(vlan_requested, switch, user):
    # TODO add code to check user permissions, return boolean
    return True


class RegExp():

    def __init__(self, regEx):
        self.ports = regEx
        self.ports = self.ports.replace("\"", "")
        self.ports = self.ports.replace("[", "")
        self.ports = self.ports.replace("]", "")
        self.ports = self.ports.replace(" ", "")
        self.ports = self.ports.replace("\'", "")
        self.array = self.ports.split(",")

        self.final = []
        if not self.array == ['']:
            for port in self.array:
                if "-" in port:
                    port = port.split("-")
                    for i in range(int(port[0]), int(port[1]) + 1):
                        self.final.append(i)
                else:
                    self.final.append(int(port))

    def getTuple(self):
        return sorted(self.final)

    def getStringArray(self):
        return self.array
