import re
from time import sleep

import netsnmp

from ..models import Switch


class SessionError(Exception):
    """Used to raise errors related to snmp session"""


class SwitchCommunication(object):
    """Universal class that contains only methods not specific for different
    manufacturers and does not affect their functionality.

    Under this class, it is possible to find classes for specific manufacturers
    (CiscoSwitch, HpSwitch, etc.)

    """

    oiddict = {'sysDescr': '.1.3.6.1.2.1.1.1.0',
               'ifAdminStatus': '.1.3.6.1.2.1.2.2.1.7',
               'ifDescr': '.1.3.6.1.2.1.2.2.1.2',
               'ifIndex': '.1.3.6.1.2.1.2.2.1.1',
               'dot1dTpFdbPort': '.1.3.6.1.2.1.17.4.3.1.2',
               'dot1dTpFdbAddress': '.1.3.6.1.2.1.17.4.3.1.1',
               'vmVlan': '.1.3.6.1.4.1.9.9.68.1.2.2.1.2',
               'ifAlias': '.1.3.6.1.2.1.31.1.1.1.18',
               'ifPhysAddress': '.1.3.6.1.2.1.2.2.1.6',
               'vtpVlanName': '.1.3.6.1.4.1.9.9.46.1.3.1.1.4.1',
               'dot1qVlanStaticName': '.1.3.6.1.2.1.17.7.1.4.3.1.1',
               'dot1qVlanStaticPort': '.1.3.6.1.4.1.207.1.4.167.116.7.1.4.5.1.1',
               'dot1qVlanStaticForbidPorts': '.1.3.6.1.2.1.17.7.1.4.3.1.3',
               'dot1qVlanStaticEgressPorts': '.1.3.6.1.2.1.17.7.1.4.3.1.2',
               'dot1qVlanStaticUntaggedPorts': '.1.3.6.1.2.1.17.7.1.4.3.1.4',
               'dot1qVlanStaticTable': '.1.3.6.1.2.1.17.7.1.4.3.1.4',
               'dot1qPvid': '.1.3.6.1.2.1.17.7.1.4.5.1.1',
               'dot1dBasePortIfIndex': '.1.3.6.1.2.1.17.1.4.1.2',
               'dot3StatsIndex': '.1.3.6.1.2.1.10.7.2.1.1',
               'h3cvlanlist': '.1.3.6.1.4.1.2011.2.23.1.2.1.1.1.1',
               'ifTrunk': '.1.3.6.1.4.1.9.9.46.1.6.1.1.14',
               'ifOperStatus': '.1.3.6.1.2.1.2.2.1.8',
               'ipNetToMediaPhysAddress': '.1.3.6.1.2.1.4.22.1.2',
               'ipNetToMedia': '1.3.6.1.2.1.4.35',
               'vlanTrunkPortVlansEnabled': '1.3.6.1.4.1.9.9.46.1.6.1.1.4',
               'cpsIfPortSecurityEnable': '.1.3.6.1.4.1.9.9.315.1.2.1.1.1',
               'cpsIfMaxSecureMacAddr': '.1.3.6.1.4.1.9.9.315.1.2.1.1.3',
               'vtpRevNum': '.1.3.6.1.4.1.9.9.46.1.2.1.1.4.1',
               'poe_pwr': '.1.3.6.1.4.1.9.9.402.1.2.1.9',
               'poe_pwr_index': '.1.3.6.1.4.1.9.9.402.1.2.1.11',
               'download_config': '.1.3.6.1.4.1.9.2.1.55',
               'upload_config': '.1.3.6.1.4.1.9.2.1.53',
               'copy_run_start': '.1.3.6.1.4.1.9.2.1.54.0',
               'ifType': '.1.3.6.1.2.1.2.2.1.3',
               'jnxExVlanPortAccessMode': '.1.3.6.1.4.1.2636.3.40.1.5.1.7.1.5',
               'jnxExVlanTag': '.1.3.6.1.4.1.2636.3.40.1.5.1.5.1.5',
               'dot1qTpFdbEntry': '.1.3.6.1.2.1.17.7.1.2.2.1.2',
               'hpSwitchIgmpVlanIndex': '.1.3.6.1.4.1.11.2.14.11.5.1.7.1.15.1.1.1',
               }

    def __init__(self, id_switch):
        """
        Instance of the class for communication with different devices.
        This class takes care of all communication with devices.
        :param id_switch: integer with switch ID
        """

        self.switch = Switch.objects.get(id_switch=id_switch)
        try:
            int(self.switch.snmp_version)
        except ValueError:
            raise TypeError(
                'version must be integer, or string which contains integer')
        self.session = netsnmp.Session(DestHost=self.switch.dest_host, Version=self.switch.snmp_version, Community=self.switch.community)
        self.session.UseLongNames = 1
        self.session.UseNumeric = 1
        self.ports = []

    def get(self, oid):
        var = netsnmp.VarList(oid)
        self.session.get(var)
        if len(self.session.ErrorStr) > 0:
            raise SessionError(self.session.ErrorStr)
        return var

    def set(self, oid):
        var = netsnmp.VarList(oid)
        netsnmp.verbose = 1
        self.session.set(var)
        if len(self.session.ErrorStr) > 0:
            raise SessionError(self.session.ErrorStr)
        return var

    def walk(self, oid):
        vars = netsnmp.VarList(oid)
        self.session.walk(vars)
        if len(self.session.ErrorStr) > 0:
            raise SessionError(self.session.ErrorStr)
        return vars

    def format_mac(self, mac):
        mac = re.findall("..", mac)
        i = 0
        for byte in mac:
            if i == 0:
                temp = str(byte)
                i = 1
            else:
                temp = temp + ":" + str(byte)
                i = 1
        return temp

    def get_port_macs(self, port):
        """Method returns tuple [MAC of port, MAC of connected devices]
        It gets own MAC address from the device and then finds
        what addresses are connected to the device.
        """
        oid = self.oiddict["dot1dTpFdbPort"]
        ports = self.walk(oid)
        connected_macs = []
        # if something is connected to the port, find what it is.
        if port in ports:
            i = 0
            mac_index = []
            for temp in ports:
                if port == temp:
                    mac_index.append(i)
                i += 1

            oid = netsnmp.Varbind(self.oiddict["dot1dTpFdbAddress"])
            macs = self.walk(oid)[0].val
            for index in mac_index:
                connected_macs.append(self.format_mac(macs[index].encode("hex").upper()))
        else:
            connected_macs = "None"

        return [connected_macs]

    def get_port_alias(self, port):
        """
        Returns alias on specified port
        @rtype: str
        @param port: ifIndex
        @return: ifAlias
        """
        oid = netsnmp.Varbind(self.oiddict["ifAlias"], port)
        alias = self.get(oid)[0].val
        return alias

    def get_port_descr(self, port):
        oid = netsnmp.Varbind(self.oiddict["ifDescr"], port)
        descr = self.get(oid)
        return descr

    def get_port_status(self, port):
        oid = netsnmp.Varbind(self.oiddict["ifAdminStatus"], port)
        descr = self.get(oid)[0].val
        return descr

    def set_port_status(self, port, status):
        oid = netsnmp.Varbind(self.oiddict["ifAdminStatus"], port, status, 'INTEGER')
        descr = self.set(oid)
        return descr

    def get_oper_status(self, port):
        oid = netsnmp.Varbind(self.oiddict['ifOperStatus'], port)
        oper_status = self.get(oid)[0].val
        return oper_status

    def is_port_trunk(self, port):
        oid = netsnmp.Varbind(self.oiddict["ifTrunk"], port)
        trunk = self.get(oid)
        if trunk == '2':
            return False
        else:
            return True

    def change_port_alias(self, port, port_name, new_alias):
        """
        Method for port alias change.
        @rtype: bool
        @param port: ifIndex
        @param port_name: string with ifAlias
        @param new_alias: string with new ifAlias
        @return: 1 if change is done correctly, 2 in case of some problem.
        """
        oid = netsnmp.Varbind(self.oiddict["ifAlias"], port, new_alias, 'OCTETSTR')
        self.set(oid)
        sleep(1)
        alias = self.get_port_alias(port)
        if alias == new_alias:
            return True
        else:
            return False

    def get_port_descriptions(self):
        """Method returns dictionary with [index = ifDescr] for every port.
        It returns only ethernet ports, not TenGig, because portmanager is not
        intended to manage these ports.
        """

        def valid(string):
            if 'Ethernet' in string:
                return True
            else:
                return False

        oid = netsnmp.Varbind(self.oiddict['ifDescr'])
        try:
            indexes = self.walk(oid)
        except:
            return None
        ids = []
        names = []
        for index in indexes:
            if valid(index.val):
                ids.append(index.iid)
                names.append(index.val)
        port_array = dict(zip(ids, names))

        return port_array


class GeneralSwitch(SwitchCommunication):

    def get_port_vlan(self, port):
        """
        Returns VLAN set on specific port
        @rtype: str
        @param port: ifIndex
        @return: Vlan ID
        """
        pass

    def set_vlan_membership(self, port, port_name, vlan):
        """
        Sets VLAN on specific port.
        @rtype: bool
        @param port: ifIndex
        @param port_name: port name
        @param vlan: vlan index
        @return: true if change is successfull
        """
        pass

    def get_connected_macs(self, port, vlan_name):
        """
        Gets MAC adresses of devices connected to single port.
        @rtype: list
        @param port: ifIndex
        @param vlan_name: VLAN name
        @return: list of MAC adrresses
        """
        return self.get_port_macs(port)

    def render_ports(self, user):
        """
        Returns all available port parameters on switch
        @rtype: object
        @param user: user from request for permission check
        @return: Switch object
        """
        pass

    def render_vlans(self, user):
        """
        Returns all available vlans on switch
        @rtype: object
        @param user: user from request for permission check
        @return: Switch object
        """
        pass

    def turn_on_off(self, port):
        """
        Function checks status of the port, if it is turned on/off
        [on: 1, off: 2]. Then if it is necessary, it turns on/off the port
        and validates if the change by method get_port_status.
        :param port:
        :return:
        """
        try:
            status_set = self.get_port_status(port)
        except:
            return 0
        if status_set == '1':
            control = "2"
        elif status_set == '2':
            control = "1"
        else:
            control = "1"
        try:
            self.set_port_status(port, control)[0].val
        except:
            return 0
        try:
            status = self.get_port_status(port)
            if status == control:
                return 1
            else:
                return 0
        except:
            return 0

    def get_changeable_port_information(self, port, user):
        """
        Returns changed attributes on device port.
        @rtype: list
        @param port: ifIndex
        @param user: user from request
        @return: [alias, status, vlan, vlan_name]
        """
        pass

    def get_port_pwr_consumption(self, ports):
        pass

    def get_port_sec(self, port):
        pass

    def set_port_sec(self, port, mac_count):
        pass

    def apply_port_default_config(self, port_id, port_name):
        """
        Applies default config from Device default_port_config field
        @rtype: bool
        @param port_id: ifIndex
        @param port_name: port name
        @return: true if it is set correctly
        """
        pass

    def copy_run_start(self):
        """
        Copies running config to device persistent memory
        @return:
        """
        pass

    def transfer_bulk_port_vals(self, port_id, port_name, port_alias, vlan_id, port_sec, status):
        """
        Sets destination port during port transfer
        (bulk operation to remove overhead with e.g. commits on Junos devices)
        @rtype: bool
        @param status:
        @param port_id:
        @param port_name:
        @param port_alias:
        @param vlan_id:
        @param vlan_name:
        @param port_sec:
        @return: true if it is set correctly
        """
        pass

    def transfer_src_default_config(self, port_id, port_name):
        """
        Sets source port during port transfer to blank description and turns it off
        (bulk operation to remove overhead with e.g. commits on Junos devices)
        @rtype: bool
        @param port_name:
        @param port_id:
        @return: true if it is set correctly
        """
        pass
