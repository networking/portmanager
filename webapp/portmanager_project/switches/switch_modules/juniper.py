from general import SessionError
from general import GeneralSwitch
from ..users import *
import json
from fastsnmpy import SnmpSession
from operator import itemgetter
from django.core.cache import cache
import sys
from time import sleep
from jnpr.junos import Device as Junos_Device
from jnpr.junos.utils.config import Config

import netsnmp

from ..models import *


sys.path.append('../')


class JuniperSwitch(GeneralSwitch):
    """Subclass of SwitchCommunication with extended functionality for devices
    made by Juniper.
    """

    def get_dot1_table(self):
        oid = self.oiddict['dot1dBasePortIfIndex']
        try:
            table = self.walk(oid)
        except:
            return None
        ids = []
        names = []
        for vlan in table:
            ids.append(vlan.iid)
            names.append(vlan.val)
        return dict(zip(ids, names))

    def get_mac_table(self, vlan):
        oid = self.oiddict['dot1qTpFdbEntry'] + '.' + vlan
        try:
            table = self.walk(oid)
        except:
            return None
        ids = []
        names = []
        for vlan in table:
            ids.append(vlan.tag)
            names.append(vlan.val)
        return dict(zip(ids, names))

    def get_vlan_table(self):
        oid = self.oiddict['dot1qVlanStaticName']
        try:
            table = self.walk(oid)
        except:
            return None
        ids = []
        names = []
        for vlan in table:
            ids.append(vlan.iid)
            names.append(vlan.val)
        return dict(zip(ids, names))

    def get_vlan_tags(self):
        oid = self.oiddict['jnxExVlanTag']
        try:
            table = self.walk(oid)
        except:
            return None
        ids = []
        names = []
        for vlan in table:
            ids.append(vlan.iid)
            names.append(vlan.val)
        return dict(zip(ids, names))

    def get_vlan_port_access(self):
        oid = self.oiddict['jnxExVlanPortAccessMode']
        try:
            table = self.walk(oid)
        except:
            return None
        ids = []
        names = []
        for vlan in table:
            if str(vlan.val) != '1':
                continue
            ids.append(vlan.tag[-1])
            names.append(vlan.iid)

        return dict(zip(names, ids))

    def render_ports(self, user):

        groups = from_uco_load_groups(user.id_person)
        ports = Vlans.objects.filter(
            switch=self.switch, groups__in=groups).distinct()
        oids = [self.oiddict['ifDescr'], self.oiddict['ifAlias'], self.oiddict['ifOperStatus'],
                self.oiddict['ifAdminStatus'], self.oiddict['ifType'], self.oiddict['dot1dBasePortIfIndex'],
                self.oiddict['dot1qVlanStaticName'],
                ]

        fast_session = SnmpSession(targets=[self.switch.dest_host], oidlist=oids, community=self.switch.community)
        results = fast_session.snmpbulkwalk(workers=15)
        port_dict = {}
        switch_id = str(self.switch.id_switch)
        location_id = str(self.switch.location.id_location)

        results = json.loads(results)

        descr_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.2'], results))
        type_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.3'], results))
        alias_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.31.1.1.1.18'], results))
        oper_status_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.8'], results))
        admin_status_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.7'], results))
        dot_1_indexes_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.17.1.4.1.2'], results))

        # process vlan to port mapping
        vlans = self.get_vlan_table()
        vlan_tags = self.get_vlan_tags()

        # get_vlan_port_access {dot1_id: vlan_id}
        port_vlans = self.get_vlan_port_access()

        # process ports
        for port in descr_list:
            port_struct = {}
            port_struct['iid'] = port['iid']
            port_struct['descr'] = port['val']
            if port_struct['descr'] is None:
                continue
            if 'fe-' not in port_struct['descr'] or 'ge-' in port_struct['descr'] or 'xe-' in port_struct['descr']:
                continue

            # check interface type
            if_type = list(filter(lambda d: d['iid'] in [port['iid']], type_list))[0]['val']
            if str(if_type) != '6' and str(if_type) != '53':
                continue

            alias = list(filter(lambda d: d['iid'] in [port['iid']], alias_list))
            if len(alias) > 0:
                port_struct['alias'] = alias[0]['val']

            # find vlan
            vlan_id = ''
            dot1_id = list(filter(lambda d: d['val'] in [port['iid']], dot_1_indexes_list))
            if dot1_id:
                try:
                    vlan_id = port_vlans[dot1_id[0]['iid']]
                except:
                    print "cant find vlan"

            if vlan_id != "":
                vlan_name = vlans[vlan_id]
                port_struct['vlan'] = vlan_tags[vlan_id] + " - " + vlan_name
            else:
                continue

            admin_status = list(filter(lambda d: d['iid'] in [port['iid']], admin_status_list))
            oper_status = list(filter(lambda d: d['iid'] in [port['iid']], oper_status_list))
            if len(admin_status) > 0 and len(oper_status) > 0:
                if admin_status[0]['val'] == '1' and oper_status[0]['val'] == '1':
                    port_struct['status'] = 0

                elif admin_status[0]['val'] == '1' and oper_status[0]['val'] != '2':
                    port_struct['status'] = 1

                elif admin_status[0]['val'] == '2':
                    port_struct['status'] = 2
            else:
                port_struct['status'] = 3

            read_only = 0
            port_matched = False
            for group_of_ports in ports:
                instance = RegExp(group_of_ports.regex)
                tuple = instance.getTuple()
                for port_def in tuple:
                    if (str(port_def)) == str(port['iid']):
                        if group_of_ports.view_only and (not port_matched):
                            read_only = 1
                            port_matched = True
                        else:
                            read_only = 0
                            port_matched = True
            port_struct['read_only'] = str(read_only)
            cache.set(
                'port_' + location_id + '_' + switch_id + '_' + port_struct['descr'],
                port_struct['alias'],
                timeout=int(environ.get('CACHE_TTL'))
            )
            port_dict[int(port['iid'])] = port_struct

        self.switch.porty = sorted(port_dict.items(), key=itemgetter(0))

        return self.switch

    def render_vlans(self, user):
        """
        :param user:
        :return:
        """
        groups = from_uco_load_groups(user.id_person)
        vlans = Vlans.objects.filter(
            switch=self.switch, groups__in=groups).distinct()
        list_of_allowed_vlans = []
        loaded_vlans = self.get_vlan_table()
        vlan_tags = self.get_vlan_tags()

        vlans_processed = {}
        for id, vlan in loaded_vlans.items():
            vlans_processed[vlan_tags[id]] = vlan
        # could switch be with no vlans?
        if not loaded_vlans:
            raise SessionError("Switch ID" + str(self.switch.id_switch) +
                               " does not have VLANs")

        for group_of_vlans in vlans:
            instance = RegExp(group_of_vlans.regex)
            tuple = instance.getTuple()
            for vlan in tuple:
                if (str(vlan)) in vlans_processed:
                    vlan_name = vlans_processed.get(str(vlan))
                    if group_of_vlans.view_only:
                        list_of_allowed_vlans.append((str(vlan), vlan_name, 1))
                    else:
                        if (str(vlan), vlan_name, 1) in list_of_allowed_vlans:
                            list_of_allowed_vlans.remove((str(vlan), vlan_name, 1))
                            list_of_allowed_vlans.append((str(vlan), vlan_name, 0))
                        else:
                            list_of_allowed_vlans.append((str(vlan), vlan_name, 0))

        # Sort vlans and remove duplicates
        self.switch.vlany = sorted(list(set(list_of_allowed_vlans)),
                                   key=lambda x: int(x[0]))
        return self.switch

    def format_mac_address(self, macint):
        if type(macint) != int:
            raise ValueError('invalid integer')
        return ':'.join(['{}{}'.format(a, b)
                         for a, b
                         in zip(*[iter('{:012x}'.format(macint))] * 2)])

    def get_connected_macs(self, port, vlan):
        """
        :param port:
        :param vlan:
        :return:
        """
        try:
            int(port)
        except ValueError:
            raise TypeError("Port id must be number")

        macs = []
        vlan_dot1 = ''
        vlan_table = self.get_vlan_tags()
        for dot1, id in vlan_table.items():
            if id == vlan:
                vlan_dot1 = dot1
        macs_table = self.get_mac_table(vlan_dot1)
        dot1_port_table = self.get_dot1_table()
        dot1_port = ''
        for dot1_id, id in dot1_port_table.items():
            if id == port:
                dot1_port = dot1_id

        for mac, port_id in macs_table.items():
            if port_id == dot1_port:
                formated_mac = self.format_mac_address(int(mac[30:].replace('.', '')))
                macs.append(formated_mac)

        return macs

    def change_port_alias(self, port, port_name, new_alias):
        """Method for port alias change.
        Returns 1 if change is done, 2 in case of some problem.
        """
        cmd = 'set interfaces ' + port_name + ' description \"' + new_alias + '\"'

        priv_key = '/srv/ssh_keys/' + str(self.switch.dest_host)
        dev = Junos_Device(host=self.switch.dest_host,
                           user='portmanager',
                           ssh_private_key_file=priv_key,
                           gather_facts=False,
                     )
        dev.open()

        with Config(dev) as change:
            try:
                change.load(cmd, format='set')
                change.commit(timeout=60)
                dev.close()
                return True
            except Exception as err:
                print err
                dev.close()
                return False

    def get_port_vlan(self, port):
        """Returns VLAN set on specific port"""
        oids = [self.oiddict['ifDescr'], self.oiddict['ifAlias'], self.oiddict['ifOperStatus'],
                self.oiddict['ifAdminStatus'], self.oiddict['ifType'], self.oiddict['dot1dBasePortIfIndex'],
                self.oiddict['dot1qVlanStaticName'],
                ]
        fast_session = SnmpSession(targets=[self.switch.dest_host], oidlist=oids, community=self.switch.community)
        results = fast_session.snmpbulkwalk(workers=15)
        results = json.loads(results)
        dot_1_indexes_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.17.1.4.1.2'], results))
        port_vlans = self.get_vlan_port_access()
        vlans = self.get_vlan_table()
        vlan_tags = self.get_vlan_tags()

        # find vlan
        vlan_id = ''
        dot1_id = list(filter(lambda d: d['val'] in port, dot_1_indexes_list))
        if dot1_id:
            try:
                vlan_id = port_vlans[dot1_id[0]['iid']]
            except:
                print "cant find vlan"
        vlan_result = vlan_tags[vlan_id]

        return vlan_result

    def set_vlan_membership(self, port, port_name, new_status):
        """Sets VLAN on specific port.
        """
        oids = [self.oiddict['ifDescr']]
        fast_session = SnmpSession(targets=[self.switch.dest_host], oidlist=oids, community=self.switch.community)
        results = fast_session.snmpbulkwalk(workers=15)
        results = json.loads(results)
        descr_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.2'], results))

        vlans = self.get_vlan_table()
        vlan_tags = self.get_vlan_tags()

        # get port name for set commad from ID
        port_name = str(list(filter(lambda d: d['iid'] in port, descr_list))[0]['val'])

        # get vlan name for set command
        vlan_name = ''
        for id, status in vlan_tags.items():
            if status == new_status:
                vlan_name = vlans[id]
        del_cmd = 'delete interfaces ' + port_name + ' family ethernet-switching vlan members'
        cmd = 'set interfaces ' + port_name + ' family ethernet-switching vlan members ' + vlan_name

        priv_key = '/srv/ssh_keys/' + str(self.switch.dest_host)
        dev = Junos_Device(host=self.switch.dest_host,
                           user='portmanager',
                           ssh_private_key_file=priv_key,
                           gather_facts=False,
                           )
        dev.open()

        with Config(dev) as change:
            try:
                change.load(del_cmd, format='set')
                change.load(cmd, format='set')
                change.commit(timeout=60)
                dev.close()
                return True
            except Exception as err:
                print err
                dev.close()
                return False

    def turn_on_off(self, port):
        """
        Function checks status of the port, if it is turned on/off
        [on: 1, off: 2]. Then if it is necessary, it turns on/off the port
        and validates if the change by method get_port_status.
        :param port:
        :return:
        """
        oids = [self.oiddict['ifDescr']]
        fast_session = SnmpSession(targets=[self.switch.dest_host], oidlist=oids, community=self.switch.community)
        results = fast_session.snmpbulkwalk(workers=15)
        results = json.loads(results)
        descr_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.2'], results))

        # get port name for set commad from ID
        port_name = str(list(filter(lambda d: d['iid'] in port, descr_list))[0]['val'])
        cmd = 'delete interfaces ' + port_name + ' disable'

        priv_key = '/srv/ssh_keys/' + str(self.switch.dest_host)
        dev = Junos_Device(host=self.switch.dest_host,
                           user='portmanager',
                           ssh_private_key_file=priv_key,
                           gather_facts=False,
                     )
        dev.open()
        try:
            status_set = self.get_port_status(port)
        except:
            return 0
        if status_set == '1':
            cmd = 'set interfaces ' + port_name + ' disable'
            control = "2"
        elif status_set == '2':
            cmd = 'delete interfaces ' + port_name + ' disable'
            control = "1"
        else:
            control = "1"
        with Config(dev) as change:
            try:
                change.load(cmd, format='set')
                change.commit(timeout=60)
                dev.close()
            except Exception as err:
                dev.close()
                return 0
        timeout = 60
        while timeout > 0:
            try:
                status = self.get_port_status(port)
                if status == control:
                    return 1
                else:
                    sleep(1)
            except:
                sleep(1)
        return 0

    def get_changeable_port_information(self, port, user):

        vlans = self.get_vlan_table()
        vlan_tags = self.get_vlan_tags()
        alias = self.get_port_alias(port)
        status = self.get_port_status(port)
        vlan = self.get_port_vlan(port)

        vlan_name = ''
        for id, status in vlan_tags.items():
            if status == vlan:
                vlan_name = vlans[id]

        oper_status = self.get_oper_status(port)

        if not alias:
            alias = 'None'

        status = int(status)
        oper_status = int(oper_status)

        if status == 1 and oper_status == 1:
            status = 0

        elif status == 1 and oper_status == 2:
            status = 1

        elif status == 2:
            status = 2

        else:
            status = 3

        port = [alias, status, vlan, vlan_name]


        return port

    def apply_port_default_config(self, port_id, port_name):
        new_config = (str(self.switch.device.default_port_config) + '\n').splitlines(True)
        priv_key = '/srv/ssh_keys/' + str(self.switch.dest_host)
        dev = Junos_Device(host=self.switch.dest_host,
                           user='portmanager',
                           ssh_private_key_file=priv_key,
                           gather_facts=False,
                           )
        dev.open()

        with Config(dev) as change:
            try:
                for line in new_config:
                    cmd = 'set interfaces ' + port_name + ' ' + line
                    change.load(cmd, format='set')
                change.commit(timeout=60)
                dev.close()
                return True
            except Exception as err:
                print err
                dev.close()
                return False

    def transfer_bulk_port_vals(self, port_id, port_name, port_alias, vlan_id, port_sec, status):
        priv_key = '/srv/ssh_keys/' + str(self.switch.dest_host)
        dev = Junos_Device(host=self.switch.dest_host,
                           user='portmanager',
                           ssh_private_key_file=priv_key,
                           gather_facts=False,
                           )
        dev.open()

        with Config(dev) as change:
            try:
                if status == '2':
                    if_status = 'set interfaces ' + port_name + '  disable'
                    change.load(if_status, format='set')
                descr = 'set interfaces ' + port_name + ' description ' + port_alias
                change.load(descr, format='set')
                del_vlan = 'delete interfaces ' + port_name + ' family ethernet-switching vlan members'
                change.load(del_vlan, format='set')
                new_vlan = 'set interfaces ' + port_name + ' family ethernet-switching vlan members ' + vlan_name
                change.load(new_vlan, format='set')
                change.commit(timeout=60)
                dev.close()
                return True
            except Exception as err:
                print err
                dev.close()
                return False

    def transfer_src_default_config(self, port_id, port_name):
        priv_key = '/srv/ssh_keys/' + str(self.switch.dest_host)
        dev = Junos_Device(host=self.switch.dest_host,
                           user='portmanager',
                           ssh_private_key_file=priv_key,
                           gather_facts=False,
                           )
        dev.open()

        with Config(dev) as change:
            try:
                status = 'set interfaces ' + port_name + '  disable'
                descr = 'delete interfaces ' + port_name + ' description'
                change.load(status, format='set')
                change.load(descr, format='set')
                change.commit(timeout=60)
                dev.close()
                return True
            except Exception as err:
                print err
                dev.close()
                return False
