import multiprocessing

import netsnmp

from general import SwitchCommunication
from ..models import *
from ..users import *


class HpSwitch(SwitchCommunication):
    """

        $$\   $$\ $$$$$$$\
        $$ |  $$ |$$  __$$\
        $$ |  $$ |$$ |  $$ |
        $$$$$$$$ |$$$$$$$  |
        $$  __$$ |$$  ____/
        $$ |  $$ |$$ |
        $$ |  $$ |$$ |
        \__|  \__|\__|   h3c h3c h3c h3c


    """

    def __init__(self, id_switch):
        self.switch = Switch.objects.get(id_switch=id_switch)
        self.s = SNMPSessionClass(DestHost=self.switch.dest_host,
                                  Version=self.switch.snmp_version,
                                  Community=self.switch.community)
        self.ports = []
        self.result_queue = multiprocessing.Queue()
        self.s.oid = netsnmp.Varbind(
            self.s.oiddict['dot1qVlanStaticUntaggedPorts'])

        self.list_of_untagged_ports = self.s.walk()

        self.s.oid = netsnmp.Varbind(self.s.oiddict['h3cvlanlist'])

        self.vlans = self.s.walk()

        max = 0

        self.list_of_untagged_ports = map(
            lambda x:
            str(bin(int(x.encode('hex'), 16)))[2:],
            self.list_of_untagged_ports)

        for x in self.list_of_untagged_ports:
            length = len(x)
            if length > max:
                max = length
        temp_array = []

        for x in self.list_of_untagged_ports:
            while len(x) < max:
                x = '0' + x
            temp_array.append(x)

        structure = {}

        if len(temp_array) == len(self.vlans):
            for i in range(len(self.list_of_untagged_ports)):
                structure[self.vlans[i]] = temp_array[i]

        else:
            raise IOError('Loading of VLANs failed')
        self.vlan_structure = structure

    def turn_on_off(self, port):
        """
        Function checks status of the port, if it is turned on/off
        [on: 1, off: 2]. Then if it is necessary, it turns on/off the port
        and validates if the change by method get_port_status.
        """
        port = self.get_simple_port_index(int(port))
        self.s.oid = netsnmp.Varbind(
            self.s.oiddict["ifAdminStatus"],
            port,
            '1',
            'INTEGER')
        if self.s.get()[0] == '1':
            self.s.oid = netsnmp.Varbind(
                self.s.oiddict["ifAdminStatus"],
                port,
                '2',
                'INTEGER')
            control = "2"
        elif self.s.get()[0] == '2':
            self.s.oid = netsnmp.Varbind(
                self.s.oiddict["ifAdminStatus"],
                port,
                '1',
                'INTEGER')
            control = "1"
        else:
            control = "1"
            self.s.oid = netsnmp.Varbind(
                self.s.oiddict["ifAdminStatus"],
                port,
                '1',
                'INTEGER')
        self.s.set()
        status = self.get_port_status(port)

        if status == control:
            return 1
        else:
            return 0

    def change_port_alias(self, port, new_alias):
        port = self.get_simple_port_index(int(port))

        self.s.oid = netsnmp.Varbind(
            self.s.oiddict["ifAlias"],
            port,
            new_alias,
            'OCTETSTR')

        h = self.s.set()
        alias = self.get_port_alias(port)
        if alias == new_alias:
            return 1
        else:
            return 2

    def get_port_macs(self, port):
        self.s.oid = netsnmp.Varbind(self.s.oiddict['ifPhysAddress'], port)
        own_mac = self.s.get()[0].encode('hex')
        own_mac = self.format_mac_address(own_mac).upper()
        # self.s.oid = netsnmp.Varbind(self.s.oiddict[])

        connected_macs = 'None'

        return [own_mac, connected_macs]

    def get_changeable_port_information(self, port, user):
        alias = self.get_port_alias(port)
        status = self.get_port_status(port)
        vlan = self.get_port_vlan(port)

        port = [alias, status, vlan[0], vlan[1]]

        return port

    def return_port_array(self, port):
        '''
        Returns array [descr, alias, own_mac, connected_macs, status,
        port[1],str(port[0]),vlan, self.get_vmVlan(vlan)]
        '''
        macs = self.get_port_macs(str(port[0]))
        alias = self.get_port_alias(str(port[0]))
        descr = self.get_port_descr(str(port[0]))
        status = self.get_port_status(str(port[0]))
        oper_status = self.get_oper_status(str(port[0]))

        try:
            status = int(status)
            oper_status = int(oper_status)
        except:
            status = 1
            oper_status = 1

        if status == 1 and oper_status == 1:
            status = 0

        elif status == 1 and oper_status == 2:
            status = 1

        elif status == 2 and oper_status == 2:
            status = 2
        else:
            status = 3

        own_mac = macs[0]
        connected_macs = macs[1]
        vlan = self.get_port_vlan(str(port[1]))
        self.result_queue.put([descr, alias, own_mac, connected_macs, status,
                               port[2], str(port[1]), vlan, self.get_name_of_vlan(vlan)])

    def get_port_vlan(self, port):
        '''Returns VLAN set on specific port'''

        port = int(port) - 1

        for i in self.vlan_structure.iterkeys():
            value = self.vlan_structure[i]
            if value[port] == '1':
                return i

        return 0

    def get_port_index(self, port):
        self.s.oid = netsnmp.Varbind(self.s.oiddict['dot3StatsIndex'])
        indexes = self.s.walk()

        return (int(indexes[(port[0] - 1)]), port[0], port[1])

    def get_simple_port_index(self, port):
        self.s.oid = netsnmp.Varbind(self.s.oiddict['dot3StatsIndex'])
        indexes = self.s.walk()
        return indexes[int(port) - 1]

    def render_switch_ports(self, set_of_ports, user):
        print "render"
        ports_of_switch = []
        array_of_processes = []

        for port in set_of_ports:
            # For every port, get data about MAC address, alias,
            # description, etc. from SwitchCommunication
            port = self.get_port_index(port)
            p = multiprocessing.Process(
                target=self.return_port_array, args=(port,))
            array_of_processes.append(p)

        for job in array_of_processes:
            job.start()

        for job in array_of_processes:
            job.join()

        for element in array_of_processes:
            ports_of_switch.append(self.result_queue.get())
        ports_of_switch = sorted(ports_of_switch, key=(lambda x: int(x[6])))
        groups = from_uco_load_groups(user.id_person)
        vlans = Vlans.objects.filter(
            switch=self.switch,
            groups__in=groups).distinct()

        list_of_allowed_vlans = []

        # save cached VLANs to the memory and compare with regex to show only
        # VLANs actually set up on device.
        loaded_vlans = []
        for vlan in self.vlan_structure.iterkeys():
            loaded_vlans.append(vlan)

        for group_of_vlans in vlans:
            instance = RegExp(group_of_vlans.regex)
            tuple = instance.getTuple()
            for vlan in tuple:
                if (str(vlan)) in loaded_vlans:
                    if group_of_vlans.view_only:
                        list_of_allowed_vlans.append((str(vlan), 1))
                    else:
                        if (str(port), 1) in list_of_allowed_vlans:
                            list_of_allowed_vlans.remove((str(vlan), 1))
                            list_of_allowed_vlans.append((str(vlan), 0))
                        else:
                            list_of_allowed_vlans.append((str(vlan), 0))

        list_of_allowed_vlans = map(
            lambda x_y2: (x_y2[0],
                          self.get_name_of_vlan(x_y2[0]),
                          x_y2[1]),
            list_of_allowed_vlans)

        self.switch.porty = ports_of_switch
        # sort VLANs by its number and remove duplicates
        self.switch.vlany = sorted(
            list(set(list_of_allowed_vlans)),
            key=lambda x: int(x[0]))

        return self.switch

    def set_vlan_membership(self, portnumber, vlan):
        portnumber = self.get_simple_port_index(portnumber)
        self.set_port_membership(portnumber, vlan, '3')

        return 0

    def get_port_bitmap_location(self, portnumber):
        '''
        Finds out where in the bit string the port is located.
        :param portnumber
        '''
        for i in range(1, 16):
            if portnumber < 8 * i:
                return i
        return 0

    def format_bitstring(self, bitstring):
        '''
        Returns an eight-bit map from an originaly incomplete eight-bit map.
        '''
        bitstring = str(bitstring)

        for i in range(0, 8 - len(bitstring)):
            bitstring = '0' + bitstring

        return bitstring

    def get_id_of_vlans(self):
        # vrati vsechny identifikacni cisla VLAN, vyskytujicich se na zarizeni,
        # -----------------------------------------------------------------------------------------------------DOPLNIT
        # proc je vraci vetsi o 62, POZNAMKA: zeptat se nekoho
        self.s.oid = ".1.3.6.1.4.1.11.2.14.11.5.1.7.1.15.1.1.1"
        vlanidlist = self.s.walk()

        return vlanidlist

    def refresh_vlan_list(self):
        vlans = self.get_id_of_vlans()

        vlan_list = []

        for vlan in vlans:
            vlan_list.append([vlan, self.get_name_of_vlan(vlan)])

        return vlan_list

    def is_it_untagged(self, port, vlan):
        # urci jestli port nalezi do dane vlany
        #
        self.s.oid = netsnmp.Varbind(
            self.s.oiddict["dot1qVlanStaticUntaggedPorts"])
        list_of_untagged_ports = self.s.walk()

        position_in_bitsequence = ((int(port) - 1) % 8)

        list_of_untagged_ports = map(ord, list_of_untagged_ports)
        port_position = self.get_port_bitmap_location(port)
        position = (list_of_untagged_ports[port_position])
        position = (bin(position))[2:]
        position = self.format_bitstring(position)
        information = position[position_in_bitsequence]

        if information == "1":
            return True
        else:
            return False

    def set_port_membership(self, port, vlan, new_status):
        # nastavi prislusnost port pro VLAN
        #
        #  0  - No
        #  1  - Forbid
        #
        #  2  - Tagged
        #  3  - Untagged
        # forbid  .1.3.6.1.2.1.17.7.1.4.3.1.3
        old_status = self.get_port_membership(port, vlan)
        vlans = []

        for vlan in self.vlan_structure.iterkeys():
            vlans.append(vlan)
        vlans.sort()

        if old_status == new_status:
            return "Old status is same as new status."

        # metoda nejprve overi, zda puvodni stav neni stejny jako novy stav,
        # pote prejde k samotnemu nastavovani. Pokud je nas novy status No,
        # musime nejprve vypnout Egress a pote nastavit Forbid na 0, tim se
        # nastavi na pozadovanem miste No
        if new_status == 0:
            self.set_membership(port, vlan, "dot1qVlanStaticEgressPorts", 0)
            self.set_membership(port, vlan, "dot1qVlanStaticForbidPorts", 0)

        # pokud chceme novy port nastavit na Forbid, postup je podobny jako
        # v predchozim pripade, ale tentokrat forbid se nastavi na 1

        elif new_status == 1:
            if old_status == 0:
                self.set_membership(
                    port,
                    vlan,
                    "dot1qVlanStaticForbidPorts",
                    1)
            else:
                self.set_membership(
                    port,
                    vlan,
                    "dot1qVlanStaticEgressPorts",
                    0)
                self.set_membership(
                    port,
                    vlan,
                    "dot1qVlanStaticForbidPorts",
                    1)

        # pokud chceme nastavit port na tagged, musime zjistit, zda puvodne byl
        # port egress na portu nastaven na 1/0.
        # pokud egress nebyl nastaven na 1, tak jej nejprve nastavime na 1 a
        # pak untagged vypneme

        elif new_status == 2:
            if old_status == 0 or old_status == 1:
                self.set_membership(
                    port,
                    vlan,
                    "dot1qVlanStaticEgressPorts",
                    1)
                self.set_membership(
                    port,
                    vlan,
                    "dot1qVlanStaticUntaggedPorts",
                    0)
            else:
                for vlanka in vlans:
                    untagged_exists_control = self.get_port_membership(
                        port, vlanka)
                    if untagged_exists_control == 3:
                        self.set_membership(
                            port,
                            vlanka,
                            "dot1qVlanStaticUntaggedPorts",
                            0)
                self.set_membership(
                    port,
                    vlan,
                    "dot1qVlanStaticUntaggedPorts",
                    0)
        # pokud chceme natsavit na untagged, musime nejprve overit,
        # jestli na danem portu, neni jina VLAN, untagged,
        # pokud ano, musime nejprve tu untagged nastavit na tagged a az pote,
        # tuto vlan nastavit na untagged
        elif new_status == 3:
            for vlanka in vlans:
                untagged_exists_control = self.get_port_membership(
                    port, vlanka)
                if untagged_exists_control == 3:
                    self.set_membership(
                        port,
                        vlanka,
                        "dot1qVlanStaticUntaggedPorts",
                        0)

            if old_status == 0 or old_status == 1:
                self.set_membership(
                    port,
                    vlan,
                    "dot1qVlanStaticEgressPorts",
                    1)
                self.set_membership(
                    port,
                    vlan,
                    "dot1qVlanStaticUntaggedPorts",
                    1)
            else:
                self.set_membership(
                    port,
                    vlan,
                    "dot1qVlanStaticUntaggedPorts",
                    1)

        else:
            return "incorrect condition"

    def get_port_membership(self, port, vlan):
        # vrati informaci jestli je port pro danou VLAN
        # na vstupu bere port, pro ktery ma urcit prislusnost
        # a take ID_vlany pro kterou se ma urcit
        #
        #  0  - No
        #  1  - Forbid
        #  2  - Tagged
        #  3  - Untagged
        #
        #  'dot1qVlanStaticEgressPorts': '.1.3.6.1.2.1.17.7.1.4.3.1.2',
        #  'dot1qVlanStaticUntaggedPorts': '.1.3.6.1.2.1.17.7.1.4.3.1.4',
        #  'dot1qVlanStaticPort' :'.1.3.6.1.4.1.207.1.4.167.116.7.1.4.5.1.1'

        self.s.oid = netsnmp.Varbind(
            self.s.oiddict["dot1qVlanStaticUntaggedPorts"],
            vlan)

        list_of_egress_ports = self.s.get()
        list_of_egress_ports = map(ord, list_of_egress_ports[0])

        port_position = self.get_port_bitmap_location(port)

        position = (list_of_egress_ports[port_position])
        position = (bin(position))[2:]
        position = self.format_bitstring(position)
        position_in_bitsequence = (int(port) - 1) % 8

        information = position[position_in_bitsequence]

        if information == '1':
            return '3'
        else:
            return '2'

        # pomoci SNMP vytahneme prvotni nezbytnou informaci
        # a to je, zda port ma nastaveny Egress na 1 / 0,
        # na zaklade toho muzeme rozhodnout o tom, zda je zapnuty/vypnuty
        # map(ord ) rozlozi prijaty string na byty v poli,se kterymi budeme pracovat
        # pomoci metody get_port_bitmap_location()
        # zjistime ve kterem useku pole je nas port
        # pote prevedeme tento usek na binarni retezec, z nejz odebereme prvni dva znaky
        # ktere znaci, ze se jedna o binarni retezec [0b]
        # nyni musime naformatovat string na korektni velikost
        # pokud jsou na zarizeni nastaveny na prvnich nekolik mistech nuly,
        # tak je tento string doplnen do pozadovane velikosti jednoho bytu nulami
        # pote se spocita pozice portu v retezci. tedy se nejprve odecte jednicka
        # (kvuli indexaci od nuly) a pote se vymoduluje 8, cimz zjistime konkretni pozici
        # nyni z naseho predtim zjisteneho positionu tedy zjistime nastaveni na portu
        # tyto operace se jeste dvakrat zopakuji pro untagged/forbid
        # pokud je onen konkretni egress bit nastaven na jednicku, tak vime, ze port
        # je bud tagged/untagged, pokud neni egress nastaven, tak vime, ze je bud zakazany
        # nebo neprirazeny, to tedy zjistime zavolanim getu na Untagged/Forbid
        # v one konkretni vetvi

        """
        if information == "1":
            self.s.oid = netsnmp.Varbind(self.s.oiddict["dot1qVlanStaticUntaggedPorts"], vlan)
            list_of_untagged_ports = self.s.get()

            list_of_untagged_ports = map(ord,list_of_untagged_ports[0])
            port_position = self.get_port_bitmap_location(port)

            position = (list_of_untagged_ports[port_position])
            position = (bin(position))[2:]
            position = self.format_bitstring(position)

            information = position[position_in_bitsequence]
            if information == "1":
                return 3
            else:
                return 2

        else:
            self.s.oid = netsnmp.Varbind(self.s.oiddict['dot1qVlanStaticForbidPorts'], vlan)

            list_of_egress_ports = self.s.get()
            list_of_egress_ports = map(ord,list_of_egress_ports[0])
            port_position = self.get_port_bitmap_location(port)
            position = (list_of_egress_ports[port_position])
            position = (bin(position))[2:]
            position = self.format_bitstring(position)

            position_in_bitsequence = (int(port)-1) % 8
            information = position[position_in_bitsequence]

            if information == "1":
                return 1
            else:
                return 0
        """

    def create_binary_changer(self, value):
        # vytvori 8 bytovy binarni retezec s jehoz pomoci se pote
        # na zaklade binarniho soucinu pronasobi hodnota, tak
        # aby zmenila pouze pozadovanou cast a mohla se nastavit
        #
        binary_string = "0"
        if value != 0:
            for i in range(0, value - 1):
                binary_string = "1" + binary_string
            for i in range(value - 1, 64):
                binary_string += "1"
        else:
            for i in range(0, 63):
                binary_string += "1"
        return binary_string

    def binary_and_for_strings(self, first, second):
        # metoda pro binarni soucin na dvou stringach
        #

        final = ""

        if len(first) == len(second):
            for i in range(0, len(first)):
                if first[i] == second[i]:
                    final = final + "1"
                else:
                    final = final + "0"
            return final
        else:
            return "Strings are not equally long"

    def from_string_make_hexval(self, string):
        # metoda ktera pro binarni retezec vrati
        # hexval nutny pro zaslani snmp pozadavku

        hex_dictionary = {'0000': '0',
                          '0001': '1',
                          '0010': '2',
                          '0011': '3',
                          '0100': '4',
                          '0101': '5',
                          '0110': '6',
                          '0111': '7',
                          '1000': '8',
                          '1001': '9',
                          '1010': 'A',
                          '1011': 'B',
                          '1100': 'C',
                          '1101': 'D',
                          '1110': 'E',
                          '1111': 'F'}

        hexval = ""
        for i in range(0, len(string), 4):

            if string[i:i + 4] in hex_dictionary:
                hexval = hexval + hex_dictionary[string[i:i + 4]]

        return hexval

    def set_membership(self, port, vlan, dict, state):
        self.s.oid = netsnmp.Varbind(self.s.oiddict[dict], vlan)
        untagged_ports = self.s.get()[0]
        port_position = self.get_port_bitmap_location(port)
        # position je promenna ve tvaru jednoho bytu, po nasledujicich v binarnim kodu
        # kod je puvodne uveden 0x proto oddelavame prvni dva prvky
        # position_in_bitsequence vypocita na kolikate pozici v poli se nachazi nas
        # pozadovany prvek

        position_in_bitsequence = (int(port) - 1) % 8
        # binary_string

        bit_position = (port_position * 8) + position_in_bitsequence

        # nyni untagged_ports encodujeme z hexa, pote ho prevedeme do desitkove soustavy,
        # oddelime prvni dva znaky, ktere oznacuji, ze je desitkovy
        # a prevedeme do binarni soustavy funkci bin()
        untagged_ports = bin(int(untagged_ports.encode("hex"), 16))[2:]

        # switche vraceji pokud udaj zacina nulami, tak zacinaji vracet, az se "neco deje"
        # tedy typicky nekolik prvnich bitu vynechaji, proto jej musime doplnit
        # do plneho tvaru osmi bytu

        if len(untagged_ports) != 64:
            for i in range(0, 64 - len(untagged_ports)):
                untagged_ports = "0" + untagged_ports

        # udelaem z "binarniho" stringu pole a zmenime bit na pozadovanou hodnotu 1,0
        # pote pole prevedeme zpet na string

        untagged_ports = list(untagged_ports)

        untagged_ports[bit_position] = str(state)
        untagged_ports = "".join(untagged_ports)

        # nyni pomoci metody from_string_make_hexval vytvorime hexadecimalni retezec
        # ktery predame switchi na nastaveni
        hexval = self.from_string_make_hexval(untagged_ports)
        # string = "snmpset -v 2c -c " + self.s.Community + " " + self.switch.dest_host + " " + self.s.oiddict[dict][1:] + "." + vlan + " x " + hexval
        # os.system(string)

        return 1

    def set_vlan_name(self, vlan_id, new_alias):
        # metoda, ktera umoznuje menit nastaveny alias na portu
        # obsahuje i funkci pro overeni, zda se opravdu zmena provedla
        # pokud ne, vrati cislo 2, coz je signal nastaleho problemu

        self.s.oid = netsnmp.Varbind(
            self.s.oiddict["dot1qVlanStaticName"], vlan_id,
            new_alias, 'OCTETSTR')
        self.s.set()

        self.s.oid = netsnmp.Varbind(
            self.s.oiddict["dot1qVlanStaticName"], vlan_id)
        alias = self.s.get()[0]

        if alias == new_alias:
            return 1
        else:
            return 0

    def get_name_of_vlan(self, vlan_id):
        self.s.oid = netsnmp.Varbind(
            self.s.oiddict['dot1qVlanStaticName'], vlan_id)
        return self.s.get()[0]
