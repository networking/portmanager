import json
import multiprocessing
import os
from operator import itemgetter
from time import sleep

import netsnmp
from fastsnmpy import SnmpSession

from general import GeneralSwitch
from general import SessionError
from ..models import *
from ..users import *


class OldHpSwitch(GeneralSwitch):
    """

        $$\   $$\ $$$$$$$\
        $$ |  $$ |$$  __$$\
        $$ |  $$ |$$ |  $$ |
        $$$$$$$$ |$$$$$$$  |
        $$  __$$ |$$  ____/
        $$ |  $$ |$$ |
        $$ |  $$ |$$ |
        \__|  \__|\__|


    """

    def get_port_descriptions(self):
        """Method returns dictionary with [index = ifDescr] for every port.
        It returns only ethernet ports, not TenGig, because portmanager is not
        intended to manage these ports.
        """

        def valid(string):
            if 'VLAN' not in string and 'loopback' not in string:
                return True
            else:
                return False

        oid = netsnmp.Varbind(self.oiddict['ifDescr'])
        try:
            indexes = self.walk(oid)
        except:
            return None
        ids = []
        names = []
        for index in indexes:
            print index.val

            if valid(index.val):
                ids.append(index.iid)
                names.append(index.val)
        port_array = dict(zip(ids, names))

        return port_array

    def render_ports(self, user):

        groups = from_uco_load_groups(user.id_person)
        ports = Vlans.objects.filter(
            switch=self.switch, groups__in=groups).distinct()
        oids = [self.oiddict['ifDescr'], self.oiddict['ifAlias'], self.oiddict['ifOperStatus'],
                self.oiddict['ifAdminStatus'],
                ]

        fast_session = SnmpSession(targets=[self.switch.dest_host], oidlist=oids, community=self.switch.community)
        results = fast_session.snmpbulkwalk(workers=15)
        port_dict = {}
        switch_id = str(self.switch.id_switch)
        location_id = str(self.switch.location.id_location)

        results = json.loads(results)
        descr_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.2'], results))
        alias_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.31.1.1.1.18'], results))
        oper_status_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.8'], results))
        admin_status_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.7'], results))

        loaded_vlans = self.get_vlan_table()

        for port in descr_list:
            port_struct = {}
            port_struct['iid'] = port['iid']
            port_struct['descr'] = port['val']
            if port_struct['descr'] is None:
                continue
            if 'VLAN' in port_struct['descr'] or 'loopback' in port_struct['descr']:
                continue

            alias = list(filter(lambda d: d['iid'] in [port['iid']], alias_list))
            if len(alias) > 0:
                port_struct['alias'] = alias[0]['val']
            else:
                port_struct['alias'] = 'None'
            vlan = self.get_port_vlan(port['iid'])
            try:
                port_struct['vlan'] = str(vlan) + ' - ' + loaded_vlans[str(vlan)]
            except:
                continue

            admin_status = list(filter(lambda d: d['iid'] in [port['iid']], admin_status_list))
            oper_status = list(filter(lambda d: d['iid'] in [port['iid']], oper_status_list))
            if len(admin_status) > 0 and len(oper_status) > 0:
                if admin_status[0]['val'] == '1' and oper_status[0]['val'] == '1':
                    port_struct['status'] = 0

                elif admin_status[0]['val'] == '1' and oper_status[0]['val'] == '2':
                    port_struct['status'] = 1

                elif admin_status[0]['val'] == '2' and oper_status[0]['val'] == '2':
                    port_struct['status'] = 2
            else:
                port_struct['status'] = 3

            read_only = 0
            port_matched = False
            for group_of_ports in ports:
                instance = RegExp(group_of_ports.regex)
                tuple = instance.getTuple()
                for port_def in tuple:
                    if (str(port_def)) == str(port['iid']):
                        if group_of_ports.view_only and (not port_matched):
                            read_only = 1
                            port_matched = True
                        else:
                            read_only = 0
                            port_matched = True
            port_struct['read_only'] = str(read_only)
            port_dict[int(port['iid'])] = port_struct

        self.switch.porty = sorted(port_dict.items(), key=itemgetter(0))

        return self.switch

    def render_vlans(self, user):
        """
        :param user:
        :return:
        """
        groups = from_uco_load_groups(user.id_person)
        vlans = Vlans.objects.filter(
            switch=self.switch, groups__in=groups).distinct()
        list_of_allowed_vlans = []
        loaded_vlans = self.get_vlan_table()

        if not loaded_vlans:
            raise SessionError("Switch ID" + str(self.switch.id_switch) +
                               " does not have VLANs")

        for group_of_vlans in vlans:
            instance = RegExp(group_of_vlans.regex)
            tuple = instance.getTuple()
            for vlan in tuple:
                if (str(vlan)) in loaded_vlans:
                    vlan_name = loaded_vlans.get(str(vlan))
                    if group_of_vlans.view_only:
                        list_of_allowed_vlans.append((str(vlan), vlan_name, 1))
                    else:
                        if (str(vlan), vlan_name, 1) in list_of_allowed_vlans:
                            list_of_allowed_vlans.remove((str(vlan), vlan_name, 1))
                            list_of_allowed_vlans.append((str(vlan), vlan_name, 0))
                        else:
                            list_of_allowed_vlans.append((str(vlan), vlan_name, 0))

        # Sort vlans and remove duplicates
        self.switch.vlany = sorted(list(set(list_of_allowed_vlans)),
                                   key=lambda x: int(x[0]))
        return self.switch

    def get_vlan_table(self):
        oid = self.oiddict['dot1qVlanStaticName']
        try:
            table = self.walk(oid)
        except:
            return None
        ids = []
        names = []
        for vlan in table:
            ids.append(vlan.iid)
            names.append(vlan.val)
        return dict(zip(ids, names))

    def create_vlan_map(self, hex_string):
        value = str(bin(int(hex_string, 16)))[2:]

        while len(value) != pow(2, 6):
            value = '0' + value

        return value

    def get_changeable_port_information(self, port, user):

        alias = self.get_port_alias(port)
        status = self.get_port_status(port)
        vlan = self.get_port_vlan(port)

        port = [alias, status, vlan[0], vlan[1]]

        return port

    def return_port_array(self, port):
        """
        returns array in format:
        port = [descr, alias, own_mac, connected_macs, status,
        port[1],str(port[0]),vlan, self.get_vmVlan(vlan)]
        """
        macs = self.get_port_macs(str(port[0]))
        alias = self.get_port_alias(str(port[0]))
        descr = self.get_port_descr(str(port[0]))
        status = self.get_port_status(str(port[0]))
        oper_status = self.get_oper_status(str(port[0]))

        status = int(status)
        oper_status = int(oper_status)

        if status == 1 and oper_status == 1:
            status = 0

        elif status == 1 and oper_status == 2:
            status = 1

        elif status == 2 and oper_status == 2:
            status = 2
        else:
            status = 3

        own_mac = macs[0]
        connected_macs = macs[1]

        vlan = self.get_port_vlan(str(port[0]))

        self.result_queue.put(
            [descr, alias, own_mac, connected_macs, status, port[1],
             str(port[0]), vlan, self.get_name_of_vlan(vlan)])

    def get_port_vlan(self, port):
        """
        param: port: port number
        return: number and name of VLAN
        """

        port = int(port) - 1
        list_of_untagged_ports = self.walk(self.oiddict['dot1qVlanStaticUntaggedPorts'])
        vlans = self.get_id_of_vlans()

        list_of_untagged_ports = list(list_of_untagged_ports)
        list_of_untagged_ports = map(
            lambda x: x.val.encode('hex'), list_of_untagged_ports)

        test_string = map(
            lambda x: self.create_vlan_map(x), list_of_untagged_ports)

        vlan_structure = {}

        if len(test_string) == len(vlans):
            for i in range(len(test_string)):
                vlan_structure[vlans[i].val] = test_string[i]
        else:
            raise IOError('Loading of VLANs failed')

        for i in vlan_structure.iterkeys():
            value = vlan_structure[i]
            if value[port] == '1':
                return i

        return 0

    def render_switch_ports(self, set_of_ports, user):
        ports_of_switch = []

        array_of_processes = []

        f = open("/tmp/pm_debug.txt", "a")
        f.write("-----\n")
        # For every port, get data about MAC address, alias,
        # description, etc. from SwitchCommunication
        for port in set_of_ports:
            p = multiprocessing.Process(
                target=self.return_port_array, args=(port,))
            array_of_processes.append(p)
            # debug
            f.write(str(self.return_port_array))
            f.write("\n")
            f.flush()
        f.close()

        for job in array_of_processes:
            job.start()

        for job in array_of_processes:
            job.join()

        for element in array_of_processes:
            ports_of_switch.append(self.result_queue.get())

        ports_of_switch = sorted(ports_of_switch, key=(lambda x: int(x[0])))

        groups = from_uco_load_groups(user.id_person)
        vlans = Vlans.objects.filter(
            switch=self.switch, groups__in=groups).distinct()

        list_of_allowed_vlans = []

        # ulozime do pameti nacachovane vlany, ktere na zarizeni skutecne jsou nastaveny,
        # a porovname je s vlanami z regexu, abychom nezobrazovali VLANy, ktere skutecne
        # nejsou nastaveny
        loaded_vlans = []
        for vlan in self.vlan_structure.iterkeys():
            loaded_vlans.append(vlan)

        for group_of_vlans in vlans:
            instance = RegExp(group_of_vlans.regex)
            tuple = instance.getTuple()
            for vlan in tuple:
                if (str(vlan)) in loaded_vlans:
                    if group_of_vlans.view_only:
                        list_of_allowed_vlans.append((str(vlan), 1))
                    else:
                        if (str(port), 1) in list_of_allowed_vlans:
                            list_of_allowed_vlans.remove((str(vlan), 1))
                            list_of_allowed_vlans.append((str(vlan), 0))
                        else:
                            list_of_allowed_vlans.append((str(vlan), 0))

        list_of_allowed_vlans = map(
            lambda x_y3: (x_y3[0],
                          self.get_name_of_vlan(x_y3[0]),
                          x_y3[1]),
            list_of_allowed_vlans)
        self.switch.porty = ports_of_switch
        # sort VLANs by its number and remove duplicates
        self.switch.vlany = sorted(
            list(set(list_of_allowed_vlans)), key=lambda x: int(x[0]))

        return self.switch

    def get_port_bitmap_location(self, portnumber):
        # prijima a pocet bitovych retezcu
        # funkce zjisti, v kolikatem retezci se nachazi
        for i in range(1, 16):
            if portnumber < 8 * i:
                return i

        return 0

    def format_bitstring(self, bitstring):
        # funkce ktera vraci z puvodni neplne osmibitove mapy osmibitovou)
        bitstring = str(bitstring)

        for i in range(0, 8 - len(bitstring)):
            bitstring = '0' + bitstring

        return bitstring

    def get_id_of_vlans(self):
        # vrati vsechny identifikacni cisla VLAN, vyskytujicich se na zarizeni,
        # -----------------------------------------------------------------------------------------------------DOPLNIT
        # proc je vraci vetsi o 62, POZNAMKA: zeptat se nekoho
        oid = self.oiddict['hpSwitchIgmpVlanIndex']
        vlanidlist = self.walk(oid)

        return vlanidlist

    def refresh_vlan_list(self):
        vlans = self.get_id_of_vlans()

        vlan_list = []

        for vlan in vlans:
            vlan_list.append([vlan, self.get_name_of_vlan(vlan)])

        return vlan_list

    def is_it_untagged(self, port, vlan):
        # urci jestli port nalezi do dane vlany
        #
        self.s.oid = netsnmp.Varbind(
            self.s.oiddict["dot1qVlanStaticUntaggedPorts"])
        list_of_untagged_ports = self.s.walk()

        position_in_bitsequence = ((int(port) - 1) % 8)

        list_of_untagged_ports = map(ord, list_of_untagged_ports)
        port_position = self.get_port_bitmap_location(port)
        position = (list_of_untagged_ports[port_position])
        position = (bin(position))[2:]
        position = self.format_bitstring(position)
        information = position[position_in_bitsequence]

        if information == "1":
            return True
        else:
            return False

    def get_port_membership(self, port, vlan):
        # vrati informaci jestli je port pro danou VLAN
        # na vstupu bere port, pro ktery ma urcit prislusnost
        # a take ID_vlany pro kterou se ma urcit
        #
        #  0  - No
        #  1  - Forbid
        #  2  - Tagged
        #  3  - Untagged
        #
        #  'dot1qVlanStaticEgressPorts': '.1.3.6.1.2.1.17.7.1.4.3.1.2',
        #  'dot1qVlanStaticUntaggedPorts': '.1.3.6.1.2.1.17.7.1.4.3.1.4',
        #  'dot1qVlanStaticPort' :'.1.3.6.1.4.1.207.1.4.167.116.7.1.4.5.1.1'

        oid = netsnmp.Varbind(self.oiddict["dot1qVlanStaticEgressPorts"], vlan)

        list_of_egress_ports = self.get(oid)

        for port in list_of_egress_ports:
            print port.val

        list_of_egress_ports = map(ord, list_of_egress_ports[0].val)

        port_position = self.get_port_bitmap_location(port)

        position = (list_of_egress_ports[port_position])
        position = (bin(position))[2:]
        position = self.format_bitstring(position)
        position_in_bitsequence = (int(port) - 1) % 8

        information = position[position_in_bitsequence]
        # pomoci SNMP vytahneme prvotni nezbytnou informaci
        # a to je, zda port ma nastaveny Egress na 1 / 0,
        # na zaklade toho muzeme rozhodnout o tom, zda je zapnuty/vypnuty
        # map(ord ) rozlozi prijaty string na byty v poli,se kterymi budeme pracovat
        # pomoci metody get_port_bitmap_location()
        # zjistime ve kterem useku pole je nas port
        # pote prevedeme tento usek na binarni retezec, z nejz odebereme prvni dva znaky
        # ktere znaci, ze se jedna o binarni retezec [0b]
        # nyni musime naformatovat string na korektni velikost
        # pokud jsou na zarizeni nastaveny na prvnich nekolik mistech nuly,
        # tak je tento string doplnen do pozadovane velikosti jednoho bytu nulami
        # pote se spocita pozice portu v retezci. tedy se nejprve odecte jednicka
        # (kvuli indexaci od nuly) a pote se vymoduluje 8, cimz zjistime konkretni pozici
        # nyni z naseho predtim zjisteneho positionu tedy zjistime nastaveni na portu
        # tyto operace se jeste dvakrat zopakuji pro untagged/forbid
        # pokud je onen konkretni egress bit nastaven na jednicku, tak vime, ze port
        # je bud tagged/untagged, pokud neni egress nastaven, tak vime, ze je bud zakazany
        # nebo neprirazeny, to tedy zjistime zavolanim getu na Untagged/Forbid
        # v one konkretni vetvi

        if information == "1":
            oid = netsnmp.Varbind(self.oiddict["dot1qVlanStaticUntaggedPorts"], vlan)
            list_of_untagged_ports = self.get(oid)

            list_of_untagged_ports = map(ord, list_of_untagged_ports[0].val)
            port_position = self.get_port_bitmap_location(port)

            position = (list_of_untagged_ports[port_position])
            position = (bin(position))[2:]
            position = self.format_bitstring(position)

            information = position[position_in_bitsequence]
            if information == "1":
                return 3
            else:
                return 2

        else:
            oid = netsnmp.Varbind(self.oiddict['dot1qVlanStaticForbidPorts'], vlan)

            list_of_egress_ports = self.get(oid)
            list_of_egress_ports = map(ord, list_of_egress_ports[0].val)
            port_position = self.get_port_bitmap_location(port)
            position = (list_of_egress_ports[port_position])
            position = (bin(position))[2:]
            position = self.format_bitstring(position)

            position_in_bitsequence = (int(port) - 1) % 8
            information = position[position_in_bitsequence]

            if information == "1":
                return 1
            else:
                return 0

    def create_binary_changer(self, value):
        # vytvori 8 bytovy binarni retezec s jehoz pomoci se pote
        # na zaklade binarniho soucinu pronasobi hodnota, tak
        # aby zmenila pouze pozadovanou cast a mohla se nastavit
        #
        binary_string = "0"
        if value != 0:
            for i in range(0, value - 1):
                binary_string = "1" + binary_string
            for i in range(value - 1, 64):
                binary_string += "1"
        else:
            for i in range(0, 63):
                binary_string += "1"
        return binary_string

    def binary_and_for_strings(self, first, second):
        # metoda pro binarni soucin na dvou stringach

        final = ""

        if len(first) == len(second):
            for i in range(0, len(first)):
                if first[i] == second[i]:
                    final = final + "1"
                else:
                    final = final + "0"
            return final
        else:
            return "Strings are not equally long"

    def from_string_make_hexval(self, string):
        # metoda ktera pro binarni retezec vrati
        # hexval nutny pro zaslani snmp pozadavku

        hex_dictionary = {'0000': '0',
                          '0001': '1',
                          '0010': '2',
                          '0011': '3',
                          '0100': '4',
                          '0101': '5',
                          '0110': '6',
                          '0111': '7',
                          '1000': '8',
                          '1001': '9',
                          '1010': 'A',
                          '1011': 'B',
                          '1100': 'C',
                          '1101': 'D',
                          '1110': 'E',
                          '1111': 'F'}

        hexval = ""
        for i in range(0, len(string), 4):

            if string[i:i + 4] in hex_dictionary:
                hexval = hexval + hex_dictionary[string[i:i + 4]]

        return hexval

    def set_membership(self, port, vlan, dict, state):
        oid = netsnmp.Varbind(self.oiddict[dict] + '.' + str(vlan))
        untagged_ports = self.create_vlan_map(self.get(oid)[0].val.encode('hex'))

        untagged_ports = list(untagged_ports)
        untagged_ports[int(port) - 1] = str(state)
        untagged_ports = "".join(untagged_ports)
        # nyni pomoci metody from_string_make_hexval vytvorime hexadecimalni retezec
        # ktery predame switchi na nastaveni
        hexval = self.from_string_make_hexval(untagged_ports)

        string = "snmpset -v " + " 2c -c" + \
                 self.switch.community + " " + \
                 self.switch.dest_host + " " + \
                 self.oiddict[dict][1:] + "." \
                 + vlan + " x " + hexval
        print string
        os.system(string)
        return 1

    def set_vlan_name(self, vlan_id, new_alias):
        # metoda, ktera umoznuje menit nastaveny alias na portu
        # obsahuje i funkci pro overeni, zda se opravdu zmena provedla
        # pokud ne, vrati cislo 2, coz je signal nastaleho problemu

        self.s.oid = netsnmp.Varbind(self.s.oiddict["dot1qVlanStaticName"],
                                     vlan_id, new_alias, 'OCTETSTR')
        self.s.set()

        self.s.oid = netsnmp.Varbind(
            self.s.oiddict["dot1qVlanStaticName"], vlan_id)
        alias = self.s.get()[0]

        if alias == new_alias:
            return 1
        else:
            return 0

    def get_name_of_vlan(self, vlan_id):
        self.s.oid = netsnmp.Varbind(
            self.s.oiddict['dot1qVlanStaticName'], vlan_id)
        return self.s.get()[0]

    def set_vlan_membership(self, port, port_name, vlan):
        """
        Sets VLAN on specific port.
        @rtype: bool
        @param port: ifIndex
        @param port_name: port name
        @param vlan: vlan index
        @return: true if change is successfull
        """
        oid = netsnmp.Varbind(self.oiddict["dot1qPvid"], port, vlan, "GAUGE")
        self.set(oid)
        sleep(2)
        control = self.get_port_vlan(port)

        if control == vlan:
            return True
        else:
            return False

    def transfer_bulk_port_vals(self, port_id, port_name, port_alias, vlan_id, port_sec, port_status):
        descr = self.change_port_alias(port_id, port_name, port_alias)
        vlan = self.set_vlan_membership(port_id, port_name, vlan_id)
        status = True
        if port_status == '2':
            status = self.turn_on_off(port_id)
        if not descr or not vlan or not status:
            return False
        return True

    def transfer_src_default_config(self, port_id, port_name):
        descr = self.change_port_alias(port_id, port_name, "")
        status = True
        if self.get_port_status(port_id) == '1':
            status = self.turn_on_off(port_id)
        if not descr or not status:
            return False
        return True
