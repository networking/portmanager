import json
import os
import sys
from operator import itemgetter
from subprocess import call
from time import sleep

import configparser
import netsnmp
from django.core.cache import cache
from fastsnmpy import SnmpSession

from general import GeneralSwitch
from general import SessionError
from ..models import *
from ..users import *

sys.path.append('../')


class CiscoSwitch(GeneralSwitch):
    """Subclass of SwitchCommunication with extended functionality for devices
    made by Cisco.
    """

    def get_changeable_port_information(self, port, user):
        """Function downloads information that user could change.

        :param port
        :param user
        :return: returns array in format [alias, status, vlan, vlan_name]
        """
        alias = self.get_port_alias(port)
        status = self.get_port_status(port)
        vlan = self.get_port_vlan(port)
        oper_status = self.get_oper_status(port)
        port_sec = self.get_port_sec(port)

        if not alias:
            alias = '(None)'

        status = int(status)
        oper_status = int(oper_status)

        if status == 1 and oper_status == 1:
            status = 0

        elif status == 1 and oper_status == 2:
            status = 1

        elif status == 2 and oper_status == 2:
            status = 2

        else:
            status = 3

        port = [alias, status, vlan, self.get_vmVlan(vlan), port_sec]
        return port

    def format_mac_address(self, unformatted_mac):
        array = [unformatted_mac[i: i + 2] for i in range
        (0, len(unformatted_mac), 2)]
        return ":".join(array)

    def get_connected_macs(self, port, vlan_name):
        """
        :param port:
        :return:
        """
        try:
            int(port)
        except ValueError:
            raise TypeError("Port id must be number")

        try:
            oid = netsnmp.Varbind(self.oiddict["vmVlan"], port)
            vlan = self.get(oid)[0].val

        except Exception as e:
            raise e
        macs = []
        old_community = self.switch.community

        self.switch.community = "%s@%s" % (self.switch.community, vlan)
        self.session = netsnmp.Session(DestHost=self.switch.dest_host, Version=self.switch.snmp_version,
                                       Community=self.switch.community)
        self.session.UseLongNames = 1
        self.session.UseNumeric = 1
        try:
            oid = netsnmp.Varbind(self.oiddict['dot1dTpFdbAddress'])
            temp_macs = self.walk(oid)
        except Exception as e:
            raise e

        for mac in temp_macs:
            oid = netsnmp.Varbind(self.oiddict['dot1dTpFdbPort'])
            port_ids = self.walk(oid)
            for port_id in port_ids:
                oid = netsnmp.Varbind(self.oiddict['dot1dBasePortIfIndex'], port_id.val)
                if_index = self.get(oid)[0].val
                if if_index == port and mac.iid == port_id.iid:
                    macs.append(self.format_mac_address(mac.val.encode("hex").upper()))

        self.switch.community = old_community
        self.session = netsnmp.Session(DestHost=self.switch.dest_host, Version=self.switch.snmp_version,
                                       Community=self.switch.community)
        self.session.UseLongNames = 1
        self.session.UseNumeric = 1
        return macs

    def get_port_sec(self, port):
        """

        :param port:
        :return:
        """
        oid_enabled = self.oiddict['cpsIfPortSecurityEnable']
        oid_mac_count = self.oiddict['cpsIfMaxSecureMacAddr']
        oid = netsnmp.Varbind(oid_enabled, port)
        sec_enabled = self.get(oid)[0].val
        if sec_enabled[0] == '2':
            return 0
        oid = netsnmp.Varbind(oid_mac_count, port)
        sec_count = self.get(oid)[0].val
        return sec_count

    def set_port_sec(self, port, mac_count):
        """

        :param port:
        :param mac_count:
        :return:
        """
        if mac_count == '0':
            oid = netsnmp.Varbind(self.oiddict['cpsIfPortSecurityEnable'], port, '2', "INTEGER")
            self.set(oid)
        else:
            oid_enable = netsnmp.Varbind(self.oiddict['cpsIfPortSecurityEnable'], port, '1', "INTEGER")
            oid_count = netsnmp.Varbind(self.oiddict['cpsIfMaxSecureMacAddr'], port, mac_count, "INTEGER")
            self.set(oid_enable)
            self.set(oid_count)
        sleep(1)
        return self.get_port_sec(port) == mac_count

    def get_running_config(self):
        config = configparser.ConfigParser()
        config.read('/srv/config/app_config.ini')
        tftp_server = config['app']['tftp_server']
        oid = netsnmp.Varbind(self.oiddict["download_config"], tftp_server, self.switch.dest_host, "OCTETSTR")
        self.set(oid)

    def upload_running_config(self):
        config = configparser.ConfigParser()
        config.read('/srv/config/app_config.ini')
        tftp_server = config['app']['tftp_server']
        oid = netsnmp.Varbind(self.oiddict["upload_config"], tftp_server, self.switch.dest_host, "OCTETSTR")
        self.set(oid)

    def copy_run_start(self):
        # oid = netsnmp.Varbind(self.oiddict["copy_run_start"], 1, "INTEGER")
        # self.set(oid)
        os.system("snmpset -v 2c -c %s %s .1.3.6.1.4.1.9.2.1.54.0 i 1" % (self.switch.community, self.switch.dest_host))

    def get_vmVlan(self, vlan):
        """Returns name of specific VLAN"""
        oid = netsnmp.Varbind(self.oiddict['vtpVlanName'], vlan)
        name = self.get(oid)
        return name[0].val

    def get_vlan_table(self):
        """Returns name of specific VLAN"""
        oid = '.1.3.6.1.4.1.9.9.46.1.3.1.1.4'
        try:
            table = self.walk(oid)
        except:
            return None
        ids = []
        names = []
        for vlan in table:
            ids.append(vlan.iid)
            names.append(vlan.val)
        return dict(zip(ids, names))

    def get_port_vlan(self, port):
        """Returns VLAN set on specific port"""
        oid = netsnmp.Varbind(self.oiddict["vmVlan"], port)
        return self.get(oid)[0].val

    def get_vtp_rev_number(self):
        """Returns current VTP revision number"""
        oid = self.oiddict['vtpRevNum']
        result = self.get(oid)[0].val
        return result

    def set_vlan_membership(self, port, port_name, new_status):
        """Sets VLAN on specific port.
        :param port
        :param new_status: new VLAN that will be set on port
        """
        oid = netsnmp.Varbind(self.oiddict["vmVlan"], port, new_status, "INTEGER")
        self.set(oid)
        sleep(1)
        oid = netsnmp.Varbind(self.oiddict["vmVlan"], port)
        control = self.get(oid)[0].val

        if control == new_status:
            return True
        else:
            return False

    def get_port_pwr_consumption(self, ports):
        oid = netsnmp.Varbind(self.oiddict['poe_pwr'])
        result = self.walk(oid)
        oid = netsnmp.Varbind(self.oiddict['ifDescr'])
        indexes = self.walk(oid)
        ports_descr = {}
        pwr_list = []
        pwr_pos = 0
        results = []
        for index in indexes:
            ports_descr[index.iid] = index.val
        for pwr in result:
            pwr_list.append(pwr.val)

        for port_def in ports:
            start_pos = str(port_def.split('-')[0])
            end_pos = str(port_def.split('-')[1])
            for pos in range(int(start_pos), int(end_pos) + 1):
                if str(pos) in ports_descr and pwr_pos < len(pwr_list):
                    results.append(ports_descr[str(pos)] + ': ' + pwr_list[pwr_pos])
                    pwr_pos += 1
                if pwr_pos > len(pwr_list) - 1:
                    break

        return results

    def render_ports(self, user):

        groups = from_uco_load_groups(user.id_person)
        ports = Ports.objects.filter(
            switch=self.switch, groups__in=groups).distinct()
        oids = [self.oiddict['ifDescr'], self.oiddict['ifAlias'], self.oiddict['ifOperStatus'],
                self.oiddict['ifAdminStatus'], self.oiddict['cpsIfPortSecurityEnable'],
                self.oiddict['vmVlan'], self.oiddict['cpsIfMaxSecureMacAddr'], self.oiddict['ifTrunk'],
                ]

        fast_session = SnmpSession(targets=[self.switch.dest_host], oidlist=oids, community=self.switch.community)
        results = fast_session.snmpbulkwalk(workers=15)
        port_dict = {}
        switch_id = str(self.switch.id_switch)
        location_id = str(self.switch.location.id_location)

        results = json.loads(results)
        descr_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.2'], results))
        trunk_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.4.1.9.9.46.1.6.1.1.14'], results))
        alias_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.31.1.1.1.18'], results))
        oper_status_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.8'], results))
        admin_status_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.2.1.2.2.1.7'], results))
        port_sec_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.4.1.9.9.315.1.2.1.1.1'], results))
        port_sec_count_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.4.1.9.9.315.1.2.1.1.3'], results))
        vlan_list = list(filter(lambda d: d['tag'] in ['.1.3.6.1.4.1.9.9.68.1.2.2.1.2'], results))

        for port in descr_list:
            port_struct = {}
            port_struct['iid'] = port['iid']
            port_struct['descr'] = port['val']
            if port_struct['descr'] is None:
                continue
            if 'Ethernet' not in port_struct['descr']:
                continue
            # Skip MGMT ports
            if port_struct['descr'] == 'FastEthernet0' or port_struct['descr'] == 'FastEthernet1':
                continue
            # Skip trunk ports
            is_trunk = list(filter(lambda d: d['iid'] in [port['iid']], trunk_list))
            if len(is_trunk) > 0 and is_trunk[0]['val'] == "1":
                continue

            alias = list(filter(lambda d: d['iid'] in [port['iid']], alias_list))
            if len(alias) > 0:
                port_struct['alias'] = alias[0]['val']

            vlan = list(filter(lambda d: d['iid'] in [port['iid']], vlan_list))
            if len(vlan) > 0:
                vlan_name = self.get_vmVlan(vlan[0]['val'])
                if vlan_name == "":
                    vlan_name = "None"
                port_struct['vlan'] = vlan[0]['val'] + " - " + vlan_name
            else:
                continue

            admin_status = list(filter(lambda d: d['iid'] in [port['iid']], admin_status_list))
            oper_status = list(filter(lambda d: d['iid'] in [port['iid']], oper_status_list))
            if len(admin_status) > 0 and len(oper_status) > 0:
                if admin_status[0]['val'] == '1' and oper_status[0]['val'] == '1':
                    port_struct['status'] = 0

                elif admin_status[0]['val'] == '1' and oper_status[0]['val'] == '2':
                    port_struct['status'] = 1

                elif admin_status[0]['val'] == '2' and oper_status[0]['val'] == '2':
                    port_struct['status'] = 2
            else:
                port_struct['status'] = 3

            port_sec = list(filter(lambda d: d['iid'] in [port['iid']], port_sec_list))
            port_sec_count = list(filter(lambda d: d['iid'] in [port['iid']], port_sec_count_list))
            if len(port_sec) > 0:
                if port_sec[0]['val'] != '1':
                    port_struct['portsec'] = 'N/A'
                else:
                    port_struct['portsec'] = port_sec_count[0]['val']
            read_only = 0
            port_matched = False
            for group_of_ports in ports:
                instance = RegExp(group_of_ports.regex)
                tuple = instance.getTuple()
                for port_def in tuple:
                    if (str(port_def)) == str(port['iid']):
                        if group_of_ports.view_only and (not port_matched):
                            read_only = 1
                            port_matched = True
                        else:
                            read_only = 0
                            port_matched = True
            port_struct['read_only'] = str(read_only)
            cache.set(
                'port_' + location_id + '_' + switch_id + '_' + port_struct['descr'],
                port_struct['alias'],
                timeout=int(environ.get('CACHE_TTL'))
            )
            if port_matched:
                port_dict[int(port['iid'])] = port_struct

        self.switch.porty = sorted(port_dict.items(), key=itemgetter(0))
        return self.switch

    def render_vlans(self, user):
        """
        :param user:
        :return:
        """
        groups = from_uco_load_groups(user.id_person)
        vlans = Vlans.objects.filter(
            switch=self.switch, groups__in=groups).distinct()
        list_of_allowed_vlans = []
        dev_revNum = self.get_vtp_rev_number()
        vlan_cache = 'vlans_' + str(self.switch.id_switch)
        if vlan_cache in cache:
            if dev_revNum == cache.get:
                loaded_vlans = cache.get(vlan_cache)
            else:
                loaded_vlans = self.get_vlan_table()
                cache.set(str(self.switch.id_switch) + 'revNum', dev_revNum, timeout=int(environ.get('CACHE_TTL')))
                cache.set(vlan_cache, loaded_vlans, timeout=int(environ.get('CACHE_TTL')))
        else:
            loaded_vlans = self.get_vlan_table()
            cache.set(str(self.switch.id_switch) + 'revNum', dev_revNum, timeout=int(environ.get('CACHE_TTL')))
            cache.set(vlan_cache, loaded_vlans, timeout=int(environ.get('CACHE_TTL')))
        # could switch be with no vlans?
        if not loaded_vlans:
            raise SessionError("Switch ID" + str(self.switch.id_switch) +
                               " does not have VLANs")

        for group_of_vlans in vlans:
            instance = RegExp(group_of_vlans.regex)
            tuple = instance.getTuple()
            for vlan in tuple:
                if (str(vlan)) in loaded_vlans:
                    vlan_name = loaded_vlans.get(str(vlan))
                    if group_of_vlans.view_only:
                        list_of_allowed_vlans.append((str(vlan), vlan_name, 1))
                    else:
                        if (str(vlan), vlan_name, 1) in list_of_allowed_vlans:
                            list_of_allowed_vlans.remove((str(vlan), vlan_name, 1))
                            list_of_allowed_vlans.append((str(vlan), vlan_name, 0))
                        else:
                            list_of_allowed_vlans.append((str(vlan), vlan_name, 0))

        # Sort vlans and remove duplicates
        self.switch.vlany = sorted(list(set(list_of_allowed_vlans)),
                                   key=lambda x: int(x[0]))

        return self.switch

    def update_vlan_table(self):
        vlan_cache = 'vlans_' + str(self.switch.id_switch)
        dev_revNum = self.get_vtp_rev_number()
        try:
            int(dev_revNum)
        except:
            return
        if vlan_cache in cache:
            cache_revNum = cache.get(str(self.switch.id_switch) + 'revNum')
            # check VTP revNum cached vs actual
            if dev_revNum != cache_revNum:
                cache.set(str(self.switch.id_switch) + 'revNum', dev_revNum, timeout=int(environ.get('CACHE_TTL')))
                loaded_vlans = self.get_vlan_table()
                cache.set(vlan_cache, loaded_vlans, timeout=int(environ.get('CACHE_TTL')))
        else:
            cache.set(str(self.switch.id_switch) + 'revNum', dev_revNum, timeout=int(environ.get('CACHE_TTL')))
            loaded_vlans = self.get_vlan_table()
            cache.set(vlan_cache, loaded_vlans, timeout=int(environ.get('CACHE_TTL')))

    def apply_port_default_config(self, port_id, port_name):
        new_config = (str(self.switch.device.default_port_config) + '\n').splitlines(True)
        run_cfg = open('/srv/tftp/' + self.switch.dest_host, "w")
        new_config_content = "".join(new_config)
        run_cfg.write('interface ' + port_name + '\n' + new_config_content)
        run_cfg.close()
        config = configparser.ConfigParser()
        config.read('/srv/config/app_config.ini')
        tftp_server = config['app']['tftp_server']
        rc = call(
            ['/srv/scripts/parcial.sh', tftp_server, str(self.switch.dest_host), str(self.switch.dest_host),
             str(self.switch.community)])
        os.remove('/srv/tftp/' + self.switch.dest_host)
        return rc

    def transfer_src_default_config(self, port_id, port_name):
        run_cfg = open('/srv/tftp/' + self.switch.dest_host, "w")
        run_cfg.write('interface ' + port_name + '\n no description \n shutdown')
        run_cfg.close()
        config = configparser.ConfigParser()
        config.read('/srv/config/app_config.ini')
        tftp_server = config['app']['tftp_server']
        rc = call(
            ['/srv/scripts/parcial.sh', tftp_server, str(self.switch.dest_host), str(self.switch.dest_host),
             str(self.switch.community)])
        os.remove('/srv/tftp/' + self.switch.dest_host)
        return rc

    def transfer_bulk_port_vals(self, port_id, port_name, port_alias, vlan_id, port_sec, status):
        run_cfg = open('/srv/tftp/' + self.switch.dest_host, "w")
        description = 'description ' + port_alias + ' \n'
        vlan = 'switchport mode access \n switchport access vlan ' + vlan_id + '\n'
        if port_sec != '':
            port_security = 'switchport port-security \n switchport port-security maximum ' + port_sec + '\n'
        else:
            port_security = ''
        if status == '1':
            port_status = 'no shutdown \n'
        else:
            port_status = 'shutdown \n'
        run_cfg.write('interface ' + port_name + '\n' + description + vlan + port_security + port_status)
        run_cfg.close()
        config = configparser.ConfigParser()
        config.read('/srv/config/app_config.ini')
        tftp_server = config['app']['tftp_server']
        rc = call(
            ['/srv/scripts/parcial.sh', tftp_server, str(self.switch.dest_host), str(self.switch.dest_host),
             str(self.switch.community)])
        os.remove('/srv/tftp/' + self.switch.dest_host)
        return rc
