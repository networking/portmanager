import json
import logging
import os
from subprocess import call

import configparser
from django.core import serializers
from django.core.cache import cache
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.utils.translation import ugettext

import services
import users
from time import sleep
from logger import Logger
from models import *
from switch_modules.general import SessionError

logger = logging.getLogger(__name__)


def get_switch_permission(request):
    """

    :param request:
    :return:
    """
    switch = request.POST['switch_id']

    try:
        user = request.POST['user_id'].id_person
        account = users.login(user)
    except Exception:
        account = users.login(request)

    user = account[1]
    groups = users.from_uco_load_groups(user.id_person)
    switch = Switch.objects.get(id_switch=switch)

    user_ports = Ports.objects.all()
    user_ports = user_ports.filter(switch=switch, groups__in=groups).distinct()
    switch_instance = services.get_instance(switch)

    existing_ports = switch_instance.get_port_descriptions()
    output_structure = []

    def update_permission(default, new):
        # 0 - no access
        # 1 - view_only
        # 2 - read_write
        permission_array = [0, 1, 2]

        if default in permission_array and new in permission_array:
            if new > default:
                return new
            else:
                return default
        else:
            raise TypeError("Default and new must be in range(0,2)")

    # existing_ports = map(lambda x: [x[0],x[1],0], existing_ports)
    temp_hash = {}

    for key in existing_ports.iterkeys():
        value = existing_ports[key]
        temp_hash[key] = [value, 0]
    existing_ports = temp_hash

    for group_of_ports in user_ports:
        regex_instance = users.RegExp(group_of_ports.regex)
        array = regex_instance.getStringArray()
        # have to differentiate between ports containing single or multiple ports

        for ports in array:
            if ports == '':
                continue
            if '-' in ports:
                start = int(ports.split('-')[0])
                end = int(ports.split('-')[1])

                for i in range(start, end + 1):
                    if str(i) not in existing_ports:
                        continue
                    value = existing_ports[str(i)]
                    if group_of_ports.view_only:
                        existing_ports[str(i)] = [
                            value[0], update_permission(value[1], 1)]
                    else:
                        existing_ports[str(i)] = [
                            value[0], update_permission(value[1], 2)]
            else:
                if str(ports) not in existing_ports:
                    continue
                value = existing_ports[str(ports)]
                if group_of_ports.view_only:
                    existing_ports[str(ports)] = [
                        value[0], update_permission(value[1], 1)]
                else:
                    existing_ports[str(ports)] = [
                        value[0], update_permission(value[1], 2)]

    for key in existing_ports.iterkeys():
        value = existing_ports[str(key)]
        output_structure.append([key, value[0], value[1]])
    output_structure.sort(key=lambda x: int(x[0]))

    return HttpResponse(json.dumps(output_structure))


def show_switch_definitions(request):
    """

    :param request:
    :return:
    """
    switch = request.POST['switch_id']

    try:
        user = request.POST['user_id'].id_person
        account = users.login(user)
    except Exception:
        account = users.login(request)

    user = account[1]
    groups = users.from_uco_load_groups(user.id_person)
    switch = Switch.objects.get(id_switch=switch)

    user_ports = Ports.objects.all()
    user_ports = user_ports.filter(switch=switch, groups__in=groups).distinct()
    user_vlans = Vlans.objects.all()
    user_vlans = user_vlans.filter(switch=switch, groups__in=groups).distinct()

    output_structure = '<h3>Port:</h3> <br>'
    for port in user_ports:
        output_structure = output_structure + str(port) + '<br>'
    output_structure = output_structure + '<h3>VLAN:</h3> <br>'
    for vlan in user_vlans:
        output_structure = output_structure + str(vlan) + '<br>'

    return HttpResponse(output_structure)


def show_user_log(request):
    """
    Returns logs of users
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    logs = Event.objects.all()
    logs = logs.filter(who=user).order_by("-id_event")

    return render(request, "user_log.html",
                  {"user": user, "logs": logs, 'adm_groups': adm_groups})


def index(request):
    """
    Primary method to show all switches and give user access to change settings
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]

    if request.method == "GET":
        # loads unseen comments and unseen events from Event, then set those events as seen
        events = Event.objects.all().filter(who=user, seen=False)
        events_to_display = events

        for event in events:
            event.seen = True
            event.save()

        groups = users.from_uco_load_groups(uco)
        adm_groups = users.is_admin(uco)

        # order_by makes sure that switches from one locations are listed together and alphabetically
        switches = Switch.objects.all().order_by('location', 'name')
        switches = switches.filter(owners__in=groups).distinct()

        if not switches:
            string = ugettext(
                'You do not have permissions to any device, '
                'please contact the administrator to assign rights to your location.')
            link = "/portmanager/account/index.html"
            return render(request, 'report.html',
                          {'string': string, 'user': user, 'link': link, 'adm_groups': adm_groups})

        locations = Location.objects.all().filter(switch__in=switches).distinct()
        keys = []
        for location in locations:
            if len(cache.keys('port_' + str(location.id_location) + '_*')) > 0:
                keys.extend(cache.keys('port_' + str(location.id_location) + '_*'))
        cached_ports = []
        cached_results = cache.get_many(keys)
        for port_id, port_val in cached_results.items():
            if not port_val:
                port_val = '(None)'
            cached_ports.append(port_id + '_' + port_val)
        # switches = sorted(switches, cmp=location_sort)
        return render(request, 'index.html',
                      {'switches': switches,
                       'user': user,
                       'events': events_to_display,
                       'locations': locations,
                       'adm_groups': adm_groups,
                       'cached_ports': cached_ports,
                       })


def return_switch_information(request):
    """ AJAX instantly changes currently changes data to other users

    :param request: switch ID
    :return: returns JSON with information about ports
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]
    person = Person.objects.get(id_person=int(uco))
    read_only = person.read_only
    portsec_modify = person.port_sec

    config = configparser.ConfigParser()
    config.read('/srv/config/app_config.ini')
    max_port_sec = config['app']['max_allowed_devices']

    switch = Switch.objects.get(id_switch=request.POST['id'])
    groups = users.from_uco_load_groups(uco)

    ports = Ports.objects.all()
    ports = ports.filter(switch=switch, groups__in=groups).distinct()
    # select ports with access of user
    # we have to create list of ports to prevent duplicates
    # because of this read permission must be rewritten by write permission
    # if user has both

    poe_ports = []
    if switch.poe_regex:
        try:
            poe_regex_instance = users.RegExp(switch.poe_regex)
            poe_array = poe_regex_instance.getStringArray()
            if poe_array is not None and poe_array != [""]:
                for ports in poe_array:
                    poe_ports.append(ports)
        except:
            print "no poe ports"
    switch_instance = services.get_instance(switch)
    json_structure = {}
    poe_ports_table = ""
    try:
        switch.template_info = switch_instance.render_ports(user)
        switch.template_info = switch_instance.render_vlans(user)
    except Exception as e:
        json_structure['error'] = str(e)
        logger.error(str(e))
        return HttpResponse(json.dumps(json_structure, separators=(',', ':')), content_type='application/javascript')
    try:
        poe_ports_table = switch_instance.get_port_pwr_consumption(poe_ports)
    except Exception as e:
        logger.error(str(e))
    poe_ports_dict = {}

    if poe_ports_table is not None:
        for port in poe_ports_table:
            port_splitted = port.split(': ')
            poe_ports_dict[port_splitted[0]] = str(float(port_splitted[1])/1000)
        json_structure['poe_ports'] = poe_ports_dict
    json_structure['ports'] = switch.template_info.porty
    json_structure['vlans'] = switch.template_info.vlany
    json_structure['switch_id'] = switch.id_switch
    json_structure['read_only'] = read_only
    json_structure['portsec_modify'] = portsec_modify
    json_structure['portsec_max'] = max_port_sec

    return HttpResponse(json.dumps(json_structure, separators=(',', ':')),
                        content_type='application/javascript')


def get_cached_ports(request):
    """
    Retrieves ports from Redis cache used for global searching
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    query_string = request.META['QUERY_STRING'].split('q=')[1]

    if request.method == "GET":
        groups = users.from_uco_load_groups(uco)

        # order_by makes sure that switches from one locations are listed together and alphabetically
        switches = Switch.objects.all().order_by('location', 'name')
        switches = switches.filter(owners__in=groups).distinct()
        locations = Location.objects.all().filter(switch__in=switches).distinct()
        keys = []
        for location in locations:
            if len(cache.keys('port_' + str(location.id_location) + '_*')) > 0:
                keys.extend(cache.keys('port_' + str(location.id_location) + '_*'))
        cached_ports = []
        cached_results = cache.get_many(keys)
        for port_id, port_val in cached_results.items():
            # process port to get location and switch names
            loc_id = port_id.split('_')[1]
            switch_id = port_id.split('_')[2]
            port_name = port_id.split('_')[3]
            loc_name = Location.objects.get(id_location=loc_id)
            switch_name = Switch.objects.get(id_switch=switch_id)

            # check if port contains querystring
            if query_string in port_val:
                cached_ports.append({
                    'id': str(switch_id),
                    'loc_name': str(loc_name),
                    'switch_name': str(switch_name),
                    'switch_host': str(switch_name.dest_host),
                    'port_name': port_name,
                    'port_alias': port_val
                })
        cached_ports = {
            'incomplete_results': False,
            'items': cached_ports,
            'total_count': 30
        }
        return HttpResponse(json.dumps(cached_ports), content_type='application/javascript')


def get_devices(request):
    """
    Prepares devices list for Select2
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    query_string = request.META['QUERY_STRING'].split('q=')[1]

    if request.method == "GET":
        groups = users.from_uco_load_groups(uco)

        # order_by makes sure that switches from one locations are listed together and alphabetically
        switches = Switch.objects.all().order_by('location', 'name')
        switches = switches.filter(owners__in=groups).distinct()
        results = []
        for switch in switches:
            if query_string in switch.name or query_string in switch.dest_host:
                results.append({
                    'id': str(switch.id_switch),
                    'loc_name': str(switch.location),
                    'switch_name': str(switch.name),
                    'switch_host': str(switch.dest_host),
                })

        cached_devices = {
            'incomplete_results': False,
            'items': results,
            'total_count': len(results),
        }
        return HttpResponse(json.dumps(cached_devices), content_type='application/javascript')


def reload_cache(request):
    account = users.login(request)
    uco = account[0]
    user = account[1]
    groups = users.from_uco_load_groups(uco)
    switches = Switch.objects.all()
    switches = switches.filter(owners__in=groups).distinct()
    error = ""
    for switch in switches:
        ports = Ports.objects.all()
        ports = ports.filter(switch=switch, groups__in=groups).distinct()
        rw_mode = []

        for group_of_ports in ports:
            regex_instance = users.RegExp(group_of_ports.regex)
            array = regex_instance.getStringArray()
            if array != [""]:
                for ports in array:
                    rw_mode.append(ports)

        try:
            switch_instance = services.get_instance(switch)
        except:
            continue
        try:
            switch.template_info = switch_instance.render_ports(user)
        except Exception as e:
            error += str(e)
    if error == "":
        error = "0"
    return HttpResponse(error, content_type='application/javascript')


def turn_on_off(request):
    """
    Check actual port status of a port and turn it on/off
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]
    person = Person.objects.get(id_person=int(uco))
    read_only = person.read_only

    log = Logger()
    id_switch = request.POST['id_switch']
    port = request.POST['port']

    switch = Switch.objects.get(id_switch=id_switch)

    if read_only | (not users.check_port_permission(port, id_switch, uco)):
        log.write_incident(user, ' missing write permission for ' +
                           switch.name + ', can\'t turn on/off the port ' + str(port))

        return HttpResponse('1', content_type='application/javascript')

    else:
        instance = services.get_instance(switch)
        services.create_port_lock(id_switch, port, uco)
        control = instance.turn_on_off(port)

        # control if settings were set up
        port = instance.get_port_descr(port)[0].val
        services.remove_port_lock(id_switch, port, uco)
        if control == 1:
            incident = switch.name + " port " + str(port) + " turned on/off"
            log.write_incident(user, incident)
            return HttpResponse("0", content_type="application/javascript")
        else:
            incident = switch.name + \
                       " unsuccessful attempt to turn on/off the port " + port
            log.write_incident(user, incident)
            return HttpResponse("2", content_type="application/javascript")


def change_alias(request):
    """
    Change alias of port
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]
    person = Person.objects.get(id_person=int(uco))
    read_only = person.read_only
    id_switch = request.POST['id_switch']
    port = request.POST['port']
    port_name = request.POST['port_name']
    switch = Switch.objects.get(id_switch=id_switch)
    log = Logger()

    new_alias = request.POST['newalias']

    # new alias is empty string, done
    if new_alias.replace(' ', '').replace('\n', '') == '':
        return HttpResponse("2", content_type="application/javascript")

    instance = services.get_instance(switch)

    try:
        new_alias.decode('ascii')
    except UnicodeEncodeError:
        log.write_incident(user, switch.name +
                           ', can\'t change alias of the port ' + str(port) + ' - ' + str(port_name) +
                           ', new alias contains non-ASCII characters')
        return HttpResponse('3', content_type='application/javascript')

    if read_only | (not users.check_port_permission(port, id_switch, uco)):
        log.write_incident(user, ' missing write permission for ' +
                           switch.name + ', can\'t change alias of the port ' + str(port) + ' - ' + str(port_name))
        return HttpResponse('4', content_type='application/javascript')

    else:
        oldalias = instance.get_port_alias(port)

        # new alias is not changed, done
        if oldalias == new_alias:
            return HttpResponse("5", content_type="application/javascript")
        services.create_port_lock(id_switch, port, uco)
        control = instance.change_port_alias(port, port_name, new_alias)
        services.remove_port_lock(id_switch, port, uco)
        if control:
            services.create_lock(switch)
            incident = switch.name + " changed alias of port " + \
                       str(port) + ' - ' + str(port_name) + " into " + new_alias
            log.write_incident(user, incident)
            return HttpResponse("0", content_type="application/javascript")

        else:
            incident = switch.name + \
                       " unsuccessful attempt to change port alias " + \
                       str(port) + ' - ' + str(port_name) + " into " + new_alias
            log.write_incident(user, incident)
            return HttpResponse("1", content_type="application/javascript")


def get_port_information(request):
    """
    Send actual information about port
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    id_switch = request.POST['id_switch']
    port = request.POST['port']

    switch = Switch.objects.get(id_switch=id_switch)

    instance = services.get_instance(switch)

    port = instance.get_changeable_port_information(port, user)
    port = json.dumps(port)
    return HttpResponse(port, content_type='application/javascript')


def change_vlan(request):
    """
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]
    person = Person.objects.get(id_person=int(uco))
    read_only = person.read_only

    id_switch = request.POST['id_switch']
    port = request.POST['port']
    port_name = request.POST['port_name']
    change_to = request.POST['vlan']

    switch = Switch.objects.get(id_switch=id_switch)
    log = Logger()

    if read_only | (
            not users.check_vlan_permission(change_to, switch, user) | (
                    not users.check_port_permission(port, id_switch, uco))):
        log.write_incident(user, ' missing write permission for ' +
                           switch.name + ', can\'t change VLAN of the port ' + str(port) + ' - ' + str(port_name))

        return HttpResponse("2", content_type='application/javascript')

    else:
        instance = services.get_instance(switch)

        if instance.get_port_vlan(port) == change_to:
            return HttpResponse("3", content_type='application/javascript')
        services.create_port_lock(id_switch, port, uco)
        control = instance.set_vlan_membership(port, port_name, change_to)
        services.remove_port_lock(id_switch, port, uco)
        if control:
            services.create_lock(switch)
            incident = switch.name + \
                       " VLAN on port " + str(port) + ' - ' + str(port_name) + " changed into " + change_to
            log.write_incident(user, incident)
            return HttpResponse("0", content_type='application/javascript')
        else:
            incident = switch.name + \
                       " Unsuccessful attempt to change VLAN on port " + str(port) + ' - ' + str(
                port_name) + " into " + change_to
            log.write_incident(user, incident)
            return HttpResponse("1", content_type='application/javascript')


def return_all_locks(request):
    """
    Return all lock files at server to avoid multiple users changing ports at same time
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    id_switch = request.POST['id_switch']
    array = []

    locks = cache.keys("lock_*")

    for lock_file in locks:
        if lock_file.endswith('.lock'):
            if lock_file.split('_')[0] in id_switch and not lock_file.split('_')[2].split('.')[0] == uco:
                switch = lock_file.split('_')[0]
                port = lock_file.split('_')[1]
                name = lock_file.split('_')[2].split('.')[0]
                person = Person.objects.get(id_person=int(name))
                array.append(switch + '_' + port + '_' + str(person))

    text = json.dumps(array)

    return HttpResponse(text, content_type='application/javascript')


def display_profile(request):
    """
    Show basic information about user
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]
    adm_groups = users.is_admin(uco)
    groups = users.from_uco_load_groups(uco)
    switches = Switch.objects.all()
    switches = switches.filter(owners__in=groups).distinct()
    switch_array = []

    for switch in switches:
        temp_structure = {'switch_name': str(switch.name), 'id_switch': switch.id_switch}
        switch_array.append(temp_structure)

    return render(request,
                  'profile.html',
                  {'user': user, 'groups':
                      groups, 'switches': switch_array, 'adm_groups': adm_groups})


def display_port_move(request):
    """
    TODO description
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]
    adm_groups = users.is_admin(uco)
    groups = users.from_uco_load_groups(uco)
    switches = Switch.objects.all().order_by('location', 'name')
    switches = switches.filter(owners__in=groups).distinct()
    locations = Location.objects.all().filter(switch__in=switches).distinct()

    return render(request,
                  'port_move.html',
                  {'user': user,
                   'locations': locations, 'adm_groups': adm_groups})


def transfer_port_settings(request):
    """
    Transfers all settings of some port into another and sets source port into default config
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]
    person = Person.objects.get(id_person=int(uco))
    readonly = person.read_only
    log = Logger()

    src_switch_id = request.POST['src_switch_id']
    src_port_id = request.POST['src_port_id']
    src_port_name = request.POST['src_port_name']
    dst_switch_id = request.POST['dst_switch_id']
    dst_port_id = request.POST['dst_port_id']
    dst_port_name = request.POST['dst_port_name']

    src_switch = Switch.objects.get(id_switch=src_switch_id)
    dst_switch = Switch.objects.get(id_switch=dst_switch_id)
    src_instance = services.get_instance(src_switch)
    dst_instance = services.get_instance(dst_switch)

    # permission check
    if readonly | (not users.check_port_permission(src_port_id, src_switch_id, uco)):
        incident = ' missing write permission for ' + \
                   src_switch.name + ', can\'t transfer port settings of port' + src_port_name
        log.write_incident(user, incident)
        return HttpResponse(incident, content_type='application/javascript')
    if readonly | (not users.check_port_permission(dst_port_id, dst_switch_id, uco)):
        incident = ' missing write permission for ' + \
                   dst_switch.name + ', can\'t transfer port settings of port' + dst_port_name
        log.write_incident(user, incident)
        return HttpResponse(incident, content_type='application/javascript')
    services.create_port_lock(src_switch_id, src_port_id, uco)
    services.create_port_lock(dst_switch_id, dst_port_id, uco)

    try:
        src_alias = src_instance.get_port_alias(src_port_id)
        src_vlan = src_instance.get_port_vlan(src_port_id)
        src_portsec = src_instance.get_port_sec(src_port_id)
        src_status = src_instance.get_port_status(src_port_id)
        transferred = dst_instance.transfer_bulk_port_vals(dst_port_id, dst_port_name, src_alias, src_vlan, src_portsec, src_status)
        sleep(5)
        set_to_default = src_instance.transfer_src_default_config(src_port_id, src_port_name)
        if not transferred or not set_to_default:
            incident = 'Unsuccessful attempt to transfer port.'
            log.write_incident(user, incident)
    except Exception as e:
        incident = 'Unsuccessful attempt to transfer port : ' + str(e)
        log.write_incident(user, incident)
        services.remove_port_lock(src_switch_id, src_port_id, uco)
        services.remove_port_lock(dst_switch_id, dst_port_id, uco)
        return HttpResponse(incident, content_type='application/javascript')

    services.remove_port_lock(src_switch_id, src_port_id, uco)
    services.remove_port_lock(dst_switch_id, dst_port_id, uco)
    services.create_lock(src_switch)
    services.create_lock(dst_switch)
    return HttpResponse("0", content_type='application/javascript')


def transfer_switch(request):
    """
    Transfers all ports of switch into another and sets source ports into default config
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]
    log = Logger()

    src_switch_id = request.POST['src_switch_id']
    dst_switch_id = request.POST['dst_switch_id']

    src_switch = Switch.objects.get(id_switch=src_switch_id)
    dst_switch = Switch.objects.get(id_switch=dst_switch_id)
    src_instance = services.get_instance(src_switch)
    dst_instance = services.get_instance(dst_switch)

    groups = users.from_uco_load_groups(uco)

    ports = Ports.objects.all()
    src_ports = ports.filter(switch=src_switch, groups__in=groups).distinct()
    dst_ports = ports.filter(switch=dst_switch, groups__in=groups).distinct()

    src_rw_mode = []
    for group_of_ports in src_ports:
        regex_instance = users.RegExp(group_of_ports.regex)
        array = regex_instance.getStringArray()
        if array != [""]:
            for ports in array:
                src_rw_mode.append(ports)
    dst_rw_mode = []
    for group_of_ports in dst_ports:
        regex_instance = users.RegExp(group_of_ports.regex)
        array = regex_instance.getStringArray()
        if array != [""]:
            for ports in array:
                dst_rw_mode.append(ports)

    try:
        src_switch.template_info = src_instance.render_ports(user)
        dst_switch.template_info = dst_instance.render_ports(user)
    except Exception as e:
        logger.error(str(e))
        return HttpResponse(str(e), content_type='application/javascript')

    src_port_list = src_switch.template_info.porty
    dst_port_list = dst_switch.template_info.porty

    if len(src_port_list) != len(dst_port_list):
        incident = ugettext(
            'Destination device has different number of ports. Set destination device port definition to match same number of ports as the source device')
        return HttpResponse(incident, content_type='application/javascript')

    pos = 0
    for port in src_port_list:
        src_port_id = port[0]
        dst_port_id = dst_port_list[pos][0]
        src_port_name = port[1]['descr']
        dst_port_name = dst_port_list[pos][1]['descr']
        services.create_port_lock(src_switch_id, src_port_id, uco)
        services.create_port_lock(dst_switch_id, dst_port_id, uco)

        try:
            src_alias = src_instance.get_port_alias(src_port_id)
            src_vlan = src_instance.get_port_vlan(src_port_id)
            src_portsec = src_instance.get_port_sec(src_port_id)
            src_status = src_instance.get_port_status(src_port_id)
            transferred = dst_instance.transfer_bulk_port_vals(dst_port_id, dst_port_name, src_alias, src_vlan,
                                                               src_portsec, src_status)
            set_to_default = src_instance.transfer_src_default_config(src_port_id, src_port_name)

            if not transferred or not set_to_default:
                incident = 'Unsuccessful attempt to transfer port.'
                log.write_incident(user, incident)
                services.remove_port_lock(src_switch_id, src_port_id, uco)
                services.remove_port_lock(dst_switch_id, dst_port_id, uco)
                return HttpResponse(incident, content_type='application/javascript')
        except Exception as e:
            incident = 'Unsuccessful attempt to transfer port : ' + str(e)
            log.write_incident(user, incident)
            services.remove_port_lock(src_switch_id, src_port_id, uco)
            services.remove_port_lock(dst_switch_id, dst_port_id, uco)
            return HttpResponse(incident, content_type='application/javascript')


        pos += 1
        services.remove_port_lock(src_switch_id, src_port_id, uco)
        services.remove_port_lock(dst_switch_id, dst_port_id, uco)
    services.create_lock(src_switch)
    services.create_lock(dst_switch)
    return HttpResponse("0", content_type='application/javascript')


def get_switches_by_location(request):
    """
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    locality_id = request.POST['locality_id']

    groups = users.from_uco_load_groups(uco)
    switches = Switch.objects.all().order_by('name')
    switches = switches.filter(owners__in=groups).distinct()
    switches = switches.filter(location__id_location=locality_id).distinct()
    switch_array = []

    for switch in switches:
        temp_structure = {'switch_name': str(switch), 'id_switch': switch.id_switch}
        switch_array.append(temp_structure)

    return HttpResponse(json.dumps(switch_array), content_type="application/json")


def display_export(request):
    """
    Show page with data export
    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]
    adm_groups = users.is_admin(uco)
    groups = users.from_uco_load_groups(uco)
    switches = Switch.objects.all().order_by('location', 'name')
    switches = switches.filter(owners__in=groups).distinct()

    locations = Location.objects.all().filter(switch__in=switches).distinct()

    return render(request,
                  'export.html',
                  {'user': user, 'switches': switches, 'locations': locations, 'adm_groups': adm_groups})


def export_csv(request):
    """

    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]
    user = account[1]

    features = str((request.POST['features'])).split(",")
    devices = str((request.POST['devices'])).split(",")

    status_map = {
        0: 'Connected',
        1: 'Not Connected',
        2: 'Disabled',
        3: 'error'}

    response_data = {'data': request.POST['features'] + "\n"}
    errors = []
    device_list = []

    if devices[0] == "":
        errors.append("Select one or more devices!")
        response_data['error'] = errors
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    for device in devices:
        switch = Switch.objects.get(id_switch=device)
        device_list.append(str(switch.name))
        groups = users.from_uco_load_groups(uco)
        ports = Ports.objects.all()
        ports = ports.filter(switch=switch, groups__in=groups).distinct()

        rw_mode = []

        for group_of_ports in ports:
            regex_instance = users.RegExp(group_of_ports.regex)
            array = regex_instance.getStringArray()
            if array != [""]:
                for ports in array:
                    rw_mode.append(ports)

        switch_instance = services.get_instance(switch)

        try:
            switch.template_info = switch_instance.render_ports(user)
        except SessionError as e:
            error = "Switch " + str(switch.name) + ": " + str(e)
            errors.append(error)
            logger.error("Switch " + str(switch.id_switch) + ": " + str(e))
            continue

        port_list = switch.template_info.porty

        for port in port_list:

            feature_list = [switch.template_info.name, str(port[1]["descr"])]

            vlan_sep = str(port[1]["vlan"]).split('-', 1)

            if "description" in features:
                feature_list.append(port[1]["alias"])
            if "vlan_num" in features:
                feature_list.append(vlan_sep[0])
            if "vlan_name" in features:
                feature_list.append(vlan_sep[1])
            if "portsec" in features:
                if port == '2':
                    feature_list.append('N/A')
                else:
                    try:
                        feature_list.append(port[1]["portsec"])
                    except:
                        feature_list.append('N/A')
            if "state" in features:
                feature_list.append(status_map[int(port[1]["status"])])

            response_data['data'] += ",".join(feature_list)
            response_data['data'] += "\n"

    if len(errors) > 0:
        response_data['error'] = errors
    response_data['devices'] = "".join(device_list)
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def get_poe_data(request):
    """

    :param request:
    :return:
    """
    account = users.login(request)
    uco = account[0]

    device = str((request.POST['device']))
    response_data = {}
    errors = []

    switch = Switch.objects.get(id_switch=device)
    groups = users.from_uco_load_groups(uco)

    ports = Ports.objects.all()
    ports = ports.filter(switch=switch, groups__in=groups).distinct()

    poe_ports = []

    for group_of_ports in ports:
        if group_of_ports.poe_regex is None:
            continue

        regex_instance = users.RegExp(group_of_ports.poe_regex)
        array = regex_instance.getStringArray()
        if array is not None and array != [""]:
            for ports in array:
                poe_ports.append(ports)

    switch_instance = services.get_instance(switch)

    try:
        poe_ports_table = switch_instance.get_port_pwr_consumption(poe_ports)

    except SessionError as e:
        error = "Switch " + str(switch.name) + ": " + str(e)
        errors.append(error)
        logger.error("Switch " + str(switch.id_switch) + ": " + str(e))

    response_data['data'] = poe_ports_table

    if len(errors) > 0:
        response_data['error'] = errors

    return HttpResponse(json.dumps(response_data), content_type="application/json")


# ---------------------------------------------
#              ADMINISTRACNI CAST:           -
#   Tato cast se zobrazuje pouze pokud je    -
#   uzivatel SuperUser - ma kompletni prava  -
# ---------------------------------------------


def create_acc(request):
    """
    Uses info from post request by django form to create new user,
    superuser then have to assign/create group
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    new_uco = request.POST['Uco'].replace('<br>', '')
    name = request.POST['Name'].replace('<br>', '')
    surname = request.POST['Surname'].replace('<br>', '')
    email = request.POST['Email'].replace('<br>', '')
    superuser = request.POST['SuperUser']
    readonly = request.POST['ReadOnly']
    portsec = request.POST['PortSec']
    contact = request.POST['Telephone'].replace('<br>', '')

    if superuser == "true":
        superuser = True
    else:
        superuser = False
    if readonly == "true":
        readonly = True
    else:
        readonly = False
    if portsec == "true":
        portsec = True
    else:
        portsec = False

    log = Logger()

    try:
        new_user = Person.objects.create(id_person=new_uco,
                                         name=name,
                                         surname=surname,
                                         email=email,
                                         contact=contact,
                                         superuser=superuser,
                                         read_only=readonly,
                                         port_sec=portsec)
        new_user.save()

    except Exception as e:
        log.write_incident(
            user, ' tried to create user ' + new_uco + ' - ' + name + ' ' +
                  surname + '. ' + e.message + " " + str(e))
        return HttpResponse("1 " + e.message + " " + str(e),
                            content_type="application/javascript")
    log.write_incident(
        user, ' created user ' + new_uco + ' - ' + name + ' ' + surname)

    return HttpResponse("0", content_type="application/javascript")


def update_acc(request):
    """

    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    changed = ""

    uco = request.POST['Uco'].replace('<br>', '')
    name = request.POST['Name'].replace('<br>', '')
    surname = request.POST['Surname'].replace('<br>', '')
    email = request.POST['Email'].replace('<br>', '')
    contact = request.POST['Telephone'].replace('<br>', '')
    superuser = request.POST['Superuser']
    readonly = request.POST['ReadOnly']
    portsec = request.POST['PortSec']

    person = Person.objects.get(id_person=uco)
    if person.name != name:
        person.name = name
        changed += " name into " + name

    if person.surname != surname:
        person.surname = surname
        changed += " surname into " + surname

    if person.email != email:
        person.email = email
        changed += " email into " + email

    if person.contact != contact:
        person.contact = contact
        changed += " tel. number into " + contact

    if str(person.superuser).lower() != superuser:
        if superuser == "true":
            person.superuser = True
        else:
            person.superuser = False

        changed += " superuser rights"

    if str(person.read_only).lower() != readonly:
        if readonly == "true":
            person.read_only = True
        else:
            person.read_only = False
        changed += " readonly rights"

    if str(person.port_sec).lower() != portsec:
        if portsec == "true":
            person.port_sec = True
        else:
            person.port_sec = False
        changed += " portsec rights"

    if len(changed) > 0:
        log = Logger()
        try:
            person.save()
        except Exception as e:
            log.write_incident(
                user, ' tried to change user with UCO ' + uco + changed + '. ' +
                      e.message + " " + str(e))
            return HttpResponse("1 " + e.message + " " + str(e),
                                content_type="application/javascript")
        log.write_incident(
            user, ' changed user with UCO ' + uco + changed + ".")

    return HttpResponse("0", content_type="application/javascript")


def get_group_users(request):
    """

    :param request:
    :return:
    """
    group_id = request.POST['group_id']
    group = Group.objects.get(id_group=group_id)
    people_all = list(Person.objects.values('id_person', 'name', 'surname'))
    members = list(group.members.values('id_person', 'name', 'surname'))
    data_struct = {
        "all": people_all,
        "members": members
    }
    return JsonResponse(data_struct)


def get_group_admins(request):
    """

    :param request:
    :return:
    """
    group_id = request.POST['group_id']
    group = Group.objects.get(id_group=group_id)
    people_all = list(Person.objects.values('id_person', 'name', 'surname'))
    members = list(group.admins.values('id_person', 'name', 'surname'))
    data_struct = {
        "all": people_all,
        "members": members
    }
    return JsonResponse(data_struct)


def add_user_to_group(request):
    """
    Add user to group by user ID and group ID at input
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    user_id = request.POST['user_id']
    group_id = request.POST['group_id']

    log = Logger()

    group = Group.objects.get(id_group=group_id)
    person = Person.objects.get(id_person=user_id)

    try:
        group.members.add(person)
        group.save()
    except Exception as e:
        log.write_incident(
            user, ' tried to add user ' + str(person) + ' to the group ' +
                  str(group) + '. ' + e.message + " " + str(e))
        return HttpResponse("1 " + e.message + " " + str(e),
                            content_type="application/javascript")
    log.write_incident(
        user, ' added user ' + str(person) + ' to the group ' + str(group))

    return HttpResponse("0", content_type="application/javascript")


def add_admin_to_group(request):
    """
    Add admin to group by user ID and group ID at input
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    user_id = request.POST['user_id']
    group_id = request.POST['group_id']

    log = Logger()

    group = Group.objects.get(id_group=group_id)
    person = Person.objects.get(id_person=user_id)

    try:
        group.admins.add(person)
        group.save()
    except Exception as e:
        log.write_incident(
            user, ' tried to add admin ' + str(person) + ' to the group ' +
                  str(group) + '. ' + e.message + " " + str(e))
        return HttpResponse("1 " + e.message + " " + str(e),
                            content_type="application/javascript")
    log.write_incident(
        user, ' added user ' + str(person) + ' to the group ' + str(group))

    return HttpResponse("0", content_type="application/javascript")


def remove_user(request):
    """
    Removes user completely
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    user_id = request.POST['user_id']
    to_remove = str(Person.objects.get(id_person=user_id))

    log = Logger()
    try:
        Person.objects.filter(id_person=user_id).delete()
    except Exception as e:
        log.write_incident(user, ' tried to remove user ' + to_remove + '. ' +
                           e.message + " " + str(e))
        return HttpResponse("1 " + e.message + " " + str(e),
                            content_type="application/javascript")
    log.write_incident(user, ' removed user ' + to_remove)
    return HttpResponse("0", content_type='application/javascript')


def remove_user_from_group(request):
    """
    Removes user from group
    :param request:
    :return:
    """
    account = users.login(request)
    removing_user = account[1]
    group_id = request.POST['group_id']
    user_id = request.POST['user_id']

    group = Group.objects.get(id_group=group_id)
    user = Person.objects.get(id_person=user_id)

    log = Logger()

    try:
        group.members.remove(user)
        group.save()
    except Exception as e:
        log.write_incident(
            removing_user, ' tried to remove user ' + str(user) +
                           ' from group ' + str(group) + '. ' + e.message + ' ' + str(e))
        return HttpResponse("1 " + e.message + " " + str(e),
                            content_type="application/javascript")
    log.write_incident(
        removing_user, ' removed user ' + str(user) +
                       ' from group ' + str(group))
    return HttpResponse("0", content_type="application/javascript")


def remove_admin_from_group(request):
    """
    Removes user from group
    :param request:
    :return:
    """
    account = users.login(request)
    removing_user = account[1]
    group_id = request.POST['group_id']
    user_id = request.POST['user_id']

    group = Group.objects.get(id_group=group_id)
    user = Person.objects.get(id_person=user_id)

    log = Logger()

    try:
        group.admins.remove(user)
        group.save()
    except Exception as e:
        log.write_incident(
            removing_user, ' tried to remove admin ' + str(user) +
                           ' from group ' + str(group) + '. ' + e.message + ' ' + str(e))
        return HttpResponse("1 " + e.message + " " + str(e),
                            content_type="application/javascript")
    log.write_incident(
        removing_user, ' removed admin ' + str(user) +
                       ' from group ' + str(group))
    return HttpResponse("0", content_type="application/javascript")


def remove_group(request):
    """
    Remove group of users
    :param request:
    :return:
    """
    group_id = request.POST['group_id']
    account = users.login(request)
    user = account[1]
    group = str(Group.objects.get(id_group=group_id))
    log = Logger()
    try:
        Group.objects.filter(id_group=group_id).delete()
    except Exception as e:
        log.write_incident(
            user, ' tried to remove group ' +
                  group + '. ' +
                  e.message + " " + str(e))
        return HttpResponse("1 " + e.message + " " + str(e),
                            content_type="application/javascript")
    log.write_incident(
        user, ' removed group ' + group)
    return HttpResponse("0", content_type="application/javascript")


def create_group(request):
    """
    Create group by django.models
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    name = request.POST['name'].replace('<br>', '')

    # finds out if there are some groups already to set ID
    if Group.objects.all().count() is 0:
        id_group = 0
        # no group exists, id set to 0
    else:
        # there are several groups, id set to number of them + 1
        id_group = Group.objects.all().order_by("-id_group")[0].id_group + 1

    log = Logger()
    try:
        group = Group.objects.create(id_group=id_group, name=name)
        group.save()
    except Exception as e:
        log.write_incident(user, ' tried to create group ' + name + '. ' +
                           e.message + " " + str(e))
        return HttpResponse("1 " + e.message + " " + str(e),
                            content_type="application/javascript")
    log.write_incident(user, ' created group ' + name)
    return HttpResponse("0", content_type="application/javascript")


def update_group_name(request):
    """
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    changed = ""

    name = request.POST['name'].replace('<br>', '')
    gid = request.POST['gid'].replace('<br>', '')

    group = Group.objects.get(id_group=gid)
    if group.name != name:
        group.name = name
        changed += " name into " + name

    if len(changed) > 0:
        log = Logger()
        try:
            group.save()
        except Exception as e:
            log.write_incident(
                user, ' tried to change group with ID ' + gid + changed + '. ' +
                      e.message + " " + str(e))
            return HttpResponse("1 " + e.message + " " + str(e),
                                content_type="application/javascript")
        log.write_incident(
            user, ' changed group with ID ' + gid + changed + ".")

    return HttpResponse("0", content_type="application/javascript")


def update_switch_address(request):
    account = users.login(request)
    user = account[1]

    new_address = request.POST['address'].replace('<br>', '')
    switch_id = request.POST['switch_id'].replace('<br>', '')

    switch = Switch.objects.get(id_switch=switch_id)
    log = Logger()

    if switch.dest_host != new_address:
        switch.dest_host = new_address

        try:
            switch.save()

            log.write_incident(
                user, ' changed switch with ID ' + switch_id + " address to " + new_address + ".")
            return HttpResponse("0", content_type="application/javascript")

        except Exception as e:
            logger.error(str(e))
            return HttpResponse(str(e), content_type="application/javascript")

    return HttpResponse("0", content_type="application/javascript")


def update_switch_name(request):
    account = users.login(request)
    user = account[1]

    new_name = request.POST['name'].replace('<br>', '')
    switch_id = request.POST['switch_id'].replace('<br>', '')

    switch = Switch.objects.get(id_switch=switch_id)
    log = Logger()

    if switch.name != new_name:
        switch.name = new_name

        try:
            switch.save()

            log.write_incident(
                user, ' changed switch with ID ' + switch_id + " name to " + new_name + ".")
            return HttpResponse("0", content_type="application/javascript")

        except Exception as e:
            logger.error(str(e))
            return HttpResponse(str(e), content_type="application/javascript")

    return HttpResponse("0", content_type="application/javascript")


def update_location_name(request):
    account = users.login(request)
    user = account[1]

    new_name = request.POST['name'].replace('<br>', '')
    location_id = request.POST['loc_id'].replace('<br>', '')

    location = Location.objects.get(id_location=location_id)
    log = Logger()

    if location.name != new_name:
        location.name = new_name

        try:
            location.save()

            log.write_incident(
                user, ' changed location with ID ' + location_id + " name to " + new_name + ".")
            return HttpResponse("0", content_type="application/javascript")

        except Exception as e:
            logger.error(str(e))
            return HttpResponse(str(e), content_type="application/javascript")

    return HttpResponse("0", content_type="application/javascript")


def update_switch_location(request):
    account = users.login(request)
    user = account[1]

    new_location_id = request.POST['location'].replace('<br>', '')
    new_location = Location.objects.get(id_location=new_location_id)

    switch_id = request.POST['switch_id'].replace('<br>', '')

    switch = Switch.objects.get(id_switch=switch_id)
    log = Logger()

    if switch.location != new_location:
        switch.location = new_location

        try:
            switch.save()

            log.write_incident(
                user, ' changed switch with ID ' + switch_id + " location to " + new_location_id + ".")
            return HttpResponse("0", content_type="application/javascript")

        except Exception as e:
            logger.error(str(e))
            return HttpResponse(str(e), content_type="application/javascript")

    return HttpResponse("0", content_type="application/javascript")


def update_switch_device(request):
    account = users.login(request)
    user = account[1]

    device_id = request.POST['device'].replace('<br>', '')
    device = Device.objects.get(id_device=device_id)

    switch_id = request.POST['switch_id'].replace('<br>', '')

    switch = Switch.objects.get(id_switch=switch_id)
    log = Logger()

    if switch.device != device:
        switch.device = device

        try:
            switch.save()

            log.write_incident(
                user, ' changed switch with ID ' + switch_id + " device to " + device_id + ".")
            return HttpResponse("0", content_type="application/javascript")

        except Exception as e:
            logger.error(str(e))
            return HttpResponse(str(e), content_type="application/javascript")

    return HttpResponse("0", content_type="application/javascript")


def update_switch_snmp(request):
    account = users.login(request)
    user = account[1]

    new_snmp_version = request.POST['snmp'].replace('<br>', '')

    switch_id = request.POST['switch_id'].replace('<br>', '')

    switch = Switch.objects.get(id_switch=switch_id)
    log = Logger()

    if switch.snmp_version != new_snmp_version:
        switch.snmp_version = new_snmp_version

        try:
            switch.save()

            log.write_incident(
                user, ' changed switch with ID ' + switch_id + " snmp version to " + new_snmp_version + ".")
            return HttpResponse("0", content_type="application/javascript")

        except Exception as e:
            logger.error(str(e))
            return HttpResponse(str(e), content_type="application/javascript")

    return HttpResponse("0", content_type="application/javascript")


def update_switch_community_string(request):
    account = users.login(request)
    user = account[1]

    new_community_string = request.POST['community_string'].replace('<br>', '')

    switch_id = request.POST['switch_id'].replace('<br>', '')

    switch = Switch.objects.get(id_switch=switch_id)
    log = Logger()

    if switch.community != new_community_string:
        switch.community = new_community_string

        try:
            switch.save()

            log.write_incident(
                user, ' changed switch with ID ' + switch_id + " community string to " + new_community_string + ".")
            return HttpResponse("0", content_type="application/javascript")

        except Exception as e:
            logger.error(str(e))
            return HttpResponse(str(e), content_type="application/javascript")

    return HttpResponse("0", content_type="application/javascript")


def update_ports_def_name(request):
    """
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    changed = ""

    name = request.POST['name'].replace('<br>', '')
    ports_def_id = request.POST['pid'].replace('<br>', '')

    ports_def = Ports.objects.get(id_ports=ports_def_id)
    if ports_def.name != name:
        ports_def.name = name
        changed += " name into " + name

    if len(changed) > 0:
        log = Logger()
        try:
            ports_def.save()
        except Exception as e:
            log.write_incident(
                user, ' tried to change port definition with ID ' + ports_def_id + changed + '. ' +
                      e.message + " " + str(e))
            return HttpResponse("1 " + e.message + " " + str(e),
                                content_type="application/javascript")
        log.write_incident(
            user, ' changed port definition with ID ' + ports_def_id + changed + ".")

    return HttpResponse("0", content_type="application/javascript")


def create_location(request):
    """
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    name = request.POST['name']
    street = request.POST['street']
    city = request.POST['city']

    if Location.objects.all().count() is 0:
        id_location = 0
    else:
        id_location = Location.objects.all().order_by(
            "-id_location")[0].id_location + 1

    log = Logger()

    try:
        location = Location.objects.create(id_location=id_location,
                                           name=name,
                                           city=city)
        location.save()
        log.write_incident(
            user, ' created location ' + name + ' ' + street + ' ' + city)
        return HttpResponse("0", content_type="application/javascript")
    except Exception as e:
        logger.error(str(e))
        return HttpResponse(str(e), content_type="application/javascript")


def create_device(request):
    """
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    manufacturer = request.POST['manufacturer']
    model = request.POST['model']
    note = request.POST['note']

    if Device.objects.all().count() is 0:
        id_device = 0
    else:
        id_device = Device.objects.all().order_by(
            "-id_device")[0].id_device + 1

    log = Logger()

    try:
        device = Device.objects.create(id_device=id_device,
                                       manufacturer=manufacturer,
                                       name=model,
                                       note=note)
        device.save()

        log.write_incident(user, ' created device template ' + manufacturer + ' ' + model + ' ' + note)
        return HttpResponse("0", content_type="application/javascript")

    except Exception as e:
        logger.error(str(e))
        return HttpResponse(str(e), content_type="application/javascript")


def create_switch(request):
    """
    :param request:
    :return:
    """

    account = users.login(request)
    user = account[1]

    name = request.POST['name']
    address = request.POST['address'].replace(' ', '')
    community = request.POST['community']
    snmp = request.POST['snmp']
    poe_regex = request.POST['poe_regex']
    ssh_key = request.POST['ssh_key']

    loc = Location.objects.get(
        id_location=request.POST['loc'].replace(" ", ''))
    device = Device.objects.get(
        id_device=request.POST['device'].replace(" ", ''))

    log = Logger()

    try:
        groups = Group.objects.all().filter(
            id_group__in=(map(lambda x: x.replace(" ", ""), request.POST['groups'].split(","))))
    except ValueError:
        groups = []

    if Switch.objects.all().count() is 0:
        id_switch = 0
    else:
        id_switch = Switch.objects.all().order_by(
            "-id_switch")[0].id_switch + 1

    try:
        switch = Switch.objects.create(id_switch=id_switch,
                                       name=name,
                                       dest_host=address,
                                       location=loc,
                                       device=device,
                                       community=community,
                                       snmp_version=snmp,
                                       poe_regex=poe_regex,
                                       ssh_key=ssh_key)
        for group in groups:
            switch.owners.add(group)

        switch.save()

        priv_key = '/srv/ssh_keys/' + address
        f = open(priv_key, "w")
        f.write(ssh_key)
        f.close()

        log.write_incident(user, ' created device ' + name + ' ' + address)
        return HttpResponse("0", content_type="application/javascript")

    except Exception as e:
        logger.error(str(e))
        return HttpResponse(str(e), content_type="application/javascript")


def remove_switch(request):
    """
    Removes switch
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    id_switch = request.POST['id_switch']

    log = Logger()

    try:
        deleted_switch = str(Switch.objects.get(id_switch=id_switch))

        Switch.objects.filter(id_switch=id_switch).delete()
        log.write_incident(user, ' removed device ' +
                           str(id_switch) + ' ' + deleted_switch)
        return HttpResponse("0", content_type="application/javascript")

    except Exception as e:
        logger.error(str(e))
        return HttpResponse(str(e), content_type="application/javascript")


def manage_location(request):
    """
    Show possible settings about switch
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    switches = Switch.objects.all().order_by('location', 'name')
    switches = switches.filter(owners__in=adm_groups)
    devices = Device.objects.all()
    groups = Group.objects.all()
    locations = Location.objects.all()
    locations = locations.filter(groups__in=adm_groups).distinct()
    ports = Ports.objects.all()
    ports = ports.filter(groups__in=adm_groups).distinct()
    vlans = Vlans.objects.all()
    vlans = vlans.filter(groups__in=adm_groups).distinct()

    return render(request, 'manage_location.html',
                  {'switches': switches,
                   'groups': groups,
                   'locations': locations,
                   'devices': devices,
                   "user": user,
                   'adm_groups': adm_groups,
                   'ports': ports,
                   'vlans': vlans,
                   })


def manage_users(request):
    """
    Returns web page for managing users
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    all_people = Person.objects.all().order_by('surname', 'name')
    people_count = all_people.count()
    if user.superuser:
        return render(request,
                      'admin_users.html',
                      {'user': user,
                       'all_people': all_people,
                       'people_count': people_count, 'adm_groups': adm_groups})


def manage_groups(request):
    """
    Returns web page for managing groups
    :param request:
    :return:
    """

    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    all_people = Person.objects.all().order_by('surname', 'name')
    groups = Group.objects.all().order_by('name')
    group_count = groups.count()
    if user.superuser:
        return render(request,
                      'admin_groups.html',
                      {'user': user, 'all_people': all_people, 'groups': groups, 'group_count': group_count,
                       'adm_groups': adm_groups})


def change_statement(request):
    """
    Add and remove ports of groups 0 removes and 1 add
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    id_switch = request.POST['id_switch']
    id_group = request.POST['id_group']
    mode = request.POST['mode']

    switch = Switch.objects.get(id_switch=id_switch)
    group = Group.objects.get(id_group=id_group)

    log = Logger()

    try:
        if mode == '0':
            switch.owners.add(group)
            switch.save()
            log.write_incident(
                user, ' removed the group ' + str(id_group) + ' access to the device ' + str(id_switch))

        elif mode == '1':
            switch.owners.remove(group)
            switch.save()
            log.write_incident(
                user, ' gave the group ' + str(id_group) + 'access to the device ' + str(id_switch))

        else:
            return HttpResponse("mode was not recognized", content_type="application/javascript")

        return HttpResponse("0", content_type="application/javascript")

    except Exception as e:
        logger.error(str(e))
        return HttpResponse(str(e), content_type="application/javascript")


def change_statement_pv(request):
    """
    Adds/removes devices/groups to port and VLAN definitions.
    pv tells if it's port or VLAN and gs if to add or remove
    :param request:
    :return:
    """
    pv = request.POST['pv'].replace(" ", "")
    pv_id = request.POST['pv_id'].replace(" ", "")
    gs = request.POST['gs'].replace(" ", "")
    gs_id = request.POST['gs_id'].replace(" ", "")
    new_statement = request.POST['new_statement'].replace(" ", "")

    if pv.upper() == 'P':
        ports = Ports.objects.get(id_ports=pv_id)

        if new_statement.upper() == '1':
            if gs == 'G':
                group = Group.objects.get(id_group=gs_id)
                ports.groups.add(group)
                ports.save()

            else:
                switch = Switch.objects.get(id_switch=gs_id)
                ports.switch.add(switch)
                ports.save()
        else:
            if gs == 'G':
                group = Group.objects.get(id_group=gs_id)
                ports.groups.remove(group)
            else:
                switch = Switch.objects.get(id_switch=gs_id)
                ports.switch.remove(switch)

    elif pv.upper() == 'V':

        vlans = Vlans.objects.get(id_vlans=pv_id)

        if new_statement.upper() == '1':
            if gs == 'G':
                group = Group.objects.get(id_group=gs_id)
                vlans.groups.add(group)
                vlans.save()

            else:
                switch = Switch.objects.get(id_switch=gs_id)
                vlans.switch.add(switch)
                vlans.save()
        else:
            if gs == 'G':
                group = Group.objects.get(id_group=gs_id)
                vlans.groups.remove(group)
            else:
                switch = Switch.objects.get(id_switch=gs_id)
                vlans.switch.remove(switch)

    else:
        logger.error("Invalid mode (P/V).")
        raise Exception

    return HttpResponse("0", content_type="application/javascript")


def change_visibility(request):
    """
    Changes visibility of port/vlan groups
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    def_id = request.POST['id']
    pv = request.POST['pv']
    log = Logger()

    if pv.upper() == 'P':
        try:
            ports = Ports.objects.get(id_ports=def_id)
            ports.view_only = not ports.view_only
            ports.save()
        except Exception as e:
            logger.error(
                "unable to change port definition " + def_id + " visibility: " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")

        log.write_incident(user, "changed port definition " + def_id + " visibility")

    elif pv.upper() == 'V':
        try:
            vlans = Vlans.objects.get(id_vlans=def_id)
            if vlans.view_only:
                vlans.view_only = False
            else:
                vlans.view_only = True
            vlans.save()
        except Exception as e:
            logger.error(
                "unable to change vlan definition " + def_id + " visibility: " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")

        log.write_incident(user, "changed vlan definition " + def_id + " visibility")

    return HttpResponse("0", content_type="application/javascript")


def manage_switches(request):
    """
    Show possible settings about switch
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    switches = Switch.objects.all().order_by('location', 'name')
    devices = Device.objects.all()
    locations = Location.objects.all()
    groups = Group.objects.all()
    if user.superuser:
        return render(request, 'manage_switches.html',
                      {'switches': switches,
                       'groups': groups,
                       'locations': locations,
                       'devices': devices,
                       "user": user, 'adm_groups': adm_groups})


def manage_default_port_config(request):
    """
    Show possible settings about switch
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    devices = Device.objects.all()
    config = configparser.ConfigParser()
    config.read('/srv/config/app_config.ini')
    max_port_sec = config['app']['max_allowed_devices']
    if user.superuser:
        return render(request, 'manage_default_port_config.html',
                      {'devices': devices,
                       "user": user,
                       'adm_groups': adm_groups,
                       'max_port_sec': max_port_sec,
                       })


def update_device_default_port_config(request):
    account = users.login(request)
    user = account[1]

    new_config = request.POST['port_config'].replace('<br>', '')
    device_id = request.POST['device_id'].replace('<br>', '')

    device = Device.objects.get(id_device=device_id)
    log = Logger()
    if user.superuser:
        if device.default_port_config != new_config:
            device.default_port_config = new_config

            try:
                device.save()

                log.write_incident(
                    user, ' changed device with ID ' + device_id + " default port config to " + new_config + ".")
                return HttpResponse("0", content_type="application/javascript")

            except Exception as e:
                logger.error(str(e))
                return HttpResponse(str(e), content_type="application/javascript")

        return HttpResponse("0", content_type="application/javascript")


def apply_port_default_config(request):
    account = users.login(request)
    uco = account[0]
    user = account[1]
    person = Person.objects.get(id_person=int(uco))
    read_only = person.read_only
    log = Logger()

    id_switch = request.POST['id_switch']
    port_id = request.POST['port_id']
    port_name = request.POST['port_name']

    switch = Switch.objects.get(id_switch=id_switch)
    instance = services.get_instance(switch)

    # Check permission for user:
    if read_only | (not users.check_port_permission(port_id, id_switch, uco)):
        log.write_incident(user, ' missing write permission for ' +
                           switch.name + ', can\'t apply default config for port ' + port_id + ' ' + port_name)
        return HttpResponse(False, content_type='application/javascript')
    services.create_port_lock(id_switch, port_id, uco)
    control = instance.apply_port_default_config(port_id, port_name)
    services.remove_port_lock(id_switch, port_id, uco)
    if not control:
        log.write_incident(
            user, ' tried to apply port ' + port_id + ' ' + port_name + ' default donfiguration on device ' +
                  str(switch.id_switch) + ' ' + switch.name
        )

    log.write_incident(
        user, ' aapplied port ' + port_id + ' ' + port_name + ' default donfiguration on device ' +
              str(switch.id_switch) + ' ' + switch.name
    )
    services.create_lock(switch)
    return HttpResponse(control, content_type='application/javascript')


def update_port_sec(request):
    account = users.login(request)
    uco = account[0]
    user = account[1]
    person = Person.objects.get(id_person=int(uco))
    read_only = person.read_only
    log = Logger()

    id_switch = request.POST['id_switch']
    port = request.POST['port']
    port_sec = request.POST['port_sec']
    switch = Switch.objects.get(id_switch=id_switch)

    instance = services.get_instance(switch)
    port_name = instance.get_port_descr(port)[0].val

    if read_only | (not users.check_port_permission(port, id_switch, uco)):
        log.write_incident(user, ' missing write permission for ' +
                           switch.name + ', can\'t change portsec for port ' + port)
        return HttpResponse(False, content_type='application/javascript')
    services.create_port_lock(id_switch, port, uco)

    run_cfg = open('/srv/tftp/' + switch.dest_host, "w")
    run_cfg.write('interface ' + port_name + '\n' + ' switchport port-security maximum ' + port_sec)
    run_cfg.close()
    config = configparser.ConfigParser()
    config.read('/srv/config/app_config.ini')
    tftp_server = config['app']['tftp_server']

    rc = call(
        ['/srv/scripts/parcial.sh', tftp_server, str(switch.dest_host), str(switch.dest_host),
         str(switch.community)])
    services.remove_port_lock(id_switch, port, uco)
    os.remove('/srv/tftp/' + switch.dest_host)
    services.create_lock(switch)
    return HttpResponse(rc, content_type='application/javascript')


def update_max_port_sec(request):
    account = users.login(request)
    user = account[1]

    port_sec = request.POST['port_sec'].replace('<br>', '')

    config = configparser.ConfigParser()
    config.read('/srv/config/app_config.ini')
    config['app']['max_allowed_devices'] = port_sec
    with open('/srv/config/app_config.ini', 'w') as configfile:
        config.write(configfile)

    return HttpResponse("0", content_type="application/javascript")


def show_log(request):
    """
    Show global log
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    logs = Logger().get_list_of_logs(200)
    logs.reverse()

    return render(request,
                  "logger.html",
                  {"user": user, 'logs': logs, 'adm_groups': adm_groups})


def load_more_logs(request):
    """
    :param request:
    :return:
    """
    lFrom = request.POST['lFrom']
    lTo = request.POST['lTo']

    try:
        obj_json = serializers.serialize('json', Logger().get_list_of_logs_from_to(lFrom, lTo))
        obj_list = json.loads(obj_json)
        json_data = json.dumps(obj_list)

        return HttpResponse(json_data, content_type='application/json')

    except Exception as e:
        json_structure = {'error': str(e)}
        logger.error(str(e))

    return HttpResponse(json.dumps(json_structure), content_type='application/json')


def add_reply(request):
    """
    Add reply of user to some event which will be shown to another user
    :param request:
    :return:
    """
    id_event = request.POST['id_event']
    text = request.POST['text']

    event = Event.objects.get(id_event=id_event)
    event.seen = False
    # have to remove <br> created at html page and save event
    event.reply = text.replace('<br>', '')

    event.save()

    return HttpResponse('0', content_type='application/javascript')


def display_guide(request):
    """
    Show guide for using portmanager
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    return render(request, "guide.html", {"user": user, 'adm_groups': adm_groups})


def display_guide_usr(request):
    """
    Show guide for using portmanager
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    return render(request, "guide_usr.html", {"user": user, 'adm_groups': adm_groups})


def display_guide_dev(request):
    """
    Show guide for using portmanager
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    return render(request, "guide_dev.html", {"user": user, 'adm_groups': adm_groups})


def display_guide_local(request):
    """
    Show guide for using portmanager
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    return render(request, "guide_local.html", {"user": user, 'adm_groups': adm_groups})


def change_regex(request):
    """
    Changes regex at existing ports/vlans,
    mode contain info if its port/vlan
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    def_id = request.POST['id']
    regex = request.POST['regex']
    mode = request.POST['pv']

    log = Logger()

    if mode.upper() == 'P':
        port = Ports.objects.get(id_ports=def_id)
        old_regex = port.regex

        try:
            port.regex = regex
            port.save()
        except Exception as e:
            logger.error(
                "unable to change port definition " + def_id + " regex from " +
                old_regex + "into " + regex + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")

        log.write_incident(user, ' changed port definition ' +
                           port.name + ' with ID ' + def_id + ' into ' + regex)

    if mode.upper() == 'POE':
        port = Ports.objects.get(id_ports=def_id)
        old_regex = port.poe_regex

        try:
            port.poe_regex = regex
            port.save()
        except Exception as e:
            logger.error(
                "unable to change port poe definition " + def_id + " regex from " +
                old_regex + "into " + regex + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")

        log.write_incident(user, ' changed port poe definition ' +
                           port.name + ' with ID ' + def_id + ' into ' + regex)

    elif mode.upper() == 'V':
        vlan = Vlans.objects.get(id_vlans=def_id)
        old_regex = vlan.regex

        try:
            vlan.regex = regex
            vlan.save()
        except Exception as e:
            logger.error(
                "unable to change vlan definition " + def_id + " regex from " +
                old_regex + "into " + regex + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")

        log.write_incident(user, ' changed vlan definition ' +
                           vlan.name + ' with ID ' + def_id + ' into ' + regex)

    return HttpResponse("0", content_type='application/javascript')


def change_poe_regex(request):
    """
    Changes existing PoE regex.
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    switch_id = request.POST['switch_id']
    regex = request.POST['poe_regex']
    switch = Switch.objects.get(id_switch=switch_id)
    log = Logger()

    if switch.poe_regex != regex:
        switch.poe_regex = regex

        try:
            switch.save()

            log.write_incident(
                user, ' changed switch with ID ' + switch_id + " PoE regex to " + regex + ".")
            return HttpResponse("0", content_type="application/javascript")

        except Exception as e:
            logger.error(str(e))
            return HttpResponse(str(e), content_type="application/javascript")

    return HttpResponse("0", content_type="application/javascript")


def change_ssh_key(request):
    """
    Changes existing PoE regex.
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]

    switch_id = request.POST['switch_id']
    ssh_key = request.POST['ssh_key']
    switch = Switch.objects.get(id_switch=switch_id)
    log = Logger()

    if switch.ssh_key != ssh_key:
        switch.ssh_key = ssh_key

        try:
            switch.save()
            priv_key = '/srv/ssh_keys/' + str(switch.dest_host)
            f = open(priv_key, "w")
            f.write(ssh_key)
            f.close()

            log.write_incident(
                user, ' changed switch with ID ' + switch_id + " SSH key.")
            return HttpResponse("0", content_type="application/javascript")

        except Exception as e:
            logger.error(str(e))
            return HttpResponse(str(e), content_type="application/javascript")

    return HttpResponse("0", content_type="application/javascript")


def add_portvlan(request):
    """
    Add ports/vlans to system,
    mode contains if its vlan/port,
    similar to create_group()
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    name = request.POST['name']

    switches = request.POST['switches']
    groups = request.POST['groups']

    if switches != "":
        switches = Switch.objects.all().filter(
            id_switch__in=map(lambda x: x.replace(' ', ''), request.POST['switches'].split(',')))

    if groups != "":
        groups = Group.objects.all().filter(
            id_group__in=map(lambda x: x.replace(' ', ''), request.POST['groups'].split(',')))

    if request.POST['readOnly'] == "true":
        view_only = True
    else:
        view_only = False

    regex = request.POST['regex']

    mode = request.POST['pv']

    log = Logger()

    if mode == 'V':
        if Vlans.objects.all().count() is 0:
            id_vlans = 0
        else:
            id_vlans = Vlans.objects.all().order_by("-id_vlans")[0].id_vlans + 1

        try:
            vlan = Vlans.objects.create(id_vlans=id_vlans,
                                        name=name,
                                        view_only=view_only,
                                        regex=regex,
                                        )
            for switch in switches:
                vlan.switch.add(switch)

            for group in groups:
                vlan.groups.add(group)

                vlan.save()
        except Exception as e:
            logger.error(
                "unable to create vlan definition " + str(id_vlans) + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")

        log.write_incident(user, ' created vlan definition ' + name + "with ID" + str(id_vlans))
        return HttpResponse("0", content_type="application/javascript")

    elif mode == 'P':
        if Ports.objects.all().count() is 0:
            id_ports = 0
        else:
            id_ports = Ports.objects.all().order_by("-id_ports")[0].id_ports + 1

        try:
            port = Ports.objects.create(id_ports=id_ports,
                                        name=name,
                                        view_only=view_only,
                                        regex=regex,
                                        )
            for switch in switches:
                port.switch.add(switch)

            for group in groups:
                port.groups.add(group)

                port.save()
        except Exception as e:
            logger.error(
                "unable to create port definition " + str(id_ports) + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")

        log.write_incident(user, ' created port definition ' + name + "with ID" + str(id_ports))
        return HttpResponse("0", content_type="application/javascript")


def remove_portvlan(request):
    """
    Removes port/vlan definition from database
    :param request:
    :return: 0 for success or error message
    """
    account = users.login(request)
    user = account[1]

    def_id = request.POST['id']
    mode = request.POST['pv']

    log = Logger()

    if mode == 'V':
        try:
            vlan_name = Vlans.objects.get(id_vlans=def_id).name
            print "------------------------------------------------------------------------------"
            Vlans.objects.filter(id_vlans=def_id).delete()
        except Exception as e:
            logger.error("Unsuccessful attempt to delete VLAN definition " + str(def_id) + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, ' removed VLAN definition ' + vlan_name)

    elif mode == 'P':
        try:
            port_name = Ports.objects.get(id_ports=def_id).name
            Ports.objects.filter(id_ports=def_id).delete()
        except Exception as e:
            logger.error("Unsuccessful attempt to delete port definition " + str(def_id) + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, ' removed port definition ' + port_name)

    return HttpResponse("0", content_type="application/javascript")


def get_connected_macs(request):
    """
    :param request:
    :return:
    """
    id_switch = request.POST['id_switch']
    id_port = request.POST['id_port']
    vlan_name = request.POST['vlan_name']

    switch = Switch.objects.get(id_switch=id_switch)  # services.get_instance()
    instance = services.get_instance(switch)
    try:
        macs = instance.get_connected_macs(id_port, vlan_name)
    except Exception as e:
        logger.error("unable to retrieve MAC address on device " + id_switch + ", port " + id_port + ": " + str(e))
        macs = "Unable to retrieve MAC address: " + str(e)

    return HttpResponse(json.dumps(macs))


def manage_ports(request):
    """Show admin permission"""
    account = users.login(request)
    user = account[1]
    ports = Ports.objects.all()
    adm_groups = users.is_admin(account[0])
    switches = Switch.objects.all()  # SWITCHES ARE NOT NEEDED (TAKES ID FROM THE PORTS)!! REMOVED, IF SOMETHING FUCKS UP ADD THEM BACK IMM
    groups = Group.objects.all()
    if user.superuser:
        return render(request, 'manage_ports.html',
                      {'user': user,
                       'ports': ports,
                       'switches': switches,
                       'groups': groups,
                       'adm_groups': adm_groups})


def save_portvlan_def_name(request):
    """
    Save new name of port definition
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    def_id = request.POST['def_id']
    name_new = request.POST['name_new'].strip()
    mode = request.POST['pv']
    log = Logger()

    if mode.upper() == "P":
        try:
            port_def = Ports.objects.get(id_ports=def_id)
            name = port_def.name
            port_def.name = name_new
            port_def.save()
        except Exception as e:
            logger.error(
                "unable to change port definition " + def_id + " name from " + name + " into " + name_new + ": " + str(
                    e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "changed port definition " + def_id + " name from " + name + " into " + name_new)
        return HttpResponse("0", content_type="application/javascript")

    elif mode.upper() == "V":
        try:
            vlan_def = Vlans.objects.get(id_vlans=def_id)
            name = vlan_def.name
            vlan_def.name = name_new
            vlan_def.save()
        except Exception as e:
            logger.error(
                "unable to change vlan definition " + def_id + " name from " + name + " into " + name_new + ": " + str(
                    e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "changed vlan definition " + def_id + " name from " + name + " into " + name_new)
        return HttpResponse("0", content_type="application/javascript")


def add_device_to_portvlan_def(request):
    """
    Add new device to port definition
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    def_id = request.POST['def_id']
    switch_id = request.POST['switch_id']
    mode = request.POST['pv']
    log = Logger()

    if mode.upper() == 'P':
        try:
            port_def = Ports.objects.get(id_ports=def_id)
            switch = Switch.objects.get(id_switch=switch_id)
            port_def.switch.add(switch)
            port_def.save()
        except Exception as e:
            logger.error(
                "unable to add device " + switch_id + " into port definition " +
                def_id + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "Added device " + switch.name + " into port definition " + port_def.name)
        return HttpResponse("0", content_type="application/javascript")

    elif mode.upper() == 'V':
        try:
            vlans_def = Vlans.objects.get(id_vlans=def_id)
            switch = Switch.objects.get(id_switch=switch_id)
            vlans_def.switch.add(switch)
            vlans_def.save()
        except Exception as e:
            logger.error(
                "unable to add device " + switch_id + " into vlan definition " +
                def_id + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "Added device " + switch.name + " into vlan definition " + vlans_def.name)
        return HttpResponse("0", content_type="application/javascript")


def remove_device_from_portvlan_def(request):
    """
    Remove device from port definition
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    def_id = request.POST['def_id']
    switch_id = request.POST['switch_id']
    mode = request.POST['pv']
    log = Logger()

    if mode.upper() == 'P':
        try:
            port_def = Ports.objects.get(id_ports=def_id)
            switch = Switch.objects.get(id_switch=switch_id)
            port_def.switch.remove(switch)
        except Exception as e:
            logger.error(
                "unable to remove device " + switch_id + " from port definition " +
                def_id + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "Removed device " + switch.name + " from port definition " + port_def.name)
        return HttpResponse("0", content_type="application/javascript")

    elif mode.upper() == "V":
        try:
            vlan_def = Vlans.objects.get(id_vlans=def_id)
            switch = Switch.objects.get(id_switch=switch_id)
            vlan_def.switch.remove(switch)
        except Exception as e:
            logger.error(
                "unable to remove device " + switch_id + " from vlan definition " +
                def_id + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "Removed device " + switch.name + " from vlan definition " + vlan_def.name)
        return HttpResponse("0", content_type="application/javascript")


def add_group_to_portvlan_def(request):
    """
    Add new group to port definition
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    def_id = request.POST['def_id']
    gid = request.POST['gid']
    mode = request.POST['pv']
    log = Logger()

    if mode.upper() == 'P':
        try:
            port_def = Ports.objects.get(id_ports=def_id)
            group = Group.objects.get(id_group=gid)
            port_def.groups.add(group)
            port_def.save()
        except Exception as e:
            logger.error(
                "unable to add group " + gid + " into port definition " +
                def_id + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "Added group " + group.name + " into port definition " + port_def.name)
        return HttpResponse("0", content_type="application/javascript")

    elif mode.upper() == 'V':
        try:
            vlan_def = Vlans.objects.get(id_vlans=def_id)
            group = Group.objects.get(id_group=gid)
            vlan_def.groups.add(group)
            vlan_def.save()
        except Exception as e:
            logger.error(
                "unable to add group " + gid + " into vlan definition " +
                def_id + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "Added group " + group.name + " into vlan definition " + vlan_def.name)
        return HttpResponse("0", content_type="application/javascript")


def delete_group_from_portvlan_def(request):
    """
    Remove group from port definition
    :param request:
    :return:
    """
    account = users.login(request)
    user = account[1]
    def_id = request.POST['def_id']
    gid = request.POST['gid']
    mode = request.POST['pv']
    log = Logger()

    if mode.upper() == 'P':
        try:
            port_def = Ports.objects.get(id_ports=def_id)
            group = Group.objects.get(id_group=gid)
            port_def.groups.remove(group)
        except Exception as e:
            logger.error(
                "unable to remove group " + gid + " from port definition " +
                def_id + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "Removed group " + group.name + " from port definition " + port_def.name)
        return HttpResponse("0", content_type="application/javascript")

    elif mode.upper() == 'V':
        try:
            vlan_def = Vlans.objects.get(id_vlans=def_id)
            group = Group.objects.get(id_group=gid)
            vlan_def.groups.remove(group)
        except Exception as e:
            logger.error(
                "unable to remove group " + gid + " from vlan definition " +
                def_id + ": " + str(e))
            return HttpResponse(str(e), content_type="application/javascript")
        log.write_incident(user, "Removed group " + group.name + " from vlan definition " + vlan_def.name)
        return HttpResponse("0", content_type="application/javascript")


def manage_vlans(request):
    """Show admin permission"""
    account = users.login(request)
    user = account[1]
    adm_groups = users.is_admin(account[0])
    vlans = Vlans.objects.all()
    groups = Group.objects.all()
    switches = Switch.objects.all()
    if user.superuser:
        return render(request, 'manage_vlans.html',
                      {'user': user,
                       'vlans': vlans,
                       'groups': groups,
                       'switches': switches,
                       'adm_groups': adm_groups
                       })


def get_switch_group_permission(request):
    switch = request.POST['switch_id']
    id_group = request.POST['group_id']

    groups = Group.objects.get(id_group=id_group)
    switch = Switch.objects.get(id_switch=switch)

    user_ports = Ports.objects.all()
    user_ports = user_ports.filter(switch=switch, groups=groups).distinct()

    switch_instance = services.get_instance(switch)

    existing_ports = switch_instance.get_port_descriptions()

    output_structure = []

    def update_permission(default, new):
        # 0 - no access
        # 1 - view_only
        # 2 - read_write
        permission_array = [0, 1, 2]
        if default in permission_array and new in permission_array:
            if new > default:
                return new
            else:
                return default
        else:
            raise TypeError("Default and new must be in range(0,2)")

    # existing_ports = map(lambda x: [x[0],x[1],0], existing_ports)
    temp_hash = {}

    for key in existing_ports.iterkeys():
        value = existing_ports[key]
        temp_hash[key] = [value, 0]
    existing_ports = temp_hash

    for group_of_ports in user_ports:
        regex_instance = users.RegExp(group_of_ports.regex)
        array = regex_instance.getStringArray()
        # ports muze obsahovat mnozinu portu (x-y), nebo
        # jeden port 10001 napr., proto je nutne rozlisit chovani
        # obou pripadu
        for ports in array:
            if '-' in ports:
                start = int(ports.split('-')[0])
                end = int(ports.split('-')[1])

                for i in range(start, end + 1):
                    try:
                        value = existing_ports[str(i)]
                    except KeyError:
                        continue

                    if group_of_ports.view_only:
                        existing_ports[str(i)] = [
                            value[0], update_permission(value[1], 1)]
                    else:
                        existing_ports[str(i)] = [
                            value[0], update_permission(value[1], 2)]
            else:
                try:
                    value = existing_ports[str(ports)]
                except KeyError:
                    continue
                if group_of_ports.view_only:
                    existing_ports[str(ports)] = [
                        value[0], update_permission(value[1], 1)]
                else:
                    existing_ports[str(ports)] = [
                        value[0], update_permission(value[1], 2)]

    for key in existing_ports.iterkeys():
        value = existing_ports[str(key)]
        output_structure.append(
            [key, value[0], value[1], switch_instance.is_port_trunk(key)])

    output_structure.sort(key=lambda x: int(x[0]))

    return HttpResponse(json.dumps(output_structure))


def generate_all_groups(request):
    try:
        obj_json = serializers.serialize('json', Group.objects.all())
        obj_list = json.loads(obj_json)
        json_data = json.dumps(obj_list)

        return HttpResponse(json_data, content_type='application/json')
    except Exception as e:
        json_structure = {'error': str(e)}
        logger.error(str(e))

        return HttpResponse(json.dumps(json_structure), content_type='application/json')


def load_portvlan_data(request):
    def_id = int(request.POST['def_id'])
    mode = request.POST['pv']

    try:
        if mode.upper() == "P":
            pv_def = Ports.objects.get(id_ports=def_id)
        elif mode.upper() == "V":
            pv_def = Vlans.objects.get(id_vlans=def_id)

        obj_json = serializers.serialize('json', [pv_def])
        obj_list = json.loads(obj_json)
        json_data = json.dumps(obj_list)

        return HttpResponse(json_data, content_type='application/json')

    except Exception as e:
        json_structure = {'error': str(e)}
        logger.error(str(e))

        return HttpResponse(json.dumps(json_structure), content_type='application/javascript')


def load_switch_groups(request):
    switch_id = int(request.POST['switch_id'])

    try:
        switch_def = Switch.objects.get(id_switch=switch_id)

        obj_json = serializers.serialize('json', [switch_def])
        obj_list = json.loads(obj_json)
        json_data = json.dumps(obj_list)

        return HttpResponse(json_data, content_type='application/json')

    except Exception as e:
        json_structure = {'error': str(e)}
        logger.error(str(e))

        return HttpResponse(json.dumps(json_structure), content_type='application/javascript')


def load_switches_available_for_portvlan(request):
    def_id = int(request.POST['def_id'])
    mode = request.POST['pv']

    try:
        if mode.upper() == "P":
            used_switches = Ports.objects.get(id_ports=def_id).switch.all()
        elif mode.upper() == "V":
            used_switches = Vlans.objects.get(id_vlans=def_id).switch.all()

        pv_def = Switch.objects.exclude(id_switch__in=[used_switch.id_switch for used_switch in used_switches])
        obj_json = serializers.serialize('json', pv_def)
        obj_list = json.loads(obj_json)
        json_data = json.dumps(obj_list)

        return HttpResponse(json_data, content_type='application/json')

    except Exception as e:
        json_structure = {'error': str(e)}
        logger.error(str(e))

        return HttpResponse(json.dumps(json_structure), content_type='application/javascript')
