FROM registry.gitlab.ics.muni.cz:443/ods/portmanager/base:2.0

#####################
## Application setup
#####################

# Add app folder with sourcecode
ADD ./webapp/portmanager_project /srv/portmanager
ADD ./webapp/scripts /srv/scripts
ADD ./config/ /srv/config

# Install application requirements
RUN pip install -r /srv/config/requirements.txt

# Create necessary folders
RUN mkdir /srv/data && chown -R www-data:www-data /srv/data
RUN mkdir /srv/tftp && chown -R www-data:www-data /srv/tftp
RUN mkdir /srv/ssh_keys && chown -R www-data:www-data /srv/ssh_keys

# Copy SNMP MIBS
COPY ./webapp/bin/mibs /usr/local/share/snmp/mibs
RUN MIBS=+CISCO-CONFIG-COPY-MIB:CISCO-SMI:SISCO:ST-TC

# Update user rights
RUN chown -R www-data:www-data /srv/portmanager
RUN chown -R www-data:www-data /srv/tftp
RUN chown -R www-data:www-data /srv/ssh_keys

# configure cron
COPY ./config/portmanager-cron /etc/cron.d/portmanager-cron
RUN crontab /etc/cron.d/portmanager-cron
RUN service cron restart

RUN chmod -R +x /srv/scripts
